<?php
/* Failiserveri seadistus */
define('CHROOT_PREFIX', ''); // define if PHP is chrooted
define('AUDIO_PATH', 'audio/');
define('PHOTO_PATH', 'foto/');
define('KASIKIRI_PATH', 'kasikiri/');
define('VIDEO_PATH', 'video/');

/* Converted files */
define('CONVERTED_AUDIO_PATH', 'converted_audio/');
define('IMAGE_PATH', 'img/');
define('CONVERTED_VIDEO_PATH', 'converted_video/');
define('ZIP_PATH', 'zip/');

/* Ekirjade saatmine */
define('EMAIL_SENDER_NAME', 'Admin');
define('EMAIL_SENDER_ADDRESS', 'admin@yourdomain.com');
define('EMAIL_SENDER_ORG', 'http://arhiiv.yourdomain.com/');

/* Keeled */
define('MASTER_LANG', 'est');
define('UI_LANG_1', 'est,Eesti keeles');
define('UI_LANG_2', 'eng,In English');
/* Keeled mida näidatakse üleval paremal nurgas. Võib uusi lisada kujul
define("UI_LANG_*", "abc,ABC keeles");
kus * on keele järjekorranumber siin nimekirjas, abc on keele Id tabelis ui_keeled.
Lipu pildi asukoht: kujundus/pildid/abc.png
Veendu enne keele siia lisamist, et ka tekstid_abc kataloog koos tekstidega oleks olemas. */

/* MySQL */
define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'username');
define('MYSQL_PASSWORD', 'password');
define('MYSQL_DB', 'db name');

/* Pildid */
/* Näiteks height 600 ja width 800 korral mahutatakse pilt 800x600 kasti,
originaalpildi proportsioonid jäävad paika.
Lõpuga PHOTO - töötab fotode puhul.
Lõpuga DEFAULT - kui pole foto ja käsikirja originaali formaat pole siin defineeritud.
Teised lõpud on käsikirjade "Originaali formaat" (A3, A4, ...). */
define('IMAGE_HEIGHT_DEFAULT', '580');
define('IMAGE_WIDTH_DEFAULT', '800');
define('IMAGE_HEIGHT_PHOTO', '450');
define('IMAGE_WIDTH_PHOTO', '700');
define('IMAGE_HEIGHT_A5', '580');
define('IMAGE_WIDTH_A5', '800');
define('IMAGE_HEIGHT_A4', '1000');
define('IMAGE_WIDTH_A4', '1300');


define('OSALEJA_FORMAAT', '{eesnimi} {perenimi}, {kihelkond} khk, {kyla} k.');
