﻿<?php
include('base.php');

if (sisse_logitud()) {
	header('Location: seaded.php');
	die();
}

$pealkiri1 = $lang['lemmikud_pealkiri'];

$nonSystemTables = get_non_system_tables();

if (isset($_SESSION['lemmikud'])) {
	$sisu = '<table width="100%">';
	foreach ($_SESSION['lemmikud'] as $lemmik) {
		$resource = explode('-', $lemmik); //eralda tabeli nimi [0] ja ID [1]
		$sisu .= '<tr><td><a href="naita.php?t=' . $resource[0] . '&id=' . $resource[1] . '">' . kriips($resource[0]);
		$tablerow = m_a(m_select($resource[0], ['id' => $resource[1]]));
		$must_show_columns = get_must_show_columns($resource[0]);
		foreach ($must_show_columns as $value) {
			if ($value == 'kestus') {
				$sisu .= ' ' . pretty_length($tablerow[$value]);
			} elseif ($value == 'materjal') {
				if ($query = m_select($tablerow['materjal'], ['id' => $tablerow['materjal_id']])) {
					$sisu .= ': ' . kriips($tablerow['materjal']);
					$row = m_a($query);
					$must_show_columns2 = get_must_show_columns($tablerow['materjal']);
					foreach ($must_show_columns2 as $value2) {
						$sisu .= ' ' . $row[$value2];
					}
				}
			} elseif (in_array($value, $nonSystemTables)) {
				if ($query = m_select($value, ['id' => $tablerow[$value]])) {
					$row = m_a($query);
					$must_show_columns2 = get_must_show_columns($value);
					foreach ($must_show_columns2 as $value2) {
						$sisu .= ' ' . $row[$value2];
					}
				}
			} else {
				$sisu .= ' ' . korrasta($tablerow[$value]);
			}
		}
		$sisu .= '</a></td></tr>';
	}
	$sisu .= '</table>';
} else {
	$sisu = $lang['lemmikud_info'];
}

include('kujundus.php');
