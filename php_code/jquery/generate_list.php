<?php
include('../base.php');
if (isset($_GET['tabel'])) {
	$tabel = $_GET['tabel'];
	$systemTables = get_system_tables();
	if (in_array($tabel, $systemTables)) {
		die();
	}
	if (isset($_GET['selected'])) {
		$selected = $_GET['selected'];
	} else {
		if (!$query = m_select($tabel, null, ['id'], ' ORDER BY andmebaasi_lisatud DESC LIMIT 1')) {
			die();
		}
		$query = m_a($query);
		$selected = $query['id'];
	}
	$xtabel = $_GET['u'];
	$shows = get_must_show_columns($tabel);
	$kysi_teisest_tabelist = m_select($tabel, null, null, ' ORDER BY `' . $shows[0] . '`');
	if ($_GET['materjal'] == 'roll' || $_GET['materjal'] == 'digitaliseerija') {
		$tabel = 'materjal_id';
	}
	if (!$xtabel) {
		$xtabel = $_GET['materjal'];
	}
	?>
	<select name="<?=$tabel?>" width="300" style="width: 300px" onchange="arhiiviNr('<?=$xtabel?>', this.value); ab('<?=$xtabel?>');">
		<option value=""></option>
		<?php
		while ($rida = m_a($kysi_teisest_tabelist)) {
			if ($rida['id'] == $selected) {
				$sel = ' selected';
			} else {
				$sel = '';
			}
			echo '<option value="' . $rida['id'] . '"' . $sel . '>';
			foreach ($shows as $show) {
				echo ' ' . $rida[$show];
			}
			echo '</option>';
		}
		?>
	</select>
<?php
}
