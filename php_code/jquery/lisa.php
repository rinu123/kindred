﻿<?php
include('../base.php');
if (!$sess = sisse_logitud()) {
	header('Location: ../index.php');
	die('viga');
}
if ($grupi_oigused = grupi_oigused($sess)) {
	if ($grupi_oigused < 2) {
		header('Location: ../index.php');
		die('viga');
	}
} else {
	header('Location: ../index.php');
	die('viga');
}

if (isset($_POST['tabel'])) { //tahetakse lisada kirje andmebaasi
	$r = $_POST;
	$xtabel = trim($r['tabel']);
	if (!in_array($xtabel, get_non_system_tables())) {
		die('<div class="center"><img src="kujundus/pildid/viga.png"><br />' . $lang['tabelit'] . ' ' . $xtabel . ' ' . $lang['ei_leitud'] . '</div>');
	}
	$must_have_fields = get_must_have_columns($xtabel);
	$excludes = excludes(0);
	if (in_array($xtabel, get_info_tables())) {
		unset($excludes[0]);
	}
	$lisatudKeel = '';
	$sql = [];
	foreach (get_full_columns($xtabel, $excludes) as $column) {
		if (in_array($column['Field'], $must_have_fields) && !$r[$column['Field']]) {
			die('<div class="center"><img src="kujundus/pildid/viga.png"><br />' . kriips($column['Field']) . ' ' . $lang['on_kohustuslik'] . '</div>');
		}
		$n = trim($r[$column['Field']] ?? ''); //eemalda whitespace'd ja muuda ära erisümbolid
		if (!in_array($column['Field'], $excludes) && $n) { //eemalda päringust ebavajalik
			$tyyp = $column['Type']; //otsitava andme tüüp
			$tyyp = explode('(', $tyyp);
			$tyyp = $tyyp[0]; //int, varchar, date, datetime etc
			if ($tyyp == 'date') {
				if ($kuup = date_to_mysql($n)) {
					$sql[$column] = $kuup;
				}
			} elseif ($column['Field'] == 'kestus') {
				$tykid = array_reverse(explode(':', $n));
				$korda = 1;
				$secs = 0;
				foreach ($tykid as $tykk) {
					$secs += $tykk * $korda;
					$korda *= 60;
				}
				$sql['kestus'] = $secs;
			} elseif ($column['Field'] == 'keel' && $xtabel != 'materjal_keel') {
				$lisatudKeel = $n;
			} else {
				$sql[$column['Field']] = $n;
			}
		}
	}
	$sql['lisaja'] = $sess['id'];
	m_insert($xtabel, $sql, ['andmebaasi_lisatud' => 'NOW()']) or die('<div class="center"><img src="kujundus/pildid/viga.png"><br />' . m_err() . '</div>');
	$inserted_id = m_id();
	if ($lisatudKeel) {
		m_insert('materjal_keel', [
			'keel' => $lisatudKeel,
			'materjal' => $xtabel,
			'materjal_id' => $inserted_id
		]);
	}
	m_insert('logi', [
		'ip' => $_SERVER['REMOTE_ADDR'],
		'syndmus' => '<a href="naita.php?t=kasutaja&id=' . $sess['id'] . '">' . $sess['email'] . '</a> lisas <a href="naita.php?t=' . $xtabel . '&id=' . $inserted_id . '">' . kriips($xtabel) . ' id ' . $inserted_id . '</a>'
	], [
		'andmebaasi_lisatud' => 'NOW()'
	]);
	echo '<div class="center"><img src="kujundus/pildid/teade.png"><br />' . kriips($xtabel) . ' ' . $lang['lisatud'] . ' <a href="naita.php?t=' . $xtabel . '&id=' . $inserted_id . '">' . $lang['vaata'] . '</a></div>';
	$arhiiviNrQuery = m_select($xtabel, [
		'arhiivinumber' => $r['arhiivinumber']
	]);
	if (isset($r['arhiivinumber']) && m_r($arhiiviNrQuery) > 1) {
		echo '<a href="otsi.php?' . $xtabel . '=Otsi&arhiivinumber=' . $r['arhiivinumber'] . '&tabel=' . $xtabel . '">' . $lang['sama_arhiivinumbriga_kirjeid_on_veel'] . '<img src="kujundus/pildid/viga.png"></a>';
	}
	echo '<script type="text/javascript">
		document.getElementById("lisaButton_' . $xtabel . '").disabled = true;';
	$tabelid = get_non_system_tables();
	foreach ($tabelid as $tabel) {
		foreach (get_columns($tabel) as $column) {
			if ($column == $xtabel) {
				echo 'document.getElementById("lisa_' . $tabel . '").style.display = "inline";';
			}
		}
	}
	$publicTables = get_public_tables();
	if (in_array($xtabel, $publicTables)) {
		echo 'document.getElementById("lisa_digitaliseerija").style.display = "inline";';
		echo 'document.getElementById("lisa_roll").style.display = "inline";';
		echo '
			$("#uus_digitaliseerija_digitaliseerija").load("jquery/generate_list.php?tabel=' . $xtabel . '&materjal=digitaliseerija");
			$("#uus_roll_roll").load("jquery/generate_list.php?tabel=' . $xtabel . '&materjal=roll");
		';
	}
	if (isset($_POST['u']) && $_POST['hide'] == '1') {
		echo '
			document.getElementById("' . $xtabel . '_' . $_POST['u'] . '_sisu").style.display="none";
			document.getElementById("hide_' . $xtabel . '_' .$_POST['u'] . '").style.display="none";
			$("#uus_' . $_POST['u'] . '_' . $xtabel . '").load("jquery/generate_list.php?u=' . $_POST['u'] . '&tabel=' . $xtabel . '");
			arhiiviNr("' . $_POST['u'] . '", ' . $inserted_id . ');
		';
	}
	echo '</script>';
} else {
	echo '<div class="center"><img src="kujundus/pildid/viga.png"><br />' . $lang['paring_ei_onnestunud'] . '</div>';
}
