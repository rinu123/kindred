﻿<?php
include('../base.php');
if (!$sess = sisse_logitud()) {
	header('Location: ../index.php');
	die();
}
if ($grupi_oigused = grupi_oigused($sess)) {
	if ($grupi_oigused < 2) {
		header('Location: index.php');
		die();
	}
} else {
	header('Location: index.php');
	die();
}
if (isset($_GET['tabel'])) { //tahetkse saada lisamise vormi
	$public_tables = get_public_tables();
	$xtabel = $_GET['tabel'];
	if (isset($_GET['u'])) {
		$u = $_GET['u'];
		$hide = $_GET['hide'] ?? null;
	} else {
		$u = '';
		$hide = '0';
	}
	if (!in_array($xtabel, get_non_system_tables())) {
		die('<div class="center"><img src="kujundus/pildid/viga.png"><br />' . $lang['tabelit'] . ' ' . $xtabel . ' ' . $lang['ei_leitud'] . '</div>');
	}
	$xtabel1 = kriips($xtabel, 0);
	$pealkiri1 = $lang['lisa'] . ' ' . $xtabel1;
	$sisu = '';
	$tabelid = get_non_system_tables();
	foreach (get_columns($xtabel) as $column) {
		if (in_array($column, $tabelid)) {
			$sisu .= '<span id="' . $column . '_' . $xtabel . '_sisu"></span>';
		}
	}
	$sisu .= '<form id="' . $xtabel . $u . '" action="#">
		<table width="100%">';
	$excludes = excludes(0);
	if (in_array($xtabel, get_info_tables())) {
		unset($excludes[0]);
	}
	foreach (get_full_columns($xtabel) as $column) { //genereeritakse html vorm
		if (!in_array($column['Field'], $excludes)) {
			$leiatyyp = explode('(', $column['Type']);
			$fieldType = $leiatyyp[0];
			$fieldTypeLength = 0;
			if (isset($leiatyyp[1])) {
				$fieldTypeLength = explode(')', $leiatyyp[1])[0];
			}
			if ($column['Null'] == 'NO') {
				$tarn = '*';
			} else {
				$tarn = '';
			}
			$sisu .= '<tr><td>' . kriips($column['Field']) . ' ' . $tarn . '</td><td>';
			if (in_array($column['Field'], $tabelid)) {
				$selected = null;
				$whereId = [];
				if ($_GET['id']) {
					if ($_GET['u'] == 'osaleja') {
						$whereId['id'] = $_GET['id'];
						$selected = $_GET['id'];
					} else {
						if ($fieldType == 'int') {
							$maxIdQuery = m_a(m_q('SELECT MAX(id) AS id FROM `' . $column['Field'] . '`'));
							$selected = $maxIdQuery['id'];
						}
					}
				} else {
					if ($fieldType == 'int') {
						$maxIdQuery = m_a(m_q('SELECT MAX(id) AS id FROM `' . $column['Field'] . '`'));
						$selected = $maxIdQuery['id'];
					}
				}
				if ($column['Field'] == 'rolli_nimetus') {
					$selected = 'Keelejuht';
				}
				$shows = get_must_show_columns($column['Field']);
				$kysi_teisest_tabelist = m_select($column['Field'], $whereId, null, ' ORDER BY `' . $shows[0] . '`');
				if ($fieldType == 'int') {
					$onChange = 'onchange="arhiiviNr(\'' . $xtabel . '\', this.value); ab(\'' . $xtabel . $u . '\');"';
				} else {
					$onChange = '';
				}
				$sisu .= '<span id="uus_' . $xtabel . '_' . $column['Field'] . '"><select name="' . $column['Field'] . '" width="300" style="width: 300px" ' . $onChange . '><option value=""></option>';
				while ($rida = m_a($kysi_teisest_tabelist)) {
					if ($rida['id'] == $selected) {
						$selectedF = ' selected';
					} else {
						$selectedF = '';
					}
					if ($column['Field'] != 'sari' || $rida['materjali_tyyp'] == $xtabel || $rida['materjali_tyyp'] == $xtabel . '_trakk' || $rida['materjali_tyyp'] . 'album' == $xtabel) {
						$sisu .= '<option value="' . $rida['id'] . '"' . $selectedF . '>';
						foreach ($shows as $show) {
							$sisu .= ' ' . $rida[$show];
						}
						$sisu .= '</option>';
					}
				}
				$sisu .= '</select></span>';
				$sisu .= '<input type="submit" name="lisanupp" value="Lisa ' . kriips($column['Field'], 0) . '" onclick="lisa_vorm(\'' . $column['Field'] . '\', \'' . $xtabel . '\', \'1\'); this.disabled = true; document.getElementById(\'hide_' . $column['Field'] . '_' . $xtabel . '\').style.display=\'inline\'; return false;">
					<img id="hide_' . $column['Field'] . '_' . $xtabel . '" src="kujundus/pildid/hide.png" style="display: none;" alt="Peida ' . kriips($column['Field'], 0) . ' lisamisvorm" title="Peida ' . kriips($column['Field'], 0) . ' lisamisvorm" onclick="document.getElementById(\'' . $column['Field'] . '_' . $xtabel . '_sisu\').style.display=\'none\'; this.style.display=\'none\';">';
			} elseif ($column['Field'] == 'ressursi_tyyp') {
				$sisu .= '
					<select name="ressursi_tyyp" onchange="ab(\'' . $xtabel . $u . '\');">
						<option value=""></option>
						<option value="1">' . $lang['fyysiline'] . '</option>
						<option value="2">' . $lang['digitaalne'] . '</option>
					</select>';
			} elseif ($column['Field'] == 'fyysiliselt_olemas') {
				$sisu .= '
					<select name="fyysiliselt_olemas" onchange="ab(\'' . $xtabel . $u . '\');">
						<option value=""></option>
						<option value="1">' . $lang['olemas'] . '</option>
						<option value="2">' . $lang['kadunud'] . '</option>
						<option value="3">' . $lang['laenatud'] . '</option>
					</select>';
			} elseif ($column['Field'] == 'digitaalselt_olemas') {
				$sisu .= '
					<select name="digitaalselt_olemas" onchange="ab(\'' . $xtabel . $u . '\');">
						<option value=""></option>
						<option value="1">' . $lang['jah'] . '</option>
						<option value="2">' . $lang['ei'] . '</option>
					</select>';
			} elseif ($column['Field'] == 'oigused') {
				$sisu .= '
					<select name="oigused" onchange="ab(\'' . $xtabel . $u . '\');">
						<option value="avalik">' . $lang['avalik'] . '</option>
						<option value="mitteavalik" selected>' . $lang['mitteavalik'] . '</option>
					</select>';
			} elseif ($column['Field'] == 'materjal' && isset($_GET['u']) && $_GET['u'] != 'osaleja') {
				$sisu .= kriips($_GET['u']) . '<input type="hidden" name="materjal" value="' . $_GET['u'] . '">';
			} elseif ($column['Field'] == 'materjal') {
				$sisu .= '
					<select name="materjal" onchange="generate_list(this.value, \'' . $xtabel . '\'); ab(\'' . $xtabel . $u . '\');">
						<option value=""></option>';
				foreach ($public_tables as $value) {
					$sisu .= '<option value="' . $value . '">' . kriips($value) . '</option>';
				}
				$sisu .= '</select>';
			} elseif ($column['Field'] == 'materjal_id' && isset($_GET['u']) && $_GET['u'] != 'osaleja') {
				$teine_tabel = $_GET['u'];
				if (isset($_GET['id'])) {
					$whereId = [
						'id' => $_GET['id']
					];
				} else {
					$whereId = [];
				}
				$must_have_columns = get_must_show_columns($teine_tabel);
				$paring = m_select($teine_tabel, $whereId, null, '  ORDER BY `' . $must_have_columns[0] . '`');
				$maxIdQuery = m_a(m_select($teine_tabel, null, ['id'], ' ORDER BY id DESC LIMIT 1'));
				$selected = $maxIdQuery['id'];
				$sisu .= '<span id="uus_' . $xtabel . '_' . $xtabel . '"><select name="materjal_id" onchange="ab(\'' . $xtabel . $u . '\');">';
				while ($rida = m_a($paring)) {
					if ($rida['id'] == $selected) {
						$selectedF = ' selected';
					} else {
						$selectedF = '';
					}
					$sisu .= '<option value="' . $rida['id'] . '"' . $selectedF . '>';
					foreach ($must_have_columns as $value) {
						if ($value == 'kestus') {
							$sisu .= pretty_length($rida[$value]) . ' ';
						} else {
							$sisu .= $rida[$value] . ' ';
						}
					}
					$sisu .= '</option>';
				}
				$sisu .= '</select></span>';
			} elseif ($column['Field'] == 'materjal_id') {
				$sisu .= '<span id="uus_' . $xtabel . '_' . $xtabel . '"></span>';
			} elseif ($column['Field'] == 'kestus') {
				$sisu .= '<input type="text" name="kestus" onchange="ab(\'' . $xtabel . $u . '\');"> ' . $lang['numbrid'] . ' ' . $lang['TT_MM_SS'];
			} elseif ($column['Field'] == 'arhiivinumber') {
				if ($xtabel == 'audio') {
					$up = 'sari';
					$digits = 4;
					$sep = '';
				} elseif ($xtabel == 'audio_trakk') {
					$up = 'audio';
					$digits = 2;
					$sep = '-';
				} elseif ($xtabel == 'foto') {
					$up = 'fotoalbum';
					$digits = 3;
					$sep = '-';
				} elseif ($xtabel == 'fotoalbum') {
					$up = 'sari';
					$digits = 4;
					$sep = '';
				} elseif ($xtabel == 'kasikiri') {
					$up = 'sari';
					$digits = 4;
					$sep = '';
				} elseif ($xtabel == 'video') {
					$up = 'sari';
					$digits = 4;
					$sep = '';
				} elseif ($xtabel == 'video_trakk') {
					$up = 'video';
					$digits = 2;
					$sep = '-';
				} else {
					$arhiiviNumber = '';
				}
				if (isset($up)) {
					$whereParams = [];
					if ($up == 'sari') {
						$what = 'sarja_tahis';
						$whereMaterjal = ' WHERE materjali_tyyp LIKE :materjali_tyyp1 OR materjali_tyyp LIKE :materjali_tyyp2';
						$whereParams['materjali_tyyp1'] = $xtabel . '%';
						$whereParams['materjali_tyyp2'] = str_replace('album', '', $xtabel) . '%';
					} else {
						$what = 'arhiivinumber';
						$whereMaterjal = '';
					}
					$query = m_q('SELECT id,' . $what . ' FROM ' . $up . $whereMaterjal . ' ORDER BY id DESC LIMIT 1', $whereParams);
					if (m_r($query)) {
						$row = m_a($query);
						$arhiiviNumber = $row[$what];
						$start = m_select($xtabel, [
							$up => $row['id']
						], null, ' ORDER BY arhiivinumber DESC LIMIT 1');
						if (m_r($start)) {
							$rida = m_a($start);
							$lastNrParts = explode($arhiiviNumber, $rida['arhiivinumber']);
							if ($sep == '-') {
								$linesep = explode('-', $lastNrParts[1]);
								$lastNrParts[1] = $linesep[1];
							}
							$newNr = $lastNrParts[1] + 1;
						} else {
							$newNr = 1;
						}
						while (strlen($newNr) < $digits) {
							$newNr = '0' . $newNr;
						}
						$arhiiviNumber = $arhiiviNumber . $sep . $newNr;
					} else {
						$arhiiviNumber = '';
					}
				}
				$sisu .= '<span id="' . $xtabel . '_arhiiv"><input type="text" name="arhiivinumber" onchange="ab(\'' . $xtabel . $u . '\');" value="' . $arhiiviNumber . '"></span>' . pretty_fieldtype($fieldType);
			} elseif ($fieldType == 'text') {
				$sisu .= '<textarea name="' . $column['Field'] . '" cols="25" rows="3" onchange="ab(\'' . $xtabel . $u . '\');"></textarea>' . pretty_fieldtype($fieldType);
			} elseif ($column['Field'] == 'materjali_tyyp') {
				$sisu .= '<select name="materjali_tyyp" onchange="ab(\'' . $xtabel . $u . '\');">';
				foreach ($public_tables as $public_table) {
					$sisu .= '<option value="' . $public_table . '">' . kriips($public_table) . '</option>';
				}
				$sisu .= '</select>';
			} else {
				$sisu .= '<input type="text" name="' . $column['Field'] . '" onchange="ab(\'' . $xtabel . $u . '\');">' . pretty_fieldtype($fieldType, $fieldTypeLength);
			}
			$sisu .= '</td></tr>';
		}
	} //genereeriti html vorm
	$sisu .= '
			<tr>
				<td></td>
				<td><input type="submit" id="lisaButton_' . $xtabel . '" value="' . $lang['lisa'] . ' ' . $xtabel1 . '" onclick="lisa(\'' . $xtabel . '\', \'' . $u . '\', \'' . $hide . '\'); return false;" /></td>
			</tr>
		</table>
	</form>';
	foreach ($tabelid as $tabel) {
		foreach (get_columns($tabel) as $column) {
			if ($column == $xtabel) {
				$sisu .= '<input type="submit" id="lisa_' . $tabel . '" style="display:none;" value="' . $lang['lisa'] . ' ' . kriips($tabel, 0) . '" onclick="lisa_vorm(\'' . $tabel . '\', \'' . $xtabel . '\', \'0\'); this.disabled = true; document.getElementById(\'hide_' . $tabel . '_' . $xtabel . '\').style.display=\'inline\'; return false;">
					<img id="hide_' . $tabel . '_' . $xtabel . '" src="kujundus/pildid/hide.png" style="display: none;" alt="' . $lang['peida'] . ' ' . kriips($tabel, 0) . ' lisamisvorm" title="' . $lang['peida'] . ' ' . kriips($tabel, 0) . ' lisamisvorm" onclick="document.getElementById(\'' . $tabel . '_' . $xtabel . '_sisu\').style.display=\'none\'; document.getElementById(\'lisa_' . $tabel . '\').disabled = false; this.style.display=\'none\';">
				<br />';
				$sisu .= '<span id="' . $tabel . '_' . $xtabel . '_sisu"></span>';
			}
		}
	}

	if (in_array($xtabel, $public_tables)) {
		$sisu .= '<input type="submit" id="lisa_digitaliseerija" style="display:none;" value="Lisa digitaliseerija" onclick="lisa_vorm(\'digitaliseerija\', \'' . $xtabel . '\', \'0\'); this.disabled = true; document.getElementById(\'hide_digitaliseerija_' . $xtabel . '\').style.display=\'inline\'; return false;">
			<img id="hide_digitaliseerija_' . $xtabel . '" src="kujundus/pildid/hide.png" style="display: none;" alt="' . $lang['peida'] . ' ' . $lang['digitaliseerija_lisamisvorm'] . '" title="' . $lang['peida'] . ' ' . $lang['digitaliseerija_lisamisvorm'] . '" onclick="document.getElementById(\'digitaliseerija_' . $xtabel . '_sisu\').style.display=\'none\'; document.getElementById(\'lisa_digitaliseerija\').disabled = false; this.style.display=\'none\';">
		<br />';
		$sisu .= '<span id="digitaliseerija_' . $xtabel . '_sisu"></span>';

		$sisu .= '<input type="submit" id="lisa_roll" style="display:none;" value="Lisa mingi muu roll" onclick="lisa_vorm(\'roll\', \'' . $xtabel . '\', \'0\'); this.disabled = true; document.getElementById(\'hide_roll_' . $xtabel . '\').style.display=\'inline\'; return false;">
			<img id="hide_roll_' . $xtabel . '" src="kujundus/pildid/hide.png" style="display: none;" alt="' . $lang['peida'] . ' ' . $lang['rolli_lisamisvorm'] . '" title="' . $lang['peida'] . ' ' . $lang['rolli_lisamisvorm'] . '" onclick="document.getElementById(\'roll_' . $xtabel . '_sisu\').style.display=\'none\'; document.getElementById(\'lisa_roll\').disabled = false; this.style.display=\'none\';">
		<br />';
		$sisu .= '<span id="roll_' . $xtabel . '_sisu"></span>';
	}
	echo $sisu;
}
