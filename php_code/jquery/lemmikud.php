﻿<?php
include('../base.php');

$xtabel = $_GET['tabel'];
if (!in_array($xtabel, get_non_system_tables())) {
	die('<img src="kujundus/pildid/not_ok.png" alt="' . $lang['lemmikut_ei_saanud_lisada'] . '" title="' . $lang['lemmikut_ei_saanud_lisada'] . '">');
}
$t_id = $_GET['id'];
if (!m_r(m_select($xtabel, ['id' => $t_id]))) {
	die('<img src="kujundus/pildid/not_ok.png" alt="' . $lang['lemmikut_ei_saanud_lisada'] . '" title="' . $lang['lemmikut_ei_saanud_lisada'] . '">');
}

if (isset($_GET['a'])) { //välja logitud
	$ressursiNimi = $xtabel . '-' . $t_id;
	if (isset($_SESSION['lemmikud'])) {
		if (in_array($ressursiNimi, $_SESSION['lemmikud'])) {
			die('<img src="kujundus/pildid/not_ok.png" alt="' . $lang['juba_lisasid'] . '" title="' . $lang['juba_lisasid'] . '">');
		}
		$_SESSION['lemmikud'][] = $ressursiNimi;
		die('<img src="kujundus/pildid/ok.png" alt="' . $lang['lemmik_lisatud'] . '" title="' . $lang['lemmik_lisatud'] . '">');
	}
	$_SESSION['lemmikud'] = [$ressursiNimi];
	die('<img src="kujundus/pildid/ok.png" alt="' . $lang['lemmik_lisatud'] . '" title="' . $lang['lemmik_lisatud'] . '">');
}

//sisse logitud
if (!$sess = sisse_logitud()) {
	header('Location: ../index.php');
	die();
}

if (m_r(m_select('lemmikud', ['tabel' => $xtabel, 'vali' => $t_id, 'lisaja' => $sess['id']]))) {
	die('<img src="kujundus/pildid/not_ok.png" alt="' . $lang['juba_lisasid'] . '" title="' . $lang['juba_lisasid'] . '">');
}

if (!m_insert('lemmikud', ['lisaja' => $sess['id'], 'tabel' => $xtabel, 'vali' => $t_id], ['andmebaasi_lisatud' => 'NOW()'])) {
	die('<img src="kujundus/pildid/not_ok.png" alt="' . $lang['lemmikut_ei_saanud_lisada'] . '" title="' . $lang['lemmikut_ei_saanud_lisada'] . '">');
}
die('<img src="kujundus/pildid/ok.png" alt="' . $lang['lemmik_lisatud'] . '" title="' . $lang['lemmik_lisatud'] . '">');
