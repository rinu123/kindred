<?php
include('../base.php');
if (isset($_GET['tabel'])) {
	$tabel = $_GET['tabel'];
	$id = $_GET['id'];
	$systemTables = get_system_tables();
	if (in_array($tabel, $systemTables)) {
		die();
	}
	if ($tabel == 'audio') {
		$up = 'sari';
		$digits = 4;
		$sep = '';
	} elseif ($tabel == 'audio_trakk') {
		$up = 'audio';
		$digits = 2;
		$sep = '-';
	} elseif ($tabel == 'foto') {
		$up = 'fotoalbum';
		$digits = 3;
		$sep = '-';
	} elseif ($tabel == 'fotoalbum') {
		$up = 'sari';
		$digits = 4;
		$sep = '';
	} elseif ($tabel == 'kasikiri') {
		$up = 'sari';
		$digits = 4;
		$sep = '';
	} elseif ($tabel == 'video') {
		$up = 'sari';
		$digits = 4;
		$sep = '';
	} elseif ($tabel == 'video_trakk') {
		$up = 'video';
		$digits = 2;
		$sep = '-';
	} else {
		$arhiiviNumber = '';
	}
	if (isset($up)) {
		if ($up == 'sari') {
			$what = 'sarja_tahis';
		} else {
			$what = 'arhiivinumber';
		}
		$query = m_select($up, ['id' => $id], [$what]);
		if (m_r($query)) {
			$row = m_a($query);
			$arhiiviNumber = $row[$what];
			$start = m_select($tabel, [$up => $id], null, ' ORDER BY arhiivinumber DESC LIMIT 1');
			if (m_r($start)) {
				$rida = m_a($start);
				$lastNrParts = explode($arhiiviNumber, $rida['arhiivinumber']);
				if ($sep == '-') {
					$linesep = explode('-', $lastNrParts[1]);
					$lastNrParts[1] = $linesep[1];
				}
				$newNr = $lastNrParts[1] + 1;
			} else {
				$newNr = 1;
			}
			while (strlen($newNr) < $digits) {
				$newNr = '0' . $newNr;
			}
			$arhiiviNumber = $arhiiviNumber.$sep.$newNr;
		} else {
			$arhiiviNumber = '';
		}
	}
	echo '<input type="text" name="arhiivinumber" onchange="ab(\'' . $tabel . '\')" value="' . $arhiiviNumber . '">';
}
