﻿<?php
include('base.php');
if ($sess = sisse_logitud()) {
	if (!$minu_grupi_oigused = grupi_oigused($sess)) {
		$minu_grupi_oigused = 0;
	}
	$minuLemmikud = m_q('SELECT * FROM lemmikud WHERE lisaja = :lisaja', ['lisaja' => $sess['id']]);
	$lemmikud = [];
	while ($lemmik = m_a($minuLemmikud)) {
		$lemmikud[] = $lemmik['tabel'] . '-' . $lemmik['vali'];
	}
	$jsLemmik = '';
} else {
	$minu_grupi_oigused = 0;
	if (isset($_SESSION['lemmikud'])) {
		$lemmikud = $_SESSION['lemmikud'];
	} else {
		$lemmikud = [];
	}
	$jsLemmik = '_a';
}

if ($minu_grupi_oigused <= 0) { //sisse logimata
	$tabelid = get_public_tables();
	$excludes = excludes();
} elseif ($minu_grupi_oigused <= 1) { //otsimine
	$tabelid = get_non_system_tables();
	$excludes = excludes();
} elseif ($sess['grupp'] <= 2) { //otsimine, lisamine
	$tabelid = get_non_system_tables();
	$excludes = excludes(0);
} else { //admin
	$tabelid = get_table_names();
	$excludes = [];
}

$allTables = get_table_names();
$nonSystemTables = get_non_system_tables();

$tabel1 = kriips($_GET['t']);
$xtabel = $_GET['t'];
$t_id = $_GET['id'];

$orig_aeg = [
	'audio_trakk' => 'salvestusaeg',
	'video_trakk' => 'salvestusaeg',
	'foto' => 'pildistamisaeg',
	'kasikiri' => 'koostamise_aeg'
];

$pealkiri1 = $tabel1;
$sisu = <<<SISU
	<table class="colspace">
SISU;

if (isset($_GET['muuda'])) {
	if ($_GET['muuda']) {
		$teade = $lang['kirje_muutmine_onnestus1'];
	} else {
		$viga = $lang['kirje_muutmine_onnestus0'];
	}
}

if (in_array($xtabel, $tabelid)) {
	$where = [
		'id' => $t_id
	];
	if ($minu_grupi_oigused <= 0) {
		$where['oigused'] = 'avalik';
	}
	$paring = m_select($xtabel, $where);
	$roles_columns = get_roles_columns($xtabel);
	if (m_r($paring)) {
		$r = m_a($paring);
		$resource_name = $xtabel . '-' . $t_id;
		$sisu .= '<tr><td style="min-width: 175px; max-width: 175px;">';
		if (isset($_SESSION['otsing'])) {
			$sisu .= '<a href="' . $_SESSION['otsing'] . '">' . $lang['viimati_sooritatud_otsing'] . '</a>';
		}
		$sisu .= '</td><td style="width: 100%"></td></tr>';
		$sisu2 = '';
		if ($xtabel == 'audio_trakk') {
			$sari = m_a(m_q('SELECT * FROM audio WHERE id = :id', ['id' => $r['audio']]));
			$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $sari['sari']]));
			$eemaldaSari = explode($sarjaParing['sarja_tahis'], $r['arhiivinumber']);
			$filePath = AUDIO_PATH . $sarjaParing['sarja_tahis'] . '_WAV/' . $sarjaParing['sarja_tahis'] . substr($eemaldaSari[1], 0, 2);
			if (findOriginalFile($filePath, $r['arhiivinumber'], ['wav'])) {
				$sisu2 .= '<tr><td><audio src="get_resource.php?t=' . $xtabel . '&amp;id=' . $t_id . '&amp;format=CONVERTED_AUDIO&amp;' . uniqid('', true) . '" controls controlsList="nodownload" /></td></tr>';
				if ($minu_grupi_oigused > 0) {
					$sisu2 .= '<tr><td><a href="get_resource.php?t=' . $xtabel . '&amp;id=' . $t_id . '">' . $lang['lae_alla'] . '</a></td></tr>';
				}
			}
			if (!$sisu2) {
				$uploadDir = $filePath;
			}
		} elseif ($xtabel == 'foto') {
			$album = m_a(m_q('SELECT * FROM fotoalbum WHERE id = :id', ['id' => $r['fotoalbum']]));
			$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $album['sari']]));
			$filePath = PHOTO_PATH . $sarjaParing['sarja_tahis'] . '/' . $album['arhiivinumber'];
			if (findOriginalFile($filePath, $r['arhiivinumber'], ['jpg', 'jpeg'])) {
				$sisu2 .= '<tr>
								<td>
									<a href="naita_pilt.php?materjal=foto&amp;materjal_id=' . $r['arhiivinumber'] . '&amp;sari=' . $sarjaParing['sarja_tahis'] . '&amp;album=' . $album['arhiivinumber'] . '&amp;formaat=PHOTO" target="_blank">' . $lang['vaata_fotot'] . '</a>
								</td>
								<td></td>
								<td rowspan="20">
									<a href="naita_pilt.php?materjal=foto&amp;materjal_id=' . $r['arhiivinumber'] . '&amp;sari=' . $sarjaParing['sarja_tahis'] . '&amp;album=' . $album['arhiivinumber'] . '&amp;formaat=PHOTO" target="_blank">
										<img src="naita_pilt.php?materjal=foto&amp;materjal_id=' . $r['arhiivinumber'] . '&amp;sari=' . $sarjaParing['sarja_tahis'] . '&amp;album=' . $album['arhiivinumber'] . '&amp;formaat=PHOTO&amp;pisi=1" style="width: auto; height: auto; max-width: 300px; max-height: 300px;" />
									</a>
								</td>
							</tr>';
			}
			if (!$sisu2) {
				$uploadDir = $filePath;
			}
		} elseif ($xtabel == 'kasikiri') {
			$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $r['sari']]));
			$filePath = KASIKIRI_PATH . $sarjaParing['sarja_tahis'] . '/' . $r['arhiivinumber'];
			if (is_dir($filePath)) {
				$sisu2 .= '<tr><td><a href="naita_pilt.php?materjal=kasikiri&amp;materjal_id=' . $r['arhiivinumber'] . '&amp;sari=' . $sarjaParing['sarja_tahis'] . '&amp;formaat=' . $r['originaali_formaat'] . '" target="_blank">' . $lang['vaata_kasikirju'] . '</a></td></tr>';
				if ($sess) {
					$sisu2 .= '<tr><td><a href="get_resource.php?t=' . $xtabel . '&amp;id=' . $t_id . '&amp;format=ZIP">' . $lang['lae_kasikirjad_alla'] . '</a></td></tr>';
				}
			}
			$uploadDir = $filePath;
		} elseif ($xtabel == 'video_trakk') {
			$sari = m_a(m_q('SELECT * FROM video WHERE id = :id', ['id' => $r['video']]));
			$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $sari['sari']]));
			$eemaldaSari = explode($sarjaParing['sarja_tahis'], $r['arhiivinumber']);
			$filePath = VIDEO_PATH . $sarjaParing['sarja_tahis'] . '/' . $sarjaParing['sarja_tahis'] . substr($eemaldaSari[1], 0, 2);
			$finalOrigFile = findOriginalFile($filePath, $r['arhiivinumber']);
			if ($finalOrigFile) {
				$finalConvertedFile = CONVERTED_VIDEO_PATH . pathinfo($finalOrigFile)['filename'] . '.mp4';
				if (file_exists($finalConvertedFile)) {
					$sisu2 .= '<tr><td colspan="2"><video style="max-width: 100%" src="get_resource.php?t=' . $xtabel . '&amp;id=' . $t_id . '&amp;format=CONVERTED_VIDEO' . '&amp;' . uniqid('', true) . '" controls controlsList="nodownload" /></td></tr>';
				}
				if ($minu_grupi_oigused > 0) {
					$sisu2 .= '<tr><td><a href="get_resource.php?t=' . $xtabel . '&amp;id=' . $t_id . '">' . $lang['lae_alla'] . '</a></td></tr>';
				}
			}
			if (!$sisu2) {
				$uploadDir = $filePath;
			}
		}
		$sisu .= '<tr><td colspan="2">';
		if ($minu_grupi_oigused > 2 || $r['lisaja'] == $sess['id']) {
			$sisu .= '
				<a href="muuda.php?t=' . $xtabel . '&id=' . $t_id . '"><img src="kujundus/pildid/muuda.png" title="' . $lang['muuda'] . '" alt="' . $lang['muuda'] . '" /></a> 
				<a href="kustuta.php?t=' . $xtabel . '&id=' . $t_id . '"><img src="kujundus/pildid/kustuta.png" title="' . $lang['kustuta'] . '" alt="' . $lang['kustuta'] . '" /></a>
			';
		}
		$sisu .= '
			<span id="' . $resource_name . '">
		';
		if (in_array($resource_name, $lemmikud)) {
			$sisu .= '<img src="kujundus/pildid/ok.png" alt="' . $lang['sinu_lemmik'] . '" title="' . $lang['sinu_lemmik'] . '">';
		} else {
			$sisu .= '<img src="kujundus/pildid/lemmik.png" title="' . $lang['lisa_lemmikutesse'] . '" alt="' . $lang['lisa_lemmikutesse'] . '" onclick="lisa_lemmik' . $jsLemmik . '(\'' . $xtabel . '\', \'' . $t_id . '\'); this.style.display=\'none\';" />';
		}
		$sisu .= '</span>';
		if ($minu_grupi_oigused == 3 && isset($uploadDir)) {
			if (isset($_FILES['userfile'])) {
				$allowedFiletypes = [];
				if ($xtabel == 'audio_trakk') {
					$allowedFiletypes = ['wav'];
				} elseif ($xtabel == 'foto') {
					$allowedFiletypes = ['jpg', 'jpeg'];
				} elseif ($xtabel == 'kasikiri') {
					$allowedFiletypes = ['jpg', 'jpeg', 'pdf'];
				} elseif ($xtabel == 'video_trakk') {
					$allowedFiletypes = ['avi', 'mp4', 'mov', 'mpg'];
				}
				$filename = $_FILES['userfile']['name'];
				if (strpos($filename, $r['arhiivinumber']) === false) {
					$filename = $r['arhiivinumber'] . '_' . $filename;
				}
				$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
				if (!is_dir($uploadDir)) {
					mkdir($uploadDir, 0777, true);
				}
				if (!is_writable($uploadDir)) {
					chmod($uploadDir, 0777);
				}
				if (!is_dir($uploadDir)) {
					$viga = 'Üleslaadimiseks pole sobilikku kataloogi olemas ja seda ei saanud luua';
				} elseif (!is_writable($uploadDir)) {
					$viga = 'Tekkis viga üleslaadimise asukoha failiõigustega';
				} elseif (!in_array($ext, $allowedFiletypes)) {
					$viga = 'Lubatud faililaiendid: ' . implode(', ', $allowedFiletypes) . ' (proovisid ' . $ext . ')';
				} elseif (file_exists($uploadDir . '/' . $filename)) {
					$viga = $filename . ' on juba olemas';
				} else {
					move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadDir . '/' . $filename);
					if (file_exists($uploadDir . '/' . $filename)) {
						$_SESSION['teade'] = 'Üles laadimine on edukalt lõpetatud';
						reload();
					} else {
						$viga = 'Üles laadimisel tekkis tundmatu viga';
					}
				}
			}
			$sisu .= ' <form action="" method="POST" enctype="multipart/form-data" style="display: inline;">
 				<input type="file" name="userfile" />
 				<input type="submit" value="Lae üles" />
 			</form>';
		}
		$sisu .= '</td></tr>';
		$sisu .= $sisu2;
		if (isset($r['arhiivinumber'])) {
			$searchWhere = [];
			if ($minu_grupi_oigused <= 0) {
				$searchWhere['oigused'] = 'avalik';
			}
			$naitaEelmine = null;
			$naitaJargmine = null;
			if ($jarjekorraParing = m_select($xtabel, $searchWhere, null, ' ORDER BY arhiivinumber ASC')) {
				while ($jarjekorraRida = m_a($jarjekorraParing)) {
					if ($jarjekorraRida['arhiivinumber'] == $r['arhiivinumber']) {
						if (isset($eelmineRida)) {
							$naitaEelmine = '<a href="naita.php?t=' . $xtabel . '&id=' . $eelmineRida['id'] . '"> &laquo; ' . $eelmineRida['arhiivinumber'] . '</a>';
							unset($eelmineRida);
						}
						$jargmineRida = 1;
					} elseif (isset($jargmineRida)) {
						$naitaJargmine = '<a href="naita.php?t=' . $xtabel . '&id=' . $jarjekorraRida['id'] . '">' . $jarjekorraRida['arhiivinumber'] . ' &raquo; </a>';
						break;
					} else {
						$eelmineRida = $jarjekorraRida;
					}
				}
			}
			if (isset($naitaEelmine) || isset($naitaJargmine)) {
				$sisu .= '<tr><td colspan="2">' . $naitaEelmine . ' ' . $naitaJargmine . '</td><tr>';
			}
		}
		foreach (get_full_columns($xtabel) as $column) {
			if (!in_array($column['Field'], $excludes)) {
				$n = trim($r[$column['Field']]); //eemalda whitespace'd ja muuda ära erisümbolid
				$tyyp = $column['Type']; //otsitava andme tüüp
				$tyyp = explode('(', $tyyp);
				$tyyp = $tyyp[0]; //int, varchar, date, datetime etc
				if ($column['Field'] == 'keel' && $xtabel != 'materjal_keel') {
					$keeled = [];
					$query = m_q('SELECT id, keel, lisaja FROM materjal_keel WHERE materjal = :materjal AND materjal_id = :materjal_id', [
						'materjal' => $xtabel,
						'materjal_id' => $r['id']
					]);
					while ($row = m_a($query)) {
						if ($sess && $sess['id'] == $row['lisaja'] || $minu_grupi_oigused > 2) {
							$row['keel'] = '<a href="naita.php?t=materjal_keel&id=' . $row['id'] . '">' . $row['keel'] . '</a>';
						}
						$keeled[] = $row['keel'];
					}
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . implode(', ', $keeled);
					if (in_array('materjal_keel', $tabelid) && $minu_grupi_oigused > 1) {
						$sisu .= '<br /><i><a href="lisa.php?t=materjal_keel&u=' . $xtabel . '&id=' . $t_id . '&ref=naita&typ=alam">' . $lang['lisa'] . '</a></i>';
					}
					$sisu .= '</td></tr>';
				} elseif (in_array($column['Field'], $allTables)) {
					$paring2 = m_select($column['Field'], ['id' => $n]);
					if ($paring2) {
						$rida2 = m_a($paring2);
						$must_show_columns = get_must_show_wide_columns($column['Field']);
						$sisu .= '
							<tr><td>' . kriips($column['Field']) . '</td><td>';
						if (in_array($column['Field'], $tabelid)) {
							$sisu .= '<a href="naita.php?t=' . $column['Field'] . '&id=' . $n . '">';
						}
						foreach ($must_show_columns as $value) {
							$sisu .= ' ' . $rida2[$value];
						}
						if (in_array($column['Field'], $tabelid)) {
							$sisu .= '</a>';
						}
						$sisu .= '</td></tr>';
						foreach (get_columns($column['Field']) as $rida3) {
							if (in_array($rida3, $allTables)) {
								if ($paring4 = m_select($rida3, ['id' => $rida2[$rida3]])) {
									$rida4 = m_a($paring4);
									$must_show_columns = get_must_show_wide_columns($rida3);
									$sisu .= '
										<tr><td>' . kriips($rida3) . '</td><td>';
									if (in_array($rida3, $tabelid)) {
										$sisu .= '<a href="naita.php?t=' . $rida3 . '&id=' . $rida2[$rida3] . '">';
									}
									foreach ($must_show_columns as $value) {
										$sisu .= ' ' . $rida4[$value];
									}
									if (in_array($rida3, $tabelid)) {
										$sisu .= '</a>';
									}
									$sisu .= '</td></tr>';
								}
							}
						}
					} else {
						$sisu .= '
							<tr><td>' . kriips($column['Field']) . '</td><td>' . $n . '</td></tr>';
					}
				} elseif ($tyyp == 'text') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . nl2br($n) . '</td></tr>';
				} elseif ($tyyp == 'date') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . date_to_est($n) . '</td></tr>';
				} elseif ($column['Field'] == 'ressursi_tyyp') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>';
					if ($n == '1') {
						$sisu .= $lang['fyysiline'];
					} elseif ($n == '2') {
						$sisu .= $lang['digitaalne'];
					}
					$sisu .= '</td></tr>';
				} elseif ($column['Field'] == 'fyysiliselt_olemas') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>';
					if ($n == '1') {
						$sisu .= $lang['olemas'];
					} elseif ($n == '2') {
						$sisu .= $lang['kadunud'];
					} elseif ($n == '3') {
						$sisu .= $lang['laenatud'];
					}
					$sisu .= '</td></tr>';
				} elseif ($column['Field'] == 'digitaalselt_olemas') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>';
					if ($n == '1') {
						$sisu .= $lang['jah'];
					} elseif ($n == '2') {
						$sisu .= $lang['ei'];
					}
					$sisu .= '</td></tr>';
				} elseif ($column['Field'] == 'kestus') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . pretty_length($n) . '</td></tr>';
				} elseif ($column['Field'] == 'lisaja') {
					if ($query = m_q('SELECT email FROM kasutaja WHERE id = :id', ['id' => $n])) {
						$row = m_a($query);
						$sisu .= '
							<tr><td>Lisaja</td><td><a href="naita.php?t=kasutaja&id=' . $n . '">' . $row['email'] . '</a></td></tr>';
					} else {
						$sisu .= '
							<tr><td>' . kriips($column['Field']) . '</td><td>' . $n . '</td></tr>';
					}
				} elseif ($column['Field'] == 'materjal') {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . kriips($n) . '</td></tr>';
				} elseif ($column['Field'] == 'materjal_id' && $paring2 = m_select($r['materjal'], ['id' => $n])) {
					$rida2 = m_a($paring2);
					$must_show_columns = get_must_show_wide_columns($r['materjal']);
					$sisu .= '
						<tr><td></td><td><a href="naita.php?t=' . $r['materjal'] . '&id=' . $n . '">';
					foreach ($must_show_columns as $value) {
						if ($value == 'kestus') {
							$sisu .= ' ' . pretty_length($rida2[$value]);
						} else {
							$sisu .= ' ' . $rida2[$value];
						}
					}
					$sisu .= '</a></td></tr>';
				} elseif ($column['Field'] == 'seostuvad_viited' && $n) {
					$arhNrid = explode(',', $n);
					$n = '';
					foreach ($arhNrid as $arhNr) {
						$arhNr1 = explode('-', substr($arhNr, 0, -1));
						$sarjaTahis = trim(str_replace(range(0,9), '', $arhNr1[0]));
						$materjal = m_a(m_q('SELECT materjali_tyyp FROM sari WHERE sarja_tahis = :sarja_tahis', ['sarja_tahis' => $sarjaTahis]));
						if ($materjal && table_exists($materjal['materjali_tyyp'])) {
							$viite_id = m_a(m_select($materjal['materjali_tyyp'], ['arhiivinumber' => trim($arhNr)], ['id']));
							if (!$viite_id) {
								$materjal['materjali_tyyp'] = str_replace('_trakk', '', $materjal['materjali_tyyp']);
								$viite_id = m_a(m_select($materjal['materjali_tyyp'], ['arhiivinumber' => trim($arhNr)], ['id']));
							}
							if (!$viite_id) {
								$materjal['materjali_tyyp'] = $materjal['materjali_tyyp'] . 'album';
								if ($viite_id = m_select($materjal['materjali_tyyp'], ['arhiivinumber' => trim($arhNr)], ['id'])) {
									$viite_id = m_a($viite_id);
								}
							}
							if ($viite_id['id']) {
								$n .= '<a href="naita.php?t=' . $materjal['materjali_tyyp'] . '&id=' . $viite_id['id'] . '">' . trim($arhNr) . '</a>, ';
							} else {
								$n .= trim($arhNr) . ', ';
							}
						} else {
							$n .= trim($arhNr) . ', ';
						}
					}
					$n = rtrim($n, ', ');
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . $n . '</td></tr>';
				} else {
					$sisu .= '
						<tr><td>' . kriips($column['Field']) . '</td><td>' . $n . '</td></tr>';
				}

				if ($roles_columns[0] == $column['Field']) { //digitaliseerijad
					$sisu .= '
						<tr><td>' . $lang['digitaliseerijad'] . '</td><td>';
					$query = m_q('SELECT * FROM digitaliseerija WHERE materjal = :materjal AND materjal_id = :materjal_id', [
						'materjal' => $xtabel,
						'materjal_id' => $t_id
					]);
					if (m_r($query)) {
						$must_show_columns = get_must_show_wide_columns('digitaliseerija');
						$must_show_columns2 = get_must_show_wide_columns('osaleja');
						while ($row = m_a($query)) {
							if ($query2 = m_q('SELECT * FROM osaleja WHERE id = :id', ['id' => $row['osaleja']])) {
								$row2 = m_a($query2);
								foreach ($must_show_columns as $value) {
									if ($value == 'osaleja') {
										if (in_array('osaleja', $tabelid)) {
											$sisu .= ' <a href="naita.php?t=osaleja&id=' . $row['osaleja'] . '">';
										}
										foreach ($must_show_columns2 as $value2) {
											$sisu .= ' ' . $row2[$value2];
										}
										if (in_array('osaleja', $tabelid)) {
											$sisu = rtrim($sisu, ' ');
											$sisu .= '</a>';
										}
									} elseif ($value == 'vanus' && ($row['vanus'] || $orig_aeg[$xtabel] && $row['vanus'] = vanus($row2['synniaasta'], $r[$orig_aeg[$xtabel]]))) {
										$sisu .= ' (' . $row['vanus'] . ')';
									} elseif ($value == 'materjal') {
									} else {
										$sisu .= ' ' . $row[$value];
									}
								}
								$sisu .= '<br />';
							}
						}
					}
					if (in_array('digitaliseerija', $tabelid) && $minu_grupi_oigused > 1) {
						$sisu .= '<i><a href="lisa.php?t=digitaliseerija&u=' . $xtabel . '&id=' . $t_id . '&ref=naita&typ=alam">' . $lang['lisa'] . '</a></i>';
					}
					$sisu .= '</td></tr>';
				}
				if ($roles_columns[1] == $column['Field']) { //rollid
					$order_by = ' ORDER BY FIELD(rolli_nimetus';
					$rolli_nimetus = m_q('SELECT id FROM rolli_nimetus ORDER BY jrknr DESC');
					while ($rolli_nimi = m_a($rolli_nimetus)) {
						$order_by .= ',\'' . $rolli_nimi['id'] . '\'';
					}
					$order_by .= ') DESC';
					$sisu .= '
						<tr><td>' . $lang['muud_rollid'] . '</td><td>';
					$query = m_q('SELECT * FROM roll WHERE materjal = :materjal AND materjal_id = :materjal_id' . $order_by, [
						'materjal' => $xtabel,
						'materjal_id' => $t_id
					]);
					if (m_r($query)) {
						$must_show_columns = get_must_show_wide_columns('roll');
						$must_show_columns2 = get_must_show_wide_columns('osaleja');
						while ($row = m_a($query)) {
							if ($query2 = m_q('SELECT * FROM osaleja WHERE id = :id', ['id' => $row['osaleja']])) {
								$row2 = m_a($query2);
								foreach ($must_show_columns as $value) {
									if ($value == 'osaleja') {
										if (in_array('osaleja', $tabelid)) {
											$sisu .= '<a href="naita.php?t=osaleja&id=' . $row['osaleja'] . '">';
										}
										$tykid = explode('{', OSALEJA_FORMAAT);
										foreach ($tykid as $count => $tykk) {
											if ($count == 0) {
												$sisu .= $tykk;
											} else {
												$tykid2 = explode('}', $tykk);
												$tykk2 = $tykid2[0];
												if ($row2[$tykk2]) {
													$sisu .= $row2[$tykk2];
													if ($count == count($tykid) - 1) {
														$sisu .= $tykid2[1];
													} else {
														for ($i = 1; isset($tykid[$count + $i]); $i++) {
															$tykid3 = explode('}', $tykid[$count + $i]);
															if ($row2[$tykid3[0]]) {
																$sisu .= $tykid2[1];
																break;
															}
														}
													}
												}
											}
										}
										if (in_array('osaleja', $tabelid)) {
											$sisu .= '</a>';
										}
									} elseif ($value == 'vanus' && ($row['vanus'] || $orig_aeg[$xtabel] && $row['vanus'] = vanus($row2['synniaasta'], $r[$orig_aeg[$xtabel]]))) {
										$sisu .= ' (' . $row['vanus'] . ')';
									} elseif ($value == 'materjal') {
									} elseif ($value == 'rolli_nimetus' && in_array('roll', $tabelid)) {
										$sisu .= ' <a href="naita.php?t=roll&id=' . $row['id'] . '">' . $row['rolli_nimetus'] . '</a> ';
									} else {
										$sisu .= ' ' . $row[$value] . ' ';
									}
								}
								$sisu .= '<br />';
							}
						}
					}
					if (in_array('roll', $tabelid) && $minu_grupi_oigused > 1) {
						$sisu .= '<i><a href="lisa.php?t=roll&u=' . $xtabel . '&id=' . $t_id . '&ref=naita&typ=alam">' . $lang['lisa'] . '</a></i>';
					}
					$sisu .= '</td></tr>';
				}
			}
		}
		foreach ($allTables as $tabel) {
			foreach (get_columns($tabel) as $column) {
				if ($column == 'lisaja') {
					$column = 'kasutaja';
				}
				if ($column == $xtabel) { //leiti alamtabel
					if ($column == 'kasutaja') {
						$column = 'lisaja';
					}
					$paring = m_select($tabel, [$column => $t_id]);
					if (m_r($paring)) {
						$must_show_columns = get_must_show_wide_columns($tabel);
						$sisu .= '
							<tr><td>' . kriips($tabel) . '</td><td>';
						while ($rida = m_a($paring)) {
							if (in_array($tabel, $tabelid)) {
								$sisu .= '<a href="naita.php?t=' . $tabel . '&id=' . $rida['id'] . '">';
							}
							if (in_array('vanus', $must_show_columns)) {
								$row2 = m_select($rida['materjal'], ['id' => $rida['materjal_id']]);
								if ($row2 && $row2 = m_a($row2)) {
									if (isset($row2[$orig_aeg[$rida['materjal']]])) {
										$orig_salvestus_aeg = $row2[$orig_aeg[$rida['materjal']]];
									}
								}
							}
							foreach ($must_show_columns as $value) {
								if ($value == 'kestus') {
									$sisu .= ' ' . pretty_length($rida[$value]);
								} elseif ($value == 'materjal') {
									if ($query = m_select($rida['materjal'], ['id' => $rida['materjal_id']])) {
										$sisu = rtrim($sisu, ' ');
										$sisu .= ': ' . kriips($rida['materjal']);
										$row = m_a($query);
										$must_show_columns2 = get_must_show_wide_columns($rida['materjal']);
										foreach ($must_show_columns2 as $value2) {
											if ($value2 == 'kestus') {
												$sisu .= ' ' . pretty_length($row[$value2]);
											} else {
												$sisu .= ' ' . $row[$value2];
											}
										}
									}
								} elseif (in_array($value, $nonSystemTables)) {
									if ($query = m_select($value, ['id' => $rida[$value]])) {
										$row = m_a($query);
										$must_show_columns2 = get_must_show_wide_columns($value);
										foreach ($must_show_columns2 as $value2) {
											if ($value2 == 'kestus') {
												$sisu .= ' ' . pretty_length($row[$value2]);
											} else {
												$sisu .= ' ' . $row[$value2];
											}
										}
									}
								} elseif ($value == 'vanus' && ($rida['vanus'] || $rida['vanus'] = vanus($r['synniaasta'] ?? null, $orig_salvestus_aeg))) {
									$sisu .= ' (' . $rida[$value] . ')';
								} else {
									$sisu .= ' ' . $rida[$value];
								}
							}
							if (in_array($tabel, $tabelid)) {
								$sisu .= '</a>';
							}
							$sisu .= '<br />';
							$orig_salvestus_aeg = '';
						}
						if ($minu_grupi_oigused > 1) {
							if ($xtabel == 'osaleja') {
								$sisu .= '<i><a href="lisa.php?t=' . $tabel . '&u=' . $xtabel . '&id=' . $t_id . '&ref=naita">' . $lang['lisa'] . '</a></i>';
							} else {
								$sisu .= '<i><a href="lisa.php?t=' . $tabel . '&u=' . $xtabel . '&id=' . $t_id . '&ref=naita&typ=alam">' . $lang['lisa'] . '</a></i>';
							}
						}
						$sisu .= '</td></tr>';
					}
				}
			}
		}
	} else {
		$viga = $lang['sellist_kirjet_ei_leitud'];
	}
} else {
	$viga = $lang['tabelit_ei_leitud'];
}
$sisu .= <<<SISU
	</table>
SISU;
include('kujundus.php');
