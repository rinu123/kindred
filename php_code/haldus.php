﻿<?php

include('base.php');
if (!$sess = sisse_logitud()) {
	header('Location: index.php');
	die();
}
if ($grupi_oigused = grupi_oigused($sess)) {
	if ($grupi_oigused < 3) {
		header('Location: index.php');
		die();
	}
} else {
	header('Location: index.php'); die();
}

if (isset($_POST['muudetav_tabel'])) {
	$systemTables = get_system_tables();

	if (isset($_POST['lisa_veerg'])) {
		$muudetav_tabel = $_POST['muudetav_tabel'];
			$sql = 'ALTER TABLE `' . $muudetav_tabel . '` ';
		$lisatav_veerg = strtolower($_POST['lisatav_veerg']);
			$sql .= 'ADD `' . $lisatav_veerg . '` ';
		$andmetyyp = $_POST['andmetyyp'];
			$sql .= $andmetyyp . ' ';
		$not_null = $_POST['not_null'];
			if ($not_null == 'jah') {
				$sql .= 'NOT NULL ';
			}
		$col_default_value = $_POST['col_default_value'];
			if (($col_default_value == '' || $col_default_value == 'NULL') && $not_null != 'jah') {
				$sql .= 'DEFAULT NULL ';
			} elseif ($col_default_value != '') {
				$sql .= 'DEFAULT \'' . $col_default_value . '\' ';
			}
		$show = $_POST['show'] ?? null;
		$wide = $_POST['wide'] ?? null;
			if ($show == 'jah' && $wide != 'jah') {
				$sql .= "COMMENT 'show' ";
			} elseif ($show == 'jah' && $wide == 'jah') {
				$sql .= "COMMENT 'show, wide' ";
			} elseif ($show != 'jah' && $wide == 'jah') {
				$sql .= "COMMENT 'wide' ";
			}
		$col_after = $_POST['col_after'];
			if ($col_after != '') {
				$sql .= 'AFTER `' . $col_after . '` ';
			}
		if (in_array($lisatav_veerg, $systemTables) || strlen($lisatav_veerg) < 3) {
			alter_t('', 'Kahjuks ' . $_POST['lisatav_veerg'] . ' ei sobi välja nimeks, vali midagi muud', $muudetav_tabel);
		} elseif (m_q($sql)) {
			if (m_q('ALTER TABLE `tabeli_keeled` ADD `' . $lisatav_veerg . '` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL')) {
				m_q('UPDATE `tabeli_keeled` SET `' . $lisatav_veerg . '`=\'' . kriips($lisatav_veerg, 0) . '\'');
			}
			alter_t('Väli nimega ' . $lisatav_veerg . ' lisatud', '', $muudetav_tabel);
		} else {
			alter_t('', m_err(), $muudetav_tabel);
		}
	} elseif (isset($_POST['eemalda_veerg'])) {
		$muudetav_tabel = $_POST['muudetav_tabel'];
		$eemaldatav_veerg = $_POST['eemaldatav_veerg'];
		$sql = 'ALTER TABLE `' . $muudetav_tabel . '` DROP `' . $eemaldatav_veerg . '` ';
		if (m_q($sql)) {
			alter_t('Väli nimega ' . $eemaldatav_veerg . ' kustutatud', '', $muudetav_tabel);
		} else {
			alter_t('', m_err(), $muudetav_tabel);
		}
	} elseif (isset($_POST['muudetav_veerg']) && !isset($_POST['muuda_veerg'])) {
		alter_t('', '', $_POST['muudetav_tabel'], $_POST['muudetav_veerg']);
	} elseif (isset($_POST['muuda_veerg'])) {
		$muudetav_tabel = $_POST['muudetav_tabel'];
			$sql = 'ALTER TABLE `' . $muudetav_tabel . '` CHANGE ';
		$muudetav_veerg = $_POST['muudetav_veerg'];
			$sql .= '`' . $muudetav_veerg . '` ';
		$veeru_nimi = strtolower($_POST['veeru_nimi']);
			$sql .= '`' . $veeru_nimi . '` ';
		$andmetyyp = $_POST['andmetyyp'];
			$sql .= $andmetyyp . ' ';
		$not_null = $_POST['not_null'] ?? null;
			if ($not_null == 'jah') {
				$sql .= 'NOT NULL ';
			}
		$col_default_value = $_POST['col_default_value'];
			if (($col_default_value == '' || $col_default_value == 'NULL') && $not_null != 'jah') {
				$sql .= 'DEFAULT NULL ';
			} elseif ($col_default_value != '') {
				$sql .= 'DEFAULT \'' . $col_default_value . '\' ';
			}
		$show = $_POST['show'] ?? null;
		$wide = $_POST['wide'] ?? null;
		$field_comment_before = $_POST['field_comment_before'];
			if ($show == 'jah' && $wide != 'jah') {
				if (strpos($field_comment_before, 'show') !== false) {
					$sql .= "COMMENT '" . str_replace(['wide, ', ', wide', 'wide', "\\", "'"], '', $field_comment_before) . "'";
				} elseif ($field_comment_before == '') {
					$sql .= "COMMENT 'show' ";
				} else {
					$sql .= "COMMENT '" . str_replace(['wide, ', ', wide', 'wide', "\\", "'"], '', $field_comment_before).", show'";
				}
			} elseif ($show == 'jah' && $wide == 'jah') {
				if (strpos($field_comment_before, 'show') !== false && strpos($field_comment_before, 'wide') !== false) {
					$sql .= "COMMENT '" . str_replace(["\\", "'"], "", $field_comment_before) . "'";
				} elseif (strpos($field_comment_before, 'show') !== false) {
					$sql .= "COMMENT '" . str_replace(["\\", "'"], "", $field_comment_before) . ", wide'";
				} elseif (strpos($field_comment_before, 'wide') !== false) {
					$sql .= "COMMENT '" . str_replace(["\\", "'"], "", $field_comment_before) . ", show'";
				} elseif ($field_comment_before == '') {
					$sql .= "COMMENT 'show, wide' ";
				} else {
					$sql .= "COMMENT '" . str_replace(["\\", "'"], '', $field_comment_before) . ", show, wide'";
				}
			} elseif ($show != 'jah' && $wide == 'jah') {
				if (strpos($field_comment_before, 'wide') !== false) {
					$sql .= "COMMENT '" . str_replace(['show, ', ', show', 'show', "\\", "'"], '', $field_comment_before) . "'";
				} elseif ($field_comment_before == '') {
					$sql .= "COMMENT 'wide' ";
				} else {
					$sql .= "COMMENT '" . str_replace(['show, ', ', show', 'show', "\\", "'"], '', $field_comment_before).", wide'";
				}
			} else {
				$field_comment_before = str_replace(['show, ', ', show', 'show', 'wide, ', ', wide', 'wide', "\\", "'"], '', $field_comment_before);
				$sql .= "COMMENT '" . $field_comment_before . "'";
			}
		$col_after = $_POST['col_after'];
			if ($col_after != '') {
				$sql .= 'AFTER `' . $col_after . '` ';
			}
		if (in_array($veeru_nimi, $systemTables) || strlen($veeru_nimi) < 3) {
			alter_t('', 'Kahjuks ' . $_POST['veeru_nimi'] . ' ei sobi välja nimeks, vali midagi muud', $muudetav_tabel, $muudetav_veerg);
		} elseif (m_q($sql)) {
			if (!array_key_exists($veeru_nimi, $lang_t)) {
				if (m_q('ALTER TABLE `tabeli_keeled` ADD `' . $veeru_nimi . '` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL')) {
					m_q('UPDATE `tabeli_keeled` SET `' . $veeru_nimi . '`=\'' . kriips($veeru_nimi, 0) . '\'');
				}
			}
			alter_t('Väli nimega ' . $muudetav_veerg . ' muudetud', '', $muudetav_tabel, $veeru_nimi);
		} else {
			alter_t('', m_err(), $muudetav_tabel, $muudetav_veerg);
		}
	} elseif (isset($_POST['muuda_avalikuks'])) {
		$sql = 'ALTER TABLE `' . $_POST['muudetav_tabel'] . "` COMMENT = 'public'";
		if (m_q($sql)) {
			alter_t('Tabel avalikuks muudetud', '', $_POST['muudetav_tabel']);
		} else {
			alter_t('', m_err(), $_POST['muudetav_tabel']);
		}
	} elseif (isset($_POST['eemalda_avalikest'])) {
		$sql = 'ALTER TABLE `' . $_POST['muudetav_tabel'] . "` COMMENT = ''";
		if (m_q($sql)) {
			alter_t('Tabel avalike hulgast eemaldatud', '', $_POST['muudetav_tabel']);
		} else {
			alter_t('', m_err(), $_POST['muudetav_tabel']);
		}
	} elseif (isset($_POST['muuda_infoks'])) {
		$sql = 'ALTER TABLE `' . $_POST['muudetav_tabel'] . "` COMMENT = 'info'";
		if (m_q($sql)) {
			alter_t('Tabel muudetud valikvastuste tabeliks', '', $_POST['muudetav_tabel']);
		} else {
			alter_t('', m_err(), $_POST['muudetav_tabel']);
		}
	} elseif (isset($_POST['eemalda_infost'])) {
		$sql = "ALTER TABLE `" . $_POST['muudetav_tabel'] . "` COMMENT = ''";
		if (m_q($sql)) {
			alter_t('Valikvastuste tabelite hulgast eemaldatud', '', $_POST['muudetav_tabel']);
		} else {
			alter_t('', m_err(), $_POST['muudetav_tabel']);
		}
	} elseif (isset($_POST['muuda_tabeli_nimi'])) {
		$sql = 'RENAME TABLE `' . $_POST['muudetav_tabel'] . '` TO `' . $_POST['uus_nimi'] . '` ;';
		if (m_q($sql)) {
			if (m_q('ALTER TABLE `tabeli_keeled` ADD `' . $_POST['uus_nimi'] . '` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL')) {
				m_q('UPDATE `tabeli_keeled` SET `' . $_POST['uus_nimi'] . '`=\'' . kriips($_POST['uus_nimi'], 0) . '\'');
			}
			alter_t('Tabeli uus nimi on ' . $_POST['uus_nimi'], '', $_POST['uus_nimi']);
		} else {
			alter_t('', m_err(), $_POST['muudetav_tabel']);
		}
	} elseif (isset($_POST['osalejate_asukohad'])) {
		$teade = '';
		$viga = '';
		$digi_vali = $_POST['digi_vali'];
		$roll_vali = $_POST['roll_vali'];
		$valjade_rollid = get_roles_columns($_POST['muudetav_tabel']);
		if ($valjade_rollid[0] != '') {
			$create_column22 = show_create_column($_POST['muudetav_tabel'], $valjade_rollid[1]);
			$tykid = explode('COMMENT ', $create_column22);
			$kommentaar = $tykid[1];
			$comment = str_replace(['digitaliseerija, ', ', digitaliseerija', 'digitaliseerija', "\\", "'"], '', $kommentaar);
			m_q('ALTER TABLE `' . $_POST['muudetav_tabel'] . '` CHANGE `' . $valjade_rollid[1] . '` ' . $tykid[0] . " COMMENT '" . $comment . "'");
		}
		if ($valjade_rollid[1] != '') {
			$create_column33 = show_create_column($_POST['muudetav_tabel'], $valjade_rollid[2]);
			$tykid = explode('COMMENT ', $create_column33);
			$kommentaar = $tykid[1];
			$comment = str_replace(['roll, ', ', roll', 'roll', "\\", "'"], '', $kommentaar);
			m_q('ALTER TABLE `' . $_POST['muudetav_tabel'] . '` CHANGE `' . $valjade_rollid[2] . '` ' . $tykid[0] . " COMMENT '" . $comment . "'");
		}
		if ($digi_vali != '') {
			$create_column2 = show_create_column($_POST['muudetav_tabel'], $digi_vali);
			$tykid = explode('COMMENT ', $create_column2);
			if (strpos($create_column2, 'COMMENT') !== false) {
				$kommentaar = $tykid[1];
				if (strpos($kommentaar, 'digitaliseerija') !== false) {
					$comment = $kommentaar;
				} else {
					$comment = "'" . str_replace("'", '', $kommentaar) . ", digitaliseerija'";
				}
			} else {
				$comment = "'digitaliseerija'";
			}
			$sql = 'ALTER TABLE `' . $_POST['muudetav_tabel'] . '` CHANGE `' . $digi_vali . '` ' . $tykid[0] . ' COMMENT ' . $comment;
			if (m_q($sql)) {
				$teade .= 'Digitaliseerija asukoht muudetud<br />';
			} else {
				$viga .= 'Digitaliseerija asukohta ei saanud muuta: ' . m_err() . '<br />';
			}
		}
		if ($roll_vali != '') {
			$create_column3 = show_create_column($_POST['muudetav_tabel'], $roll_vali);
			$tykid = explode('COMMENT ', $create_column3);
			if (strpos($create_column3, 'COMMENT') !== false) {
				$kommentaar = $tykid[1];
				if (strpos($kommentaar, 'roll') !== false) {
					$comment = $kommentaar;
				} else {
					$comment = "'" . str_replace("'", '', $kommentaar) . ", roll'";
				}
			} else {
				$comment = "'roll'";
			}
			$sql = 'ALTER TABLE `' . $_POST['muudetav_tabel'] . '` CHANGE `' . $roll_vali . '` ' . $tykid[0] . ' COMMENT ' . $comment;
			if (m_q($sql)) {
				$teade .= 'Muude rollide asukoht muudetud<br />';
			} else {
				$viga .= 'Muude rollide asukohta ei saanud muuta: ' . m_err() . '<br />';
			}
		}
		if ($viga != '') {
			$viga .= $teade;
			alter_t('', $viga, $_POST['muudetav_tabel']);
		} else {
			alter_t($teade, '', $_POST['muudetav_tabel']);
		}
	} else {
		alter_t('', '', $_POST['muudetav_tabel']);
	}
} elseif (isset($_POST['muudetav_tabel2'])) {
	if (isset($_POST['submit_paring'])) {
		$muudetav_tabel = $_POST['muudetav_tabel2'];
		$uus_vaartus = $_POST['uus_vaartus'];
		if (!$uus_vaartus) {
			muuda_ridu('', 'Uue väärtuse välja valik tegemata', $muudetav_tabel);
			die();
		}
		$uus_vaartus_text = $_POST['uus_vaartus_text'];
		$tingimus = $_POST['tingimus'];
		if (!$tingimus) {
			muuda_ridu('', 'Tingimuse välja valik tegemata', $muudetav_tabel);
			die();
		}
		$tingimus_valik = $_POST['tingimus_valik'];
		$tingimus_text = $_POST['tingimus_text'];
		if ($tingimus_valik == 0) {
			$tingimus_valik = ' LIKE ';
		} elseif ($tingimus_valik == 1) {
			$tingimus_valik = ' LIKE ';
			$tingimus_text = '%' . $tingimus_text . '%';
		} elseif ($tingimus_valik == 2) {
			$tingimus_valik = '<';
		} elseif ($tingimus_valik == 3) {
			$tingimus_valik = '>';
		} else {
			$tingimus_valik = '=';
		}
		m_q('SET @uids := null');
		$query = 'UPDATE ' . $muudetav_tabel . ' SET `' . $uus_vaartus . "`='" . $uus_vaartus_text . "' WHERE `" . $tingimus . '`' . $tingimus_valik . "'" . $tingimus_text . "' AND (SELECT @uids := CONCAT_WS(',', id, @uids))";
		if (m_q($query)) {
			$count = m_a(m_q('SELECT ROW_COUNT() AS count, @uids AS uids'));
			$uids = explode(',', $count['uids']);
			$muudetud = '';
			$ucount = 1;
			foreach ($uids as $uid) {
				if ($muudetud != '') {
					$muudetud .= ', ';
				}
				if ($ucount > 5) {
					$muudetud .= '...';
					break;
				} else {
					$muudetud .= '<a href="naita.php?t=' . $muudetav_tabel . '&id=' . $uid . '" target="_blank">' . $uid . '</a>';
				}
				$ucount++;
			}
			muuda_ridu('Muudeti ' . $count['count'] . ' rida:<br />' . $muudetud, '', $muudetav_tabel);
		} else {
			muuda_ridu('', m_err(), $muudetav_tabel);
		}
	} else {
		muuda_ridu('', '', $_POST['muudetav_tabel2']);
	}
} elseif (isset($_POST['uus_tabel'])) {
    $tabeli_nimi = $_POST['tabeli_nimi'];
	$sql = 'CREATE TABLE `' . $tabeli_nimi . '` (
	';
	if ($_POST['avalik'] == 'info') {
		$sql .= "`id` varchar(225) NOT NULL COMMENT 'show, wide',
	";
	} else {
		$sql .= '`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	';
	}
	if ($_POST['avalik'] == 'public') {
		$sql.= "`oigused` varchar(45) NOT NULL DEFAULT 'avalik',
		";
	}
	$sql .= "`andmebaasi_lisatud` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lisaja` int(10) DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";
	if ($_POST['avalik'] == 'public') {
		$sql .= " COMMENT='public'";
	} elseif ($_POST['avalik'] == 'info') {
		$sql .= " COMMENT='info'";
	}
	if (m_q($sql)) {
		if (m_q('ALTER TABLE `tabeli_keeled` ADD `' . $tabeli_nimi . '` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL')) {
			m_q('UPDATE `tabeli_keeled` SET `' . $tabeli_nimi . "`='" . kriips($tabeli_nimi, 0) . "'");
		}
		create_t('Loodi tabel nimega ' . $tabeli_nimi);
	} else {
		create_t('', m_err());
	}
} elseif (isset($_POST['kustuta_tabel'])) {
	$tabeli_nimi = $_POST['tabeli_nimi'];
	$tables = get_public_tables();
	if (in_array($tabeli_nimi, $tables)) {
		if (m_q('DROP TABLE ' . $tabeli_nimi)) {
			drop_t('Kustutati tabel nimega ' . $tabeli_nimi);
		} else {
			drop_t('', m_err());
		}
	} else {
		drop_t('', 'Kustutamiseks peab tabel avalik olema.');
	}
} elseif (isset($_POST['uus_grupp'])){
	$grupi_nimi = $_POST['grupi_nimi'];
	$grupi_oigused = $_POST['grupi_oigused'];
	if ($grupi_nimi == '') {
		create_g('', 'Anna grupile nimi');
	} else {
		if (m_insert('grupp', [
			'nimetus' => $grupi_nimi,
			'grupi_oigused' => $grupi_oigused,
			'lisaja' => $sess['id']
		], ['andmebaasi_lisatud' => 'NOW()'])) {
			create_g('Grupp ' . $grupi_nimi . ' loodud');
		} else {
			create_g('', m_err());
		}
	}
} elseif (isset($_POST['uus_kasutaja'])) {
	$sql = [];
	$viga = '';
	if ($_POST['kasutaja_eesnimi'] != '') {
		$sql['eesnimi'] = $_POST['kasutaja_eesnimi'];
	}
	if ($_POST['kasutaja_perenimi'] != '') {
		$sql['perenimi'] = $_POST['kasutaja_perenimi'];
	}
	if ($_POST['kasutaja_email'] == '') {
		$viga .= 'Kasutajal peab kindlasti olema email.<br />';
	} else {
		$sql['email'] = $_POST['kasutaja_email'];
	}
	if ($_POST['kasutaja_grupp'] == '') {
		$viga .= 'Kasutajal peab kindlasti olema grupp.<br />';
	} else {
		$sql['grupp'] = $_POST['kasutaja_grupp'];
	}
	if ($_POST['kasutaja_aegub'] != '') {
		if ($aegub = date_to_mysql($_POST['kasutaja_aegub'])) {
			$sql['aegub'] = $aegub;
		} else {
			$viga .= 'Aegumisaja formaat on vale.<br />';
		}
	}
	if ($_POST['kirja_keel'] == '' || !lang_exists($_POST['kirja_keel'])) {
		$viga .= 'Vali sobilik kirja keel.<br />';
	}
	if ($viga == '') {
		$kirja_pealkiri = m_a(m_q('SELECT uus_kasutaja_kirja_pealkiri FROM ui_keeled WHERE id = :id', ['id' => $_POST['kirja_keel']]));
		$newpass = '';
		for ($i = 0; $i < 10; $i++) {
			$newpass .= chr(rand(65, 90));
		}
		$sql['parool'] = password_hash($newpass, PASSWORD_BCRYPT);
		$sql['lisaja'] = $sess['id'];
		if (m_insert('kasutaja', $sql, ['andmebaasi_lisatud' => 'NOW()'])) {
			$replace = ['{PAROOL}', '{EESNIMI}', '{PERENIMI}', '{EMAIL}', '{AEGUB}'];
			$with = [$newpass, $_POST['kasutaja_eesnimi'], $_POST['kasutaja_perenimi'], $_POST['kasutaja_email'], $_POST['kasutaja_aegub']];
			if (mymail($_POST['kasutaja_email'], $kirja_pealkiri['uus_kasutaja_kirja_pealkiri'], str_replace($replace, $with, tekst('uuskonto')))) {
				$saadetud = ', parool saadetud emailile';
			} else {
				$saadetud = ', parooli saatmine ebaõnnestus';
			}
			create_u('Lisatud uus kasutaja, emailiga ' . $_POST['kasutaja_email'] . $saadetud);
		} else {
			create_u('', m_err());
		}
	} else {
		create_u('', $viga);
	}
} elseif (isset($_GET['liida_sarnased_osalejad'])) {
	$eesnimi = $_GET['eesnimi'];
	$perenimi = $_GET['perenimi'];
	$query = m_q('SELECT * FROM osaleja WHERE eesnimi = :eesnimi AND perenimi = :perenimi ORDER BY id DESC', [
		'eesnimi' => $eesnimi,
		'perenimi' => $perenimi
	]);
	if (m_r($query) < 2) {
		sarnased_osalejad('', 'Sarnaseid osalejaid ei leitud!');
	} else {
		$esimene = m_a($query);
		$e_id = $esimene['id'];
		while ($teised = m_a($query)) {
			foreach (get_columns('osaleja') as $column) {
				if (strlen($esimene[$column]) < strlen($teised[$column])) {
					m_update('osaleja', [
						$column => $teised[$column]
					], [
						'id' => $e_id
					]);
				}
			}
			m_update('digitaliseerija', [
				'osaleja' => $e_id
			], [
				'osaleja' => $teised['id']
			]);
			m_update('roll', [
				'osaleja' => $e_id
			], [
				'osaleja' => $teised['id']
			]);
			m_delete('osaleja', [
				'id' => $teised['id']
			]);
		}
		sarnased_osalejad('Nimega ' . $eesnimi . ' ' . $perenimi . ' on nüüd üks osaleja, kõik rollid on kokku koondatud. <a href="naita.php?t=osaleja&id=' . $e_id . '">Vaata</a>');
	}
} elseif (isset($_GET['baasi_haldus']) && $_GET['baasi_haldus'] != 0) {
	$tegevus = $_GET['baasi_haldus'];
	switch ($tegevus) {
		case '1'; {
			show_t();
			break;
		}
		case '2'; {
			create_t();
			break;
		}
		case '3';{
			drop_t();
			break;
		}
		case '4';{
			alter_t();
			break;
		}
		case '5';{
			erilised();
			break;
		}
		case '6';{
			muuda_ridu();
			break;
		}
		default;
			break;
	}
} elseif (isset($_GET['kasutaja_haldus']) && $_GET['kasutaja_haldus'] != 0) {
	$tegevus = $_GET['kasutaja_haldus'];
	switch ($tegevus) {
		case '1'; {
			show_u();
			break;
		}
		case '2'; {
			create_u();
			break;
		}
		default;
			break;
	}
} elseif (isset($_GET['grupi_haldus']) && $_GET['grupi_haldus'] != 0) {
	$tegevus = $_GET['grupi_haldus'];
	switch ($tegevus) {
		case '1';{
			show_g();
			break;
		}
		case '2';{
			create_g();
			break;
		}
		default;
			break;
	}
} elseif (isset($_GET['show_full_content'])) {
	show_full_content_helper($_GET['show_full_content']);
} elseif (isset($_GET['sarnased_osalejad'])) {
	sarnased_osalejad();
} elseif (isset($_GET['keelte_haldus'])) {
	keelte_haldus();
} else {
$pealkiri1 = $lang['haldus'];
$sisu = <<<SISU
Vali paremalt menüüst haldustoiming.<br />
Ole ettevaatlik, siinsed tegevused võivad tekitada pöördumatut kahju.<br />
Enne tegutsemist loe hoolikalt <a href="abi.php?t=haldus" target="_blank">haldusjuhendit</a>.
SISU;
include('kujundus.php');
}

function show_t() {
	global $lang;
	$publicTables = get_public_tables();
	$systemTables = get_system_tables();
	$pealkiri1 = $lang["haldus"];
	$sisu = "<table class='colspace'>";
	$sisu .= "<tr><th>Tabel</th><th>Kirjeid</th><th>Märge</th></tr>";
	$tabelid = get_table_names();
	foreach ($tabelid as $tabel) {
		$sisu .= '<tr>
		<td>' . $tabel . '</td>
		<td>
			<a href="haldus.php?show_full_content=' . $tabel . '">' . m_r(m_select($tabel)) . '</a>
		</td>
		<td>';
		if (in_array($tabel, $publicTables)) {
			$sisu .= 'avalik tabel';
		} elseif (in_array($tabel, $systemTables)) {
			$sisu .= 'süsteemne tabel';
		}
		$sisu .= '</td>
		</tr>';
	}
	$sisu .= '</table>';
	include('kujundus.php');
}

function create_t($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}
	$sisu = <<<SISU
		<form action="" method="POST">
			<h2>Uus tabel</h2>
			<table class="colspace">
			<tr><td>Uue tabeli nimi:</td><td><input type="text" name="tabeli_nimi"></td></tr>
			<tr><td>Tavaline tabel:</td><td><input type="radio" name="avalik" value="" /></td></tr>
			<tr><td>Avalik tabel:</td><td><input type="radio" name="avalik" value="public" /></td></tr>
			<tr><td>Valikvastuste tabel:</td><td><input type="radio" name="avalik" value="info" /></td></tr>
			<tr><td colspan="2">Avalikest tabelitest saavad välja logitud kasutajad avalikke andmeid otsida.<br />
			Avalikel tabelitel on osalejad ja rollid.</td></tr>
			<tr><td colspan="2">Valikvastuste tabelid on näiteks rolli_nimetus, kihelkond, keel ja too_liik.</td></tr>
			<tr><td></td><td><input type="submit" name="uus_tabel" value="Tee uus tabel"></td></tr>
			</table>
		</form>
SISU;
	include('kujundus.php');
}

function drop_t($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}
	$tables_array = get_public_tables();
	$sisu = <<<SISU
		<form action="" method="POST">
			<h2>Kustuta tabel</h2>
			<table class="colspace">
			<tr><td>Tabeli nimi:</td><td>
			<select name="tabeli_nimi">
SISU;
$sisu .= generate_options($tables_array);
$sisu .= <<<SISU
			</select></td></tr>
			<tr><td></td><td><input type="submit" value="Eemalda tabel" name="kustuta_tabel" /></td></tr>
			<tr><td colspan="2">Tabeli kustutamiseks peab tabel olema avalikuks märgitud.<br />
			Süsteemseid tabeleid ei saa kustutada.</td></tr>
			</table>
		</form>
SISU;
include('kujundus.php');
}

function alter_t($teade = '', $viga = '', $tabel = '', $vali = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	$nonSystemTables = get_non_system_tables();
	$tables = generate_options($nonSystemTables, $tabel);

	if ($tabel == '') {
		$sisu = <<<SISU
		<form action="" method="POST">
			Vali tabel:
			<select name="muudetav_tabel" onchange="this.form.submit()">
			$tables
			</select>
		</form>
SISU;
	} else {
		$pealkiri1 .= ': tabel ' . $tabel;
		$columns = get_columns($tabel, excludes(0));
		$columns = generate_options($columns);
		$columns1 = get_columns($tabel, excludes(0));
		$columns1 = generate_options($columns1, $vali);
		$columns2 = get_columns($tabel);
		$columns2 = generate_options($columns2);
		$fieldTypeOptions = get_field_type_options();
		if ($vali != '') {
			$create = show_create_column($tabel, $vali);
			$tykid = explode(' ', $create);

			if (strpos($create, 'tinyint') !== false) {
				$fieldTypeOptions['TINYINT(4) unsigned']['selected'] = true;
			} elseif (strpos($create, 'bigint') !== false) {
				$fieldTypeOptions['BIGINT(225) unsigned']['selected'] = true;
			} elseif (strpos($create, 'int') !== false) {
				$fieldTypeOptions['INT(11) unsigned']['selected'] = true;
			} elseif (strpos($create, 'double') !== false) {
				$fieldTypeOptions['DOUBLE']['selected'] = true;
			} elseif (strpos($create, 'datetime') !== false) {
				$fieldTypeOptions['DATETIME']['selected'] = true;
			} elseif (strpos($create, 'date') !== false) {
				$fieldTypeOptions['DATE']['selected'] = true;
			} elseif (strpos($create, 'text') !== false) {
				$fieldTypeOptions['TEXT']['selected'] = true;
			} else {
				$fieldTypeOptions['VARCHAR(225)']['selected'] = true;
			}

			if (strpos($create, 'DEFAULT') !== false) {
				$i = 0;
				foreach ($tykid as $tykk) {
					if ($i == 1) {
						$col_default_value = str_replace("'", '', $tykk);
						if ($col_default_value === 'NULL') {
							$col_default_value = '';
						}
						break;
					}
					if ($tykk == 'DEFAULT') {
						$i = 1;
					}
				}
			}

			if (strpos($create, 'NOT NULL') !== false) {
				$not_null = ' checked';
			}

			if (strpos($create, 'COMMENT') !== false) {
				$tykid = explode('COMMENT ', $create);
				$kommentaar = $tykid[1];
				if (strpos($kommentaar, 'show') !== false) {
					$must_show = ' checked';
				}
			}

			if (strpos($create, 'COMMENT') !== false) {
				$tykid = explode('COMMENT ', $create);
				$kommentaar = $tykid[1];
				if (strpos($kommentaar, 'wide') !== false) {
					$show_wide = ' checked';
				}
			}
		}
		$fieldTypeOptionsHtml1 = get_field_type_options_html(get_field_type_options());
		$fieldTypeOptionsHtml2 = get_field_type_options_html($fieldTypeOptions);
		$col_default_value = $col_default_value ?? '';
		$not_null = $not_null ?? '';
		$must_show = $must_show ?? '';
		$show_wide = $show_wide ?? '';
		$kommentaar = $kommentaar ?? '';
		$sisu = <<<SISU
			<form action="" method="POST">
				<input type="hidden" name="muudetav_tabel" value="{$tabel}">
				<h2>Lisa väli</h2>
				<table class="colspace">
				<tr>
					<td>Uue välja nimi:</td>
					<td><input type="text" name="lisatav_veerg"></td>
				</tr>
				<tr>
					<td>Järjekorras peale:</td>
					<td>
						<select name="col_after">
							{$columns2}
						</select>
					</td>
				</tr>
				<tr>
					<td>Uue välja tüüp:</td>
					<td>
						<select name="andmetyyp">
							{$fieldTypeOptionsHtml1}
						</select>
					</td>
				</tr>
				<tr>
					<td>Vaikimisi väärtus:</td>
					<td><input type="text" name="col_default_value"></td>
				</tr>
				<tr>
					<td>Kohustuslik väli:</td>
					<td><input type="checkbox" name="not_null" value="jah" /></td>
				</tr>
				<tr>
					<td>Kiirvaates näidatav väli:</td>
					<td><input type="checkbox" name="show" value="jah" /></td>
				</tr>
				<tr>
					<td>Laiendatud vaates näidatav väli:</td>
					<td><input type="checkbox" name="wide" value="jah" /></td>
				</tr>
                <tr>
                	<td></td>
                	<td><input type="submit" value="Lisa väli" name="lisa_veerg" /></td>
                </tr>
				</table>
			</form>
			<br />
			<form action="" method="POST">
				<input type="hidden" name="muudetav_tabel" value="{$tabel}">
				<h2>Eemalda väli</h2>
				<table class="colspace">
				<tr><td><select name="eemaldatav_veerg">
				{$columns}
				</select></td></tr>
				<tr><td><input type="submit" value="Eemalda väli" name="eemalda_veerg" /></td></tr>
				</table>
			</form>
			<br />
			<form action="" method="POST">
				<input type="hidden" name="muudetav_tabel" value="{$tabel}">
				<h2>Muuda välja</h2>
				<table class="colspace">
				<tr><td>Muudetav väli:</td><td><select name="muudetav_veerg" onchange="this.form.submit()">
				{$columns1}
				</select></td></tr>
				<tr><td>Välja nimi:</td><td><input type="text" name="veeru_nimi" value="{$vali}"></td></tr>
				<tr><td>Järjekorras peale:</td><td><select name="col_after">
				{$columns2}
				</select></td></tr>
				<tr>
					<td>Välja tüüp:</td>
					<td>
						<select name="andmetyyp">
							{$fieldTypeOptionsHtml2}
						</select>
					</td>
				</tr>
				<tr>
					<td>Vaikimisi väärtus:</td>
					<td><input type="text" name="col_default_value" value="{$col_default_value}"></td>
				</tr>
				<tr>
					<td>Kohustuslik väli:</td>
					<td><input type="checkbox" name="not_null" value="jah" {$not_null}/></td>
				</tr>
				<tr>
					<td>Kiirvaates näidatav väli:</td>
					<td><input type="checkbox" name="show" value="jah" {$must_show}/></td>
				</tr>
				<tr>
					<td>Laiendatud vaates näidatav väli:</td>
					<td><input type="checkbox" name="wide" value="jah" {$show_wide}/></td>
				</tr>
				<input type="hidden" name="field_comment_before" value="{$kommentaar}">
				<tr><td></td><td><input type="submit" value="Muuda välja" name="muuda_veerg" /></td></tr>
				</table>
			</form>
			<br />
SISU;
		if (in_array($tabel, $nonSystemTables)) {
			$sisu .= '<form action="" method="POST">
			<input type="hidden" name="muudetav_tabel" value="' . $tabel . '">
			<h2>Muud seaded</h2>
			<table class="colspace">
				<tr><td>';
			$public_tables = get_public_tables();
			$info_tables = get_info_tables();
			if (in_array($tabel, $public_tables)) {
				$sisu .= '<input type="submit" name="eemalda_avalikest" value="Eemalda avalike tabelite hulgast">';
			} else {
				$sisu .= '<input type="submit" name="muuda_avalikuks" value="Muuda tabel avalikuks">';
			}
			if (in_array($tabel, $info_tables)) {
				$sisu .= '<input type="submit" name="eemalda_infost" value="Eemalda valikvastuste tabelite hulgast">';
			} else {
				$sisu .= '<input type="submit" name="muuda_infoks" value="Muuda tabel valikvastuste tabeliks">';
			}
			$sisu .= '<br />
				Avalikest tabelitest saavad välja logitud kasutajad avalikke andmeid otsida. Avalikel tabelitel on osalejad ja rollid.</td></tr>';
			$sisu .= '
				<tr><td><br /></td></tr>
			<tr><td>Nimeta tabel ümber:
			<input type="text" name="uus_nimi" value="' . $tabel . '">
			<input type="submit" name="muuda_tabeli_nimi" value="Nimeta ümber"></td></tr>
			</table>
			<br />';
			if (in_array($tabel, $public_tables)) {
				$roles_columns = get_roles_columns($tabel);
				$columns = get_columns($tabel, excludes(0));
				$digiColumns = generate_options($columns, $roles_columns[0]);
				$rollColumns = generate_options($columns, $roles_columns[1]);
				$sisu .= <<<SISU
				<h2>Osalejate kuvamine</h2>
				<table class='colspace'>
				<tr><td>Digitaliseerijad peale:</td><td><select name="digi_vali">{$digiColumns}</select></td></tr>
				<tr><td>Muud rollid peale:</td><td><select name="roll_vali">{$rollColumns}</select></td></tr>
				<tr><td></td><td><input type="submit" name="osalejate_asukohad" value="Salvesta"></td></tr>
				</table>
				<br />
SISU;
			}
			$sisu .= '</form>';
		}
	}
include('kujundus.php');
}

function muuda_ridu($teade = '', $viga = '', $tabel = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	$nonSystemTables = get_non_system_tables();
	$tables = generate_options($nonSystemTables, $tabel);

	if ($tabel == '') {
		$sisu = <<<SISU
		<form action="" method="POST">
			Vali tabel:
			<select name="muudetav_tabel2" onchange="this.form.submit()">
			$tables
			</select>
		</form>
SISU;
	} else {
		$columns = get_columns($tabel, excludes(0));
		$columns = generate_options($columns);
		$sisu = <<<SISU
			<form action="" method="POST">
				<input type="hidden" name="muudetav_tabel2" value="$tabel" />
				<h2>Muuda ridu</h2>
				<table class="colspace">
				<tr><th></th><th>Väli</th><th></th><th>Väärtus</th></tr>
				<tr><td>Uus väärtus:</td><td><select name="uus_vaartus">$columns</select></td><td>=</td><td><input type="text" name="uus_vaartus_text" /></td></tr>
				<tr><td>Tingimus:</td><td><select name="tingimus">$columns</select></td><td>
					<select name="tingimus_valik">
						<option value="0">võrdub</option>
						<option value="1">sisaldab</option>
						<option value="2">väiksem kui</option>
						<option value="3">suurem kui</option>
					</select>
				</td><td><input type="text" name="tingimus_text" /></td></tr>
                <tr><td></td><td></td><td></td><td><input type="submit" value="Muuda read" name="submit_paring" /></td></tr>
				</table>
				Siin andmete ilustamist nagu mujal süsteemis ei toimu. Kahtluse korral kontrolli enne muutuste tegemist mis kujul andmed andmebaasis on (näiteks valikuga Vaata tabeleid).
			</form>
SISU;
	}
	include('kujundus.php');
}

function show_u() {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	$sisu = show_full_content('kasutaja');
	include('kujundus.php');
}

function create_u($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	if (isset($_POST['kasutaja_eesnimi'])) {
		$eesnimi = $_POST['kasutaja_eesnimi'];
	} else {
		$eesnimi = '';
	}
	if (isset($_POST['kasutaja_perenimi'])) {
		$perenimi = $_POST['kasutaja_perenimi'];
	} else {
		$perenimi = '';
	}
	if (isset($_POST['kasutaja_email'])) {
		$email = $_POST['kasutaja_email'];
	} else {
		$email = '';
	}
	if (isset($_POST['kasutaja_grupp'])) {
		$kasutaja_grupp = $_POST['kasutaja_grupp'];
	} else {
		$kasutaja_grupp = 0;
	}
	if (isset($_POST['kasutaja_aegub'])) {
		$aegub = $_POST['kasutaja_aegub'];
	} else {
		$aegub = '';
	}
	if (isset($_POST['kirja_keel'])) {
		$kirja_keel = $_POST['kirja_keel'];
	} else {
		$kirja_keel = 0;
	}

	$sisu = <<<SISU
	<form action="" method="POST">
		<h2>Uus kasutaja</h2>
		<table class="colspace">
		<tr><td>Kasutaja eesnimi:</td><td><input type="text" name="kasutaja_eesnimi" value="$eesnimi" /></td></tr>
		<tr><td>Kasutaja perenimi:</td><td><input type="text" name="kasutaja_perenimi" value="$perenimi" /></td></tr>
		<tr><td>Kasutaja email:</td><td><input type="text" name="kasutaja_email" value="$email" /></td></tr>
SISU;
	$sisu .= <<<SISU
		<tr><td>Kasutaja grupp:</td><td>
		<select name="kasutaja_grupp">
SISU;
	$paring = m_q('SELECT id, nimetus FROM grupp');
	while ($rida = m_a($paring)) {
		if ($kasutaja_grupp == $rida['id']) {
			$sel = 'selected';
		} else {
			$sel = '';
		}
		$sisu .= '<option value="' . $rida['id'] . '" ' . $sel . '>' . $rida['nimetus'] . '</option>';
	}
	$sisu .= <<<SISU
		</select></td></tr>
		<tr><td>Kasutaja aegub:</td><td><input type="text" name="kasutaja_aegub" value="$aegub" /></td></tr>
		<tr><td colspan="2">Aegumsaja formaat: PP.KK.AAAA<br />
		Jäta tühjaks kui ei taha, et kasutaja aeguks.<br />
		Kui kasutaja on aegunud siis ta ei saa enam sisse logida.</td></tr>
		<tr><td>Kirja keel</td><td><select name="kirja_keel">
SISU;
	$paring = m_q('SELECT id FROM ui_keeled');
	while ($rida = m_a($paring)) {
		if ($kirja_keel == $rida['id']) {
			$sel = 'selected';
		} else {
			$sel = '';
		}
		$sisu .= '<option value="' . $rida['id'] . '" ' . $sel . '>' . $rida['id'] . '</option>';
	}
	$sisu .=<<<SISU
		</select></td></tr>
		<tr><td></td><td><input type="submit" name="uus_kasutaja" value="Tee uus kasutaja"></td></tr>
		</table>
	</form>
SISU;
include('kujundus.php');
}

function show_g() {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	$sisu = show_full_content('grupp');
	include('kujundus.php');
}

function create_g($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	$sisu = <<<SISU
	<form action="" method="POST">
		<h2>Uus grupp</h2>
		<table class="colspace">
		<tr><td>Grupi nimi:</td><td><input type="text" name="grupi_nimi"></td></tr>
		<tr><td>Grupi õigused:</td><td>
		<select name="grupi_oigused">
			<option value="1">Otsimine</option>
			<option value="2">Otsimine ja lisamine</option>
			<option value="3">Administraator</option>
		</select></td></tr>
		<tr><td></td><td><input type="submit" name="uus_grupp" value="Tee uus grupp"></td></tr>
		</table>
	</form>
SISU;
include('kujundus.php');
}

function erilised() {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	$sisu = nl2br('
Erilised tabelid:
Sellist olemasolevat tabelit ei ole, mida poleks kuskil koodis sisse kirjutatud. Olemasolevate tabelite nimesid ei tohi muuta ilma koodi muutmata.
Arhiivimaterjalide tabelite seosed on kirjeldatud paljudes failides.

Erilised väljanimed:
> välja nimi ühtib tabeli nimega - kuvamisel küsitakse teisest tabelist kiirvaade
> `sarja_tahis` - tabelis sari; kasutatakse arhiivinumbri genereerimisel
> `materjal` ja `materjal_id` - osalejaid ja arhiivimaterjale siduvates tabelites (digitaliseerija, roll)
> `tabel` ja `vali` - tabelis lemmikud lemmiku ja tabeli kirje sidumiseks
> `syndmus` - tabelis logi
> `id` - igas tabelis
> `andmebaasi_lisatud` - igas tabelis
> `lisaja` - igas tabelis; väärtus on number, mis viitab kasutaja väljale `id`
> `oigused` - avalikes tabelites; väärtused avalik/mitteavalik
> `kestus` - träkkide tabelites; andembaasis täisarvuna (sekundid); kuvatakse ja sisestatakse kujul TT:MM:SS; otsigutulemustes kuvatakse kestuste kokkuvõte
> `ressursi_tyyp`
&nbsp;&nbsp;&nbsp;&nbsp;1 - "Füüsiline"
&nbsp;&nbsp;&nbsp;&nbsp;2 - "Digitaalne"
> `fyysiliselt_olemas`
&nbsp;&nbsp;&nbsp;&nbsp;1 - "Olemas"
&nbsp;&nbsp;&nbsp;&nbsp;2 - "Kadunud"
&nbsp;&nbsp;&nbsp;&nbsp;3 - "Laenatud"
> `digitaalselt_olemas`
&nbsp;&nbsp;&nbsp;&nbsp;1 - "Jah"
&nbsp;&nbsp;&nbsp;&nbsp;2 - "Ei"
> `vanus` - osalejaid ja arhiivimaterjale siduvates tabelites (digitaliseerija, roll); andmebaasis numbrina; kokkukirjutatud kiirvaates kuvatakse sulgudes
> `arhiivinumber` - kõigis arhiivimaterjale sisaldavates tabelites
> `*aeg*` ja `*aasta*` - otsimisel vaadatakse viimast nelja sümbolit kui aastaarvu; * on suvaline tekst; kehtib kui välja tüüp ei ole kuupäev (DATE)
> kuupäev (DATE) tüüpi väljad on andmebaasis kujul AAAA-KK-PP (MySQL formaat), kuvatakse ja sisestatakse kujul PP.KK.AAAA
> `lehekylgede_arv` ja `digit_lk_arv` - käsikirjade tabelis; kasutatakse otingutulemuste kokkuvõtte tegemisel
	');
	include('kujundus.php');
}

function show_full_content_helper($table) {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if (table_exists($table)) {
		$sisu = show_full_content($table);
	}
	include('kujundus.php');
}

function show_full_content($table, $where = []) {
	if (isset($_GET['l'])) {
		$l = (int)$_GET['l'];
	} else {
		$l = 0;
	}
	$num2 = m_select($table, $where);
	$uudiseid = m_r($num2);
	if ($uudiseid > 100) {
		$lehed = pages('haldus.php?show_full_content=' . $table . '&l=%d', $l, $uudiseid, 100);
	} else {
		$lehed = '';
	}

	$query = m_select($table, $where, null, ' ORDER BY id DESC LIMIT ' . $l * 100 . ', 100');
	$columns = get_columns($table);
	$sisu = $lehed;
	$sisu .= '<table class="colspace"><tr>';
	if ($table != 'logi') {
		$sisu .= '<th></th>';
	}
	foreach ($columns as $column) {
		$sisu .= '<th>' . $column . '</th>';
	}
	$sisu .= '</tr>';
	while ($row = m_a($query)) {
		$sisu .= '<tr>';
		if ($table != 'logi') {
			$sisu .= '
				<td style="min-width:55px;">
				<a href="muuda.php?t=' . $table . '&id=' . $row['id'] . '"><img src="kujundus/pildid/muuda.png" title="Muuda" alt="Muuda" /></a>
				<a href="kustuta.php?t=' . $table . '&id=' . $row['id'] . '"><img src="kujundus/pildid/kustuta.png" title="Kustuta" alt="Kustuta" /></a>
				</td>';
		}
		foreach ($columns as $column) {
			if ($column == 'id') {
				$sisu .= '<td><a href="naita.php?t=' . $table . '&id=' . $row['id'] . '">' . $row['id'] . '</a></td>';
			} else {
				$sisu .= '<td>' . $row[$column] . '</td>';
			}
		}
		$sisu .= '</tr>';
	}
	$sisu .= '</table>';
	return $sisu;
}

function sarnased_osalejad($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	$sisu = '
		<h2>Sarnased osalejad</h2>
		<table class="colspace">';
	$query = m_q('SELECT eesnimi, perenimi, COUNT(*) AS count FROM `osaleja` GROUP BY eesnimi, perenimi ORDER BY count DESC');
	if (!m_r($query)) {
		$sisu .= '
			<tr><td>Osalejaid ei leitud.</td></tr>
			</table>';
	} else {
		$sisu .= '</table>';
		while ($row = m_a($query)) {
			if ($row['count'] > 1) {
				$sisu .= '
				<table class="colspace">
				<tr><th>Eesnimi</th><th>Perenimi</th><th>Kokku</th><th>Liida osalejad</th></tr>';
				$sisu .= '
				<tr><td>' . $row['eesnimi'] . '</td><td>' . $row['perenimi'] . '</td><td>' . $row['count'] . '</td>
				<td><a href="haldus.php?liida_sarnased_osalejad=1&eesnimi=' . $row['eesnimi'] . '&perenimi=' . $row['perenimi'] . '">liida osalejad</td></tr>';
				$sisu .= '</table>';
				$sisu .= show_full_content('osaleja', ['eesnimi' => $row['eesnimi'], 'perenimi' => $row['perenimi']]);
			}
		}
	}
	$sisu .= '
		</table>';
	include('kujundus.php');
}

function keelte_haldus($teade = '', $viga = '') {
	global $lang;
	$pealkiri1 = $lang['haldus'];
	if ($teade == '') {
		unset($teade);
	}
	if ($viga == '') {
		unset($viga);
	}

	$sisu = '<h2>Keelte haldus</h2>';
	$sisu .= '<br />';

	if (isset($_POST['lisa_uus_keel'])) {
		if (table_exists($_POST['lisa_uus_keel_tyyp']) && strlen($_POST['lisa_uus_keel_kood']) == 3) {
			$master = m_a(m_select($_POST['lisa_uus_keel_tyyp'], ['id' => MASTER_LANG]));
			$sql = ['id' => $_POST['lisa_uus_keel_kood']];
			foreach ($master as $col => $val) {
				if ($col != 'id' && !is_numeric($col)) {
					$sql[$col] = $val;
				}
			}
			m_insert($_POST['lisa_uus_keel_tyyp'], $sql) or $viga = m_err();
		} else {
			$viga = 'Tabelit ' . $_POST['lisa_uus_keel_tyyp'] . ' pole olemas või ' . $_POST['lisa_uus_keel_kood'] . ' pole kolm sümbolit.';
		}
		if (!isset($viga)) {
			header('Location: haldus.php?keelte_haldus=1');
			die();
		}
	} elseif (isset($_GET['lisa'])) {
		$sisu .= '<form action="" method="POST">';
		$sisu .= '<input type="hidden" name="lisa_uus_keel_tyyp" value="' . $_GET['lisa'] . '" />';
		$sisu .= 'Kolmekohaline keele kood: <input type="text" name="lisa_uus_keel_kood" />';
		$sisu .= '<input type="submit" name="lisa_uus_keel" value="Lisa" />';
		$sisu .= '</form>';
	} elseif (isset($_POST['lisa_valjad'])) {
		for ($i = 0; $i < $_POST['kogus']; $i++) {
			if ($_POST['valja_nimi' . $i]) {
				if (m_q('ALTER TABLE `ui_keeled` ADD `' . $_POST['valja_nimi' . $i] . '` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL')) {
					m_update('ui_keeled', [
						$_POST['valja_nimi' . $i] => $_POST['tolge' . $i]
					], [
						1 => 1
					]);
				}
			}
		}
		header('Location: haldus.php?keelte_haldus=1');
		die();
	} elseif (isset($_POST['lisa_ui_keeled'])) {
		$sisu .= '<form action="" method="POST">';
		$sisu .= '<input type="hidden" name="kogus" value="' . $_POST['kogus'] . '" />';
		$sisu .= '<table class="colspace"><tr>';
		$sisu .= '<tr><th>Välja nimi</th><th></th><th>Tõlge peakeeles</th></tr>';
		for ($i = 0; $i < $_POST['kogus']; $i++) {
			$sisu .= '<tr><td><input type="text" name="valja_nimi' . $i . '" /></td><td>=</td><td><textarea name="tolge' . $i . '" cols="25" rows="3"></textarea></td></tr>';
		}
		$sisu .= '</table>';
		$sisu .= '<input type="submit" name="lisa_valjad" value="Valmis" />';
		$sisu .= '</form>';
	} else {
		$sisu .= '<table class="colspace"><tr>';
		$sisu .= '<th></th><th>Kasutajaliidese keeled:</th></tr>';
		$query = m_q('SELECT * FROM ui_keeled');
		while ($row = m_a($query)) {
			$sisu .= '
				<tr>
					<td style="min-width:55px;">
					<a href="muuda.php?t=ui_keeled&id=' . $row['id'] . '"><img src="kujundus/pildid/muuda.png" title="Muuda" alt="Muuda" /></a>
					<a href="kustuta.php?t=ui_keeled&id=' . $row['id'] . '"><img src="kujundus/pildid/kustuta.png" title="Kustuta" alt="Kustuta" /></a>
					</td>';
			$sisu .= '<td>';
			$sisu .= '<a href="naita.php?t=ui_keeled&id=' . $row['id'] . '">' . $row['id'] . '</a>';
			if ($row['id'] == MASTER_LANG) {
				$sisu .= ' (põhikeel)';
			}
			$sisu .= '</td></tr>';
		}
		$sisu .= '</table>';
		$sisu .= '<a href="haldus.php?keelte_haldus=1&lisa=ui_keeled">Lisa uus keel</a>';
		$sisu .= '<form action="" method="POST"><input type="submit" name="lisa_ui_keeled" value="Lisa" /> <input type="text" name="kogus" /> välja.</form>';
		$sisu .= '<br />';
		$sisu .= '<table class="colspace"><tr>';
		$sisu .= '<th></th><th>Tabeliväljade keeled:</th></tr>';
		$query = m_q('SELECT * FROM tabeli_keeled');
		while ($row = m_a($query)) {
			$sisu .= '
				<tr>
					<td style="min-width:55px;">
					<a href="muuda.php?t=tabeli_keeled&id=' . $row['id'] . '"><img src="kujundus/pildid/muuda.png" title="Muuda" alt="Muuda" /></a>
					<a href="kustuta.php?t=tabeli_keeled&id=' . $row['id'] . '"><img src="kujundus/pildid/kustuta.png" title="Kustuta" alt="Kustuta" /></a>
					</td>';
			$sisu .= '<td>';
			$sisu .= '<a href="naita.php?t=tabeli_keeled&id=' . $row['id'] . '">' . $row['id'] . '</a>';
			if ($row['id'] == MASTER_LANG) {
				$sisu .= ' (põhikeel)';
			}
			$sisu .= '</td></tr>';
		}
		$sisu .= '</table>';
		$sisu .= '<a href="haldus.php?keelte_haldus=1&lisa=tabeli_keeled">Lisa uus keel</a>';
		$sisu .= '<table class="colspace"><tr>';
		$sisu .= '<th></th><th>Tekstid:</th></tr>';
		$query = m_q('SELECT * FROM tekstid');
		while ($row = m_a($query)) {
			$sisu .= '
				<tr>
					<td style="min-width:55px;">
					<a href="muuda.php?t=tekstid&id=' . $row['id'] . '"><img src="kujundus/pildid/muuda.png" title="Muuda" alt="Muuda" /></a>
					<a href="kustuta.php?t=tekstid&id=' . $row['id'] . '"><img src="kujundus/pildid/kustuta.png" title="Kustuta" alt="Kustuta" /></a>
					</td>';
			$sisu .= '<td>';
			$sisu .= '<a href="naita.php?t=tekstid&id=' . $row['id'] . '">' . $row['id'] . '</a>';
			if ($row['id'] == MASTER_LANG) {
				$sisu .= ' (põhikeel)';
			}
			$sisu .= '</td></tr>';
		}
		$sisu .= '</table>';
		$sisu .= '<a href="haldus.php?keelte_haldus=1&lisa=tekstid">Lisa uus keel</a>';
	}
	include('kujundus.php');
}

function generate_options(array $options, $selected = '') {
	$html = '<option value="">Vali...</option>';
	foreach ($options as $value) {
		if ($selected == $value) {
			$valitud = 'selected';
		} else {
			$valitud = '';
		}
		$html .= '<option value="' . $value . '" ' . $valitud . '>' . $value . '</option>';
	}
	return $html;
}

function get_field_type_options() {
	return [
		'VARCHAR(225)' => [
			'label' => ' Tekst '
		],
		'TEXT' => [
			'label' => ' Pikk tekst '
		],
		'INT(11) unsigned' => [
			'label' => ' Number '
		],
		'BIGINT(225) unsigned' => [
			'label' => ' Suur number '
		],
		'TINYINT(4) unsigned' => [
			'label' => ' Lühike number '
		],
		'DOUBLE' => [
			'label' => ' Ujukomaarv'
		],
		'DATE' => [
			'label' => ' Kuupäev [PP.][KK.]AAAA '
		],
		'DATETIME' => [
			'label' => ' Kuupäev ja kellaaeg AAAA-KK-PP TT:MM:SS '
		],
		'TINYINT(1)' => [
			'label' => ' Loogiline jah või ei (1/0)'
		]
	];
}

function get_field_type_options_html(array $options) {
	$html = '';
	foreach ($options as $value => $option) {
		if ($option['selected'] ?? false) {
			$valitud = 'selected';
		} else {
			$valitud = '';
		}
		$html .= '<option value="' . $value . '" ' . $valitud . '>' . $option['label'] . '</option>';
	}
	return $html;
}
