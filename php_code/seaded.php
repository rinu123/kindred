﻿<?php

include('base.php');
if (!$sess = sisse_logitud()) {
	header('Location: index.php');
	die();
}

if (isset($_POST['muuda'])) {
	if (!password_verify($_POST['kehtiv'], $sess['parool'])) {
		$viga = $lang['kehtiv_parool_pole_oige'];
	} elseif (strlen($_POST['uus1']) < 6) {
		$viga = $lang['palun_vali_pikem_parool'];
	} elseif ($_POST['uus1'] != $_POST['uus2']) {
		$viga = $lang['uued_paroolid_ei_kattu'];
	} else {
		m_update('kasutaja', [
			'parool' => password_hash($_POST['uus1'], PASSWORD_BCRYPT)
		], [
			'id' => $sess['id']
		]);
		$teade = $lang['parool_on_vahetatud'];
		session_destroy();
	}
}

$nonSystemTables = get_non_system_tables();

$pealkiri1 = $lang['seaded'];
$sisu = <<<SISU
<form action="" method="POST">
<h2>{$lang['parooli_muutmine']}</h2>
<table class="colspace">
<tr><td>{$lang['kehtiv_parool']}</td><td><input type="password" name="kehtiv"></td></tr>
<tr><td>{$lang['uus_parool']}</td><td><input type="password" name="uus1"></td></tr>
<tr><td>{$lang['uus_parool_uuesti']}</td><td><input type="password" name="uus2"></td></tr>
<tr><td></td><td><input type="submit" name="muuda" value="{$lang['muuda_parool']}"></td></tr>
</table>
</form>
<h2>{$lang['lemmikud_pealkiri']}</h2>
<table width="100%">
SISU;
$lemmikudParing = m_q('SELECT * FROM lemmikud WHERE lisaja = :lisaja ORDER BY id DESC', ['lisaja' => $sess['id']]); //leia minu lemmikud
if (!m_r($lemmikudParing)) {
	$sisu .= '<tr><td>' . $lang['lemmikud_info2'] . '</td></tr>';
}
while ($lemmik = m_a($lemmikudParing)) {
	$sisu .= '<tr><td><a href="kustuta.php?t=lemmikud&id=' . $lemmik['id'] . '"><img src="kujundus/pildid/kustuta.png" title="' . $lang['eemalda_lemmikutest'] . '" alt="' . $lang['eemalda_lemmikutest'] . '" /></a> <a href="naita.php?t=' . $lemmik['tabel'] . '&id=' . $lemmik['vali'] . '">' . kriips($lemmik['tabel']);
	$tablerow = m_a(m_select($lemmik['tabel'], ['id' => $lemmik['vali']]));
	$must_show_columns = get_must_show_columns($lemmik['tabel']);
	foreach ($must_show_columns as $value) {
		if ($value == 'kestus') {
			$sisu .= ' ' . pretty_length($tablerow[$value]);
		} elseif ($value == 'materjal') {
			if ($query = m_select($tablerow['materjal'], ['id' => $tablerow['materjal_id']])) {
				$sisu .= ': ' . kriips($tablerow['materjal']);
				$row = m_a($query);
				$must_show_columns2 = get_must_show_columns($tablerow['materjal']);
				foreach ($must_show_columns2 as $value2) {
					$sisu .= ' ' . $row[$value2];
				}
			}
		} elseif (in_array($value, $nonSystemTables)) {
			if ($query = m_select($value, ['id' => $tablerow[$value]])) {
				$row = m_a($query);
				$must_show_columns2 = get_must_show_columns($value);
				foreach ($must_show_columns2 as $value2) {
					$sisu .= ' ' . $row[$value2];
				}
			}
		} else {
			$sisu .= ' ' . korrasta($tablerow[$value]);
		}
	}
	$sisu .= '</a></td></tr>';
}
$sisu .= '</table>';

include('kujundus.php');
