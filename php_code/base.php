<?php
include('conf.php');
session_start();
include('db_functions.php');
m_c();

if (isset($_GET['setlan'])) {
    if (lang_exists($_GET['setlan'])) {
        $_SESSION['lang'] = $_GET['setlan'];
    }
    if (getenv('HTTP_REFERER')) {
        header('Location: ' . getenv('HTTP_REFERER'));
    } else {
        header('Location: index.php');
    }
    die();
}
if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = MASTER_LANG;
}

$lang = m_f('SELECT * FROM ui_keeled WHERE id = :lang', ['lang' => $_SESSION['lang']]);
$lang_t = m_f('SELECT * FROM tabeli_keeled WHERE id = :lang', ['lang' => $_SESSION['lang']]);

function kriips($sona, $uc = 1) {
	global $lang_t;
	$lang_t['id'] = 'id';
	if (isset($lang_t[$sona])) {
		if ($uc == 1) {
			return ucfirst_utf8($lang_t[$sona]);
		}
		return $lang_t[$sona];
	}
	if ($uc == 1) {
		return ucfirst_utf8(str_replace('_', ' ', $sona));
	}
	return str_replace('_', ' ', $sona);
}

function ucfirst_utf8($stri) {
	if($stri{0}>="\xc3")
	return (($stri{1}>="\xa0")?
	($stri{0}.chr(ord($stri{1})-32)):
	($stri{0}.$stri{1})).substr($stri,2);
	else return ucfirst($stri);
}

function sisse_logitud() {
	static $signedIn = null;
	if ($signedIn === null) {
		$signedIn = false;
		if (isset($_SESSION['kindred'])) {
			$sess = $_SESSION['kindred'];
			$paring = m_q('SELECT * FROM kasutaja WHERE id = :id AND email = :email AND parool = :parool', [
				'id' => $sess['id'],
				'email' => $sess['email'],
				'parool' => $sess['parool']
			]);
			if (m_r($paring) == 1) {
				$signedIn = m_a($paring);
			}
		}
	}
	return $signedIn;
}

function grupi_oigused($sess) {
	if ($paring = m_q('SELECT grupi_oigused FROM grupp WHERE id = :grupp', ['grupp' => $sess['grupp']])) {
		if ($row = m_a($paring)) {
			return $row['grupi_oigused'];
		}
		return false;
	}
	return false;
}

function korrasta($what, $chars = 80) {
	return wordwrap($what, $chars, ' ', true);
}

function pages($url, $current, $total, $show = 7) {
	global $lang;
	if ($show == 1) {
		$lang['eelmised'] = $lang['eelmine'];
		$lang['jargmised'] = $lang['jargmine'];
	}
	$pages = '';
	if (empty($current) || $current < 0) {
		$current = 1;
	} else {
		$current++;
	}
	$max = ceil($total/$show);
	if ($current > $max) {
		$current = $max;
	}
	if ($current > 8) {
		$pages .= '<a href="' . sprintf($url, 0) . '">1</a> <a href="' . sprintf($url, 1) . '">2</a> ... ';
	} elseif ($current > 7) {
		$pages .= '<a href="' . sprintf($url, 0) . '">1</a> ... ';
	}

	$low = $current - 5;
	$high = $current + 5;
	if ($low < 1) {
		$low = 1;
		$high = 11;
	}

	$num = range($low, $high);
	foreach ($num as $p) {
		if ($p < 1 || $p > $max) {
			continue;
		}
		if ($p != $current) {
			$pages .= '<a href="' . sprintf($url, $p-1) . '">' . $p . '</a> ';
		} else {
			$pages .= '<b>' . $p . '</b> ';
		}
	}

	if ($current < $max - 7) {
		$pages .= '... <a href="' . sprintf($url, $max - 2) . '">' . ($max - 1) . '</a> <a href="' . sprintf($url, $max - 1) . '">' . $max . '</a>';
	} elseif($current < $max - 6) {
		$pages .= '... <a href="' . sprintf($url, $max - 1) . '">' . $max . '</a>';
	}

	if ($current > 1) {
		$prev = '<a href="' . sprintf($url, $current - 2) . '">&laquo; ' . $lang['eelmised'] . '</a>';
	} else {
		$prev = '&laquo; ' . $lang['eelmised'];
	}
	if ($current < $max) {
		$next = '<a href="' . sprintf($url, $current) . '">' . $lang['jargmised'] . ' &raquo;</a>';
	} else {
		$next = $lang['jargmised'] . ' &raquo;';
	}

	return '<table style="width: 100%;"><tr><td style="text-align: left;">' . $prev . '</td><td style="text-align: center;">' . $pages . '</td><td style="text-align: right;">' . $next . '</td></tr></table>';
}

function date_to_est($date) {
	if ($date == null || $date == '') {
		return '';
	}
	$date = explode('-', $date);
	if ($date[2] != '00' && $date[1] != '00') {
		return $date[2] . '.' . $date[1] . '.' . $date[0];
	}
	if ($date[1] != '00') {
		return $date[1] . '.' . $date[0];
	}
	return $date[0];
}

function date_to_mysql($date) {
	$date = explode('.', $date);
	$count = count($date);
	if ($count == 3) {
		return $date[2] . '-' . $date[1] . '-' . $date[0];
	}
	if ($count == 2) {
		return $date[1] . '-' . $date[0] . '-00';
	}
	if ($count == 1) {
		return $date[0] . '-00-00';
	}
	return false;
}

function highlight($text, $what) {
	return preg_replace("/($what)/i", '<b>\1</b>', $text);
}

function pretty_fieldtype($type, $length = null) {
	global $lang;
	if ($type === 'tinyint' && $length == 1) {
		return ' ' . $lang['boolean_jah_voi_ei'];
	}
	$r = [
		'tinyint' => ' ' . $lang['lyhike_number'],
		'bigint' => ' ' . $lang['suur_number'],
		'int' => ' ' . $lang['number'],
		'varchar' => ' ' . $lang['tekst'],
		'text' => ' ' . $lang['pikk_tekst'],
		'datetime' => ' ' . $lang['aeg_AAAA_KK_PP_TT_MM_SS'],
		'date' => ' ' . $lang['kuupaev_PP_KK_AAAA']
	];
	return $r[$type] ?? '';
}

function pretty_length($secs) {
	if (!$secs) {
		return '';
	}
	if ($secs < 60) {
		if ($secs < 10) {
			$secs = '0' . $secs;
		}
		return '0:00:' . $secs;
	}
	if ($secs < 3600) {
		$minutid = round($secs / 60 - 0.499);
		$secs = $secs - $minutid * 60;
		if ($minutid < 10) {
			$minutid = '0' . $minutid;
		}
		if ($secs < 10) {
			$secs = '0' . $secs;
		}
		return '0:' . $minutid . ':' . $secs;
	}
	$tunnid = round($secs / 3600 - 0.499);
	$minutid = round(($secs - $tunnid * 3600) / 60 - 0.499);
	if ($minutid < 10) {
		$minutid = '0' . $minutid;
	}
	$secs = $secs - $tunnid * 3600 - (int)$minutid * 60;
	if ($secs < 10) {
		$secs = '0' . $secs;
	}
	return $tunnid . ':' . str_replace('-', '', $minutid) . ':' . $secs;
}

function is_email($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function mymail($to, $title, $body, $from = '') {
	$from = trim($from);

	if (!$from) {
		$from = EMAIL_SENDER_NAME . '<' . EMAIL_SENDER_ADDRESS . '>';
	}

	$rp     = $from;
	$org    = EMAIL_SENDER_ORG;
	$mailer = 'PHP';

	$head = '';
	$head .= "Content-Type: text/plain; charset = \"UTF-8\" \r\n";
	$head .= "Date: ". date('r'). " \r\n";
	$head .= "Return-Path: $rp \r\n";
	$head .= "From: $from \r\n";
	$head .= "Sender: $from \r\n";
	$head .= "Reply-To: $from \r\n";
	$head .= "Organization: $org \r\n";
	$head .= "X-Sender: $from \r\n";
	$head .= "X-Priority: 3 \r\n";
	$head .= "X-Mailer: $mailer \r\n";

	$body = str_replace("\r\n", "\n", $body);
	$body = str_replace("\n", "\r\n", $body);

	return mail($to, $title, $body, $head);
}

function lang_exists($lang) {
	if (!$lang) {
		return false;
	}
	$query = m_q('SELECT id FROM ui_keeled');
	while ($row = m_a($query)) {
		if ($row['id'] == $lang) {
			return true;
		}
	}
	return false;
}

function vanus($synd, $salvestus) { //sünniaeg [*][[PP.]KK.]AAAA[*] ja salvestusaeg [*][[PP.]KK.]AAAA[*]
	$tykid = explode('.', $synd);
	$tykid2 = explode('.', $salvestus);
	$count = count($tykid);
	$count2 = count($tykid2);
	if (strlen($tykid[$count - 1]) > 4 && is_numeric(substr($tykid[$count - 1], -4))) {
		$tykid[$count - 1] = substr($tykid[$count - 1], -4);
	}
	if (strlen($tykid2[$count2 - 1]) > 4 && is_numeric(substr($tykid2[$count2 - 1], -4))) {
		$tykid2[$count2 - 1] = substr($tykid2[$count2 - 1], -4);
	}
	if ($count == 3) {
		$v1 = preg_replace("/[^0-9]/", '', $tykid[0]) . '.' . preg_replace("/[^0-9]/", '', $tykid[1]) . '.' . preg_replace("/[^0-9]/", '', $tykid[2]);
	} elseif ($count == 2) {
		$v1 = '0.' . preg_replace("/[^0-9]/", '', $tykid[0]) . '.' . preg_replace("/[^0-9]/", '', $tykid[1]);
	} elseif ($count == 1 && $tykid[0]) {
		$v1 = '0.0.' . preg_replace("/[^0-9]/", '', $tykid[0]);
	} else {
		return false;
	}
	if ($count2 == 3) {
		$v2 = preg_replace("/[^0-9]/", '', $tykid2[0]) . '.' . preg_replace("/[^0-9]/", '', $tykid2[1]) . '.' . preg_replace("/[^0-9]/", '', $tykid2[2]);
	} elseif ($count2 == 2) {
		$v2 = '0.' . preg_replace("/[^0-9]/", '', $tykid2[0]) . '.' . preg_replace("/[^0-9]/", '', $tykid2[1]);
	} elseif ($count2 == 1 && $tykid2[0]) {
		$v2 = '0.0.' . preg_replace("/[^0-9]/", '', $tykid2[0]);
	} else {
		return false;
	}
	$tykid = explode('.', $v1);
	$tykid2 = explode('.', $v2);
	$vanus = $tykid2[2] - $tykid[2];
	if ($tykid2[1] < $tykid[1]) {
		$vanus--;
	} elseif ($tykid2[1] == $tykid[1] && $tykid2[0] < $tykid[0]) {
		$vanus--;
	}
	return $vanus;
}

function xsendfile($filePath, $download = true) {
	ini_set('default_mimetype', '');

	if ($download) {
		header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
	}

	if (stripos(($_SERVER['SERVER_SOFTWARE'] ?? ''), 'nginx') === 0) {
		header('X-Accel-Redirect: ' . CHROOT_PREFIX . $filePath);
	} else {
		header('Content-Type: ' . mime_content_type($filePath));
		header('X-Sendfile: ' . CHROOT_PREFIX . $filePath);
	}

	exit;
}

function findOriginalFile($dir, $arhiiviNumber, $exts = []) {
	if (is_dir($dir)) {
		$handle = opendir($dir);
		while ($file = readdir($handle)) {
			if (strpos($file, '.') !== 0) {
				if (stripos($file, $arhiiviNumber) !== false) {
					if (!count($exts)) {
						return $file;
					}
					$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
					if (in_array($ext, $exts)) {
						return $file;
					}
				}
			}
		}
		closedir($handle);
	}
	return null;
}

function reload($location = null) {
	header('Location: ' . ($location ?? $_SERVER['REQUEST_URI']));
	exit;
}

function tekst($sona) {
	static $texts;
	if ($texts === null) {
		$texts = m_f('SELECT * FROM tekstid WHERE id = :lang', ['lang' => $_SESSION['lang']]);
	}
	return $texts[$sona] ?? $sona;
}
