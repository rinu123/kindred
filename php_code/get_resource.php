﻿<?php
include('base.php');
if ($sess = sisse_logitud()) {
	if (!$minu_grupi_oigused = grupi_oigused($sess)) {
		$minu_grupi_oigused = 0;
	}
} else {
	$minu_grupi_oigused = 0;
}

if ($minu_grupi_oigused <= 0) { //sisse logimata
	$tabelid = get_public_tables();
	$excludes = excludes();
} elseif ($minu_grupi_oigused <= 1) { //otsimine
	$tabelid = get_non_system_tables();
	$excludes = excludes();
} elseif ($sess['grupp'] <= 2) { //otsimine, lisamine
	$tabelid = get_non_system_tables();
	$excludes = excludes(0);
} else { //admin
	$tabelid = get_table_names();
	$excludes = [];
}

$allTables = get_table_names();
$nonSystemTables = get_non_system_tables();

$xtabel = $_GET['t'] ?? null;
$t_id = $_GET['id'] ?? null;
$format = $_GET['format'] ?? null;

if (!in_array($xtabel, $tabelid)) {
	header('HTTP/1.0 404 Not Found');
	die();
}

$where = [
	'id' => $t_id
];
if ($minu_grupi_oigused <= 0) {
	$where['oigused'] = 'avalik';
}
$paring = m_select($xtabel, $where);

if (!m_r($paring)) {
	header('HTTP/1.0 404 Not Found');
	die();
}

$r = m_a($paring);
if ($xtabel == 'audio_trakk') {
	$sari = m_a(m_q('SELECT * FROM audio WHERE id = :id', ['id' => $r['audio']]));
	$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $sari['sari']]));
	$eemaldaSari = explode($sarjaParing['sarja_tahis'], $r['arhiivinumber']);
	$filePath = AUDIO_PATH . $sarjaParing['sarja_tahis'] . '_WAV/' . $sarjaParing['sarja_tahis'] . substr($eemaldaSari[1], 0, 2);
	$finalWAVFile = findOriginalFile($filePath, $r['arhiivinumber'], ['wav']);
	if ($finalWAVFile) {
		$finalWAVFile = $filePath . '/' . $finalWAVFile;
		$finalConvertedFile = CONVERTED_AUDIO_PATH . pathinfo($finalWAVFile)['filename'] . '.mp3';
		if ($format === 'CONVERTED_AUDIO') {
			xsendfile($finalConvertedFile, false);
		}
		if ($minu_grupi_oigused > 0) {
			xsendfile($finalWAVFile);
		}
	}
} elseif ($xtabel == 'foto') {
	$album = m_a(m_q('SELECT * FROM fotoalbum WHERE id = :id', ['id' => $r['fotoalbum']]));
	$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $album['sari']]));
	$filePath = PHOTO_PATH . $sarjaParing['sarja_tahis'] . '/' . $album['arhiivinumber'];
	$finalOrigFile = findOriginalFile($filePath, $r['arhiivinumber'], ['jpg', 'jpeg']);
	if ($finalOrigFile) {
		if ($format === 'PISI') {
			$finalConvertedFile = IMAGE_PATH . basename($finalOrigFile);
			if (!file_exists($finalConvertedFile)) {
				header('HTTP/1.0 404 Not Found');
				die();
			}
			xsendfile($finalConvertedFile, false);
		}
		if ($sess) {
			xsendfile($filePath . '/' . $finalOrigFile);
		}
	}
} elseif ($xtabel == 'kasikiri') {
	$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $r['sari']]));
	$filePath = KASIKIRI_PATH . $sarjaParing['sarja_tahis'] . '/' . $r['arhiivinumber'];
	if (is_dir($filePath)) {
		if ($format === 'ZIP') {
			if (!$sess) {
				header('HTTP/1.0 404 Not Found');
				die();
			}
			$files = [];
			$handle = opendir($filePath);
			while ($file = readdir($handle)) {
				if (strpos($file, '.') !== 0) {
					$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
					if (in_array($ext, ['jpg', 'jpeg', 'pdf'])) {
						$files[] = $file;
					}
				}
			}
			closedir($handle);

			if (count($files) > 1) {
				$zip = new ZipArchive();
				$finalZipFile = ZIP_PATH . $r['arhiivinumber'] . '.zip';
				$zip->open($finalZipFile, ZIPARCHIVE::CREATE);
				foreach ($files as $file) {
					$zip->addFile($filePath . '/' . $file, $r['arhiivinumber'] . '/' . $file);
				}
				$zip->close();
				xsendfile($finalZipFile);
			}

			xsendfile($filePath . '/' . $files[0]);
		}
		if (isset($_GET['file'])) {
			$finalOrigFile = $filePath . '/' . $_GET['file'];
			if (file_exists($finalOrigFile)) {
				if ($format === 'PISI') {
					$finalConvertedFile = IMAGE_PATH . $_GET['file'];
					if (!file_exists($finalConvertedFile)) {
						header('HTTP/1.0 404 Not Found');
						die();
					}
					xsendfile($finalConvertedFile, false);
				}
				xsendfile($finalOrigFile);
			}
		}
	}
} elseif ($xtabel == 'video_trakk') {
	$sari = m_a(m_q('SELECT * FROM video WHERE id = :id', ['id' => $r['video']]));
	$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $sari['sari']]));
	$eemaldaSari = explode($sarjaParing['sarja_tahis'], $r['arhiivinumber']);
	$filePath = VIDEO_PATH . $sarjaParing['sarja_tahis'] . '/' . $sarjaParing['sarja_tahis'] . substr($eemaldaSari[1], 0, 2);
	$finalOrigFile = findOriginalFile($filePath, $r['arhiivinumber']);
	if ($finalOrigFile) {
		$finalOrigFile = $filePath . '/' . $finalOrigFile;
		$finalConvertedFile = CONVERTED_VIDEO_PATH . pathinfo($finalOrigFile)['filename'] . '.mp4';
		if ($format === 'CONVERTED_VIDEO') {
			if (!file_exists($finalConvertedFile)) {
				header('HTTP/1.0 404 Not Found');
				die();
			}
			xsendfile($finalConvertedFile);
		}
		if ($minu_grupi_oigused > 0) {
			xsendfile($finalOrigFile);
		}
	}
}

header('HTTP/1.0 404 Not Found');
die();
