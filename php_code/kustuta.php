﻿<?php

include('base.php');
if (!$sess = sisse_logitud()) {
	header('Location: index.php');
	die();
}

$xtabel = $_GET['t'];
$tabel1 = kriips($xtabel, 0);
$t_id = $_GET['id'];

if (!$minu_grupi_oigused = grupi_oigused($sess)) {
	header('Location: index.php');
	die();
}
if ($minu_grupi_oigused < 2 && $xtabel != 'lemmikud') {
	header('Location: index.php');
	die();
}

if (in_array($xtabel, get_system_tables()) && $minu_grupi_oigused < 3 && $xtabel != 'lemmikud') {
	die();
}

if (!isset($_SESSION['ref']) || $_SESSION['ref'] === '') {
	$ref = getenv('HTTP_REFERER');
	if ($ref == '') {
		$_SESSION['ref'] = 'index.php';
	} else {
		$_SESSION['ref'] = $ref;
	}
}

$orig_aeg = [
	'audio_trakk' => 'salvestusaeg',
	'video_trakk' => 'salvestusaeg',
	'foto' => 'pildistamisaeg',
	'kasikiri' => 'koostamise_aeg'
];

$pealkiri1 = 'Kustuta ' . $tabel1;
$sisu = <<<SISU
<form action="" method="POST">
	<table width="100%">
SISU;
$allTables = get_table_names();
$publicTables = get_public_tables();
$nonSystemTables = get_non_system_tables();
$infoTables = get_info_tables();
if (in_array($xtabel, $allTables)) {
	$paring = m_select($xtabel, ['id' => $t_id]);
	if (m_r($paring)) {
		//hakkame otsima kirjeid, mis kustutatava kirjega seostuvad
		$alamateArv = 0;
		$alamateNimekiri = [];
		foreach ($allTables as $tabel) {
			foreach (get_columns($tabel) as $column) {
				if ($column == $xtabel) {
					$alamateArv += m_r(m_select($tabel, [$xtabel => $t_id]));
					$alamateNimekiri[] = $tabel;
				}
			}
		}
		if (in_array('keel', get_columns($xtabel))) {
			$alamateArv += m_r(m_select('materjal_keel', ['materjal' => $tabel, 'materjal_id' => $t_id]));
			$alamateNimekiri[] = 'materjal_keel';
		}
		if (in_array($xtabel, $publicTables)) {
			$alamateArv += m_r(m_select('digitaliseerija', ['materjal' => $xtabel, 'materjal_id' => $t_id]));
			$alamateNimekiri[] = 'digitaliseerija';
			$alamateArv += m_r(m_select('roll', ['materjal' => $xtabel, 'materjal_id' => $t_id]));
			$alamateNimekiri[] = 'roll';
		}

		if (isset($_POST['kustuta']) && ($xtabel != 'sari' || $alamateArv == 0) && ($xtabel != 'osaleja' || $alamateArv == 0)) {
			$rida = m_a($paring);
			if ($minu_grupi_oigused < 3 && $rida['lisaja'] != $sess['id']) {
				$viga = $lang['pole_oigusi_kustutamiseks'];
			} else {
				$ylemus = '';
				$ylemus_id = 0;
				foreach (get_columns($xtabel) as $column) {
					if (in_array($column, $allTables)) {
						$ylemus = $column;
						$ylemus_id = $rida[$column];
						break;
					}
				}
				m_delete($xtabel, ['id' => $t_id]);
				m_delete('lemmikud', ['tabel' => $xtabel, 'vali' => $t_id]);
				if ($xtabel != 'lemmikud') {
					m_insert('logi', [
						'ip' => $_SERVER['REMOTE_ADDR'],
						'syndmus' => '<a href="naita.php?t=kasutaja&id=' . $sess['id'] . '">' . $sess['email'] . '</a> kustutas ' . kriips($xtabel) . ' id ' . $t_id . ' ning ' . $alamateArv . ' alamkirjet'
					], ['andmebaasi_lisatud' => 'NOW()']);
				}
				$teade = $lang['kustutatud'];
				if (!in_array($xtabel, $infoTables)) {
					foreach ($alamateNimekiri as $alamTabel) {
						if ($alamTabel == 'digitaliseerija') {
							m_delete('digitaliseerija', ['materjal' => $xtabel, 'materjal_id' => $t_id]);
						} elseif ($alamTabel == 'roll') {
							m_delete('roll', ['materjal' => $xtabel, 'materjal_id' => $t_id]);
						} elseif ($alamTabel == 'materjal_keel') {
							m_delete('materjal_keel', ['materjal' => $xtabel, 'materjal_id' => $t_id]);
						} else {
							m_delete($alamTabel, [$xtabel => $t_id]);
						}
					}
				}
				if (strpos($_SESSION['ref'], 'naita.php') === false && strpos($_SESSION['ref'], 'muuda.php') === false) {
					$teade .= '<meta http-equiv="refresh" content="1;url=' . $_SESSION['ref'] . '" />';
				} elseif ($ylemus != '') {
					if (strpos($_SESSION['ref'], 'naita.php') !== false) {
						$teade .= '<meta http-equiv="refresh" content="1;url=naita.php?t=' . $ylemus . '&id=' . $ylemus_id . '" />';
					} elseif (strpos($_SESSION['ref'], 'muuda.php') !== false) {
						$teade .= '<meta http-equiv="refresh" content="1;url=muuda.php?t=' . $ylemus . '&id=' . $ylemus_id. '" />';
					}
				}
				$_SESSION['ref'] = '';
			}
		} else {
			$ref = getenv('HTTP_REFERER');
			if ($ref == '') {
				$ref = 'index.php';
			}
			if ($xtabel == 'lemmikud'){
				$sisu .= '
					<tr><td>' . $lang['oled_kindel_et_soovid_oma_lemmiku_kustutada'] . '</td></tr>
					<tr><td><input type="submit" name="kustuta" value="' . $lang['jah'] . '" /> <a href="' . $ref . '">' . $lang['tagasi'] . '</a></td></tr>';
			} elseif ($xtabel == 'sari' && $alamateArv != 0) {
				$sisu .= '
					<tr><td>' . str_replace('{NUMBER}', $alamateArv, $lang['sarja_ei_saa_kustutada']) . '</td></tr>
					<tr><td><a href="' . $ref . '">' . $lang['tagasi'] . '</a></td></tr>';
			} elseif ($xtabel == "osaleja" && $alamateArv != 0) {
				$sisu .= '
					<tr><td>' . str_replace('{NUMBER}', $alamateArv, $lang['osalejat_ei_saa_kustutada']) . '</td></tr>
					<tr><td><a href="' . $ref . '">' . $lang['tagasi'] . '</a></td></tr>';
			} else {
				$sisu .= '
					<tr><td>' . str_replace('{TABEL}', $tabel1, $lang['oled_kindel_et_soovid_kustutada']);
				if ($alamateArv > 0 && !in_array($xtabel, $infoTables)) {
					$sisu .= str_replace('{NUMBER}', $alamateArv, ' ' . $lang['koos_sellega_kustutaksid_veel_seotud_kirjet']);
				}
				$sisu .= '</td></tr>';
				if ($alamateArv > 0) {
					$sisu .= '
						<tr><td>' . $lang['seotud_kirjed']. ':
					';
					foreach ($alamateNimekiri as $alamTabel) {
						if ($alamTabel == 'digitaliseerija' || $alamTabel == 'roll' || $alamTabel == 'materjal_keel') {
							$query = m_select($alamTabel, ['materjal' => $xtabel, 'materjal_id' => $t_id]);
							$orig_materjal = m_a(m_select($xtabel, ['id' => $t_id], [$orig_aeg[$xtabel]]));
						} else {
							$query = m_select($alamTabel, [$xtabel => $t_id]);
						}
						if ($query && m_r($query)) {
							$must_show_columns = get_must_show_columns($alamTabel);
							$sisu .= '<br /><br /><b>' . kriips($alamTabel) . '</b>:<br />';
							while ($row = m_a($query)) {
								if (in_array('vanus', $must_show_columns)) {
									$row2 = m_select('osaleja', ['id' => $row['osaleja']]);
									if ($row2 && $row2 = m_a($row2)) {
										if (isset($row2['synniaasta'])) {
											$synniaasta = $row2['synniaasta'];
										}
									}
								}
								$sisu .= '<a href="naita.php?t=' . $alamTabel . '&id=' . $row['id'] . '" target="_blank">';
								foreach ($must_show_columns as $value) {
									if ($value == 'kestus') {
										$sisu .= ' ' . pretty_length($row[$value]);
									} elseif ($value == 'materjal') {
										if ($query2 = m_select($row['materjal'], ['id' => $row['materjal_id']])) {
											$sisu = rtrim($sisu, ' ');
											$sisu .= ': ' . kriips($row['materjal']);
											$row2 = m_a($query2);
											$must_show_columns2 = get_must_show_columns($row['materjal']);
											foreach ($must_show_columns2 as $value2) {
												if ($value2 == 'kestus') {
													$sisu .= ' ' . pretty_length($row2[$value2]);
												} else {
													$sisu .= ' ' . $row2[$value2];
												}
											}
										}
									} elseif (in_array($value, $nonSystemTables)) {
										if ($query2 = m_select($value, ['id' => $row[$value]])) {
											$row2 = m_a($query2);
											$must_show_columns2 = get_must_show_columns($value);
											foreach ($must_show_columns2 as $value2) {
												if ($value2 == 'kestus') {
													$sisu .= ' ' . pretty_length($row2[$value2]);
												} else {
													$sisu .= ' ' . $row2[$value2];
												}
											}
										}
									} elseif ($value == 'vanus' && ($row['vanus'] || $row['vanus'] = vanus($synniaasta, $orig_materjal[$orig_aeg[$xtabel]]))) {
										$sisu .= ' (' . $row[$value] . ')';
									} else {
										$sisu .= ' ' . $row[$value];
									}
								}
								$sisu .= '</a>';
								$synniaasta = '';
								$sisu .= '<br />';
							}
						}
					}
					$sisu .= '</td></tr>';
				}
				$sisu .= '<tr><td><input type="submit" name="kustuta" value="' . $lang['jah'] . '" /> <a href="' . $ref . '">' . $lang['tagasi'] . '</a></td></tr>';
			}
		}
	} else {
		$viga = $lang['tabelis_pole_sellist_kirjet_olemas']  . ' <meta http-equiv="refresh" content="2;url=' . $_SESSION['ref'] . '" />';
		$_SESSION['ref'] = '';
	}
} else {
	$viga = $lang['tabelit_pole_olemas'] . ' <meta http-equiv="refresh" content="2;url=' . $_SESSION['ref'] . '" />';
	$_SESSION['ref'] = '';
}
$sisu .= <<<SISU
	</table>
</form>
SISU;
include('kujundus.php');
