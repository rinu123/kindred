﻿<?php

include('base.php');
if (!$sess = sisse_logitud()) {
	header('Location: index.php');
	die();
}

if (!$minu_grupi_oigused = grupi_oigused($sess)) {
	header('Location: index.php');
	die();
}
if ($minu_grupi_oigused < 2) {
	header('Location: index.php');
	die();
}

$minuLemmikud = m_q('SELECT * FROM lemmikud WHERE lisaja = :lisaja', ['lisaja' => $sess['id']]);
$lemmikud = [];
while ($lemmik = m_a($minuLemmikud)) {
	$lemmikud[] = $lemmik['tabel'] . '-' . $lemmik['vali'];
}

$tabel1 = kriips($_GET['t'], 0);
$xtabel = trim($_GET['t']);
$t_id = $_GET['id'];
if (!lang_exists($t_id)) {
	$langTable = 0;
} else {
	$langTable = 1;
}

$pealkiri1 = $lang['muuda'] . ' ' . $tabel1;
$allTables = get_table_names();
$publicTables = get_public_tables();
$infoTables = get_info_tables();

if (in_array($xtabel, $allTables)) {
	if ($paring = m_select($xtabel, ['id' => $t_id])) {
		$rida = m_a($paring);
		if ($minu_grupi_oigused < 3 && $rida['lisaja'] != $sess['id']) {
			header('Location: index.php');
			die();
		}

		$excludes = excludes(0);
		if ($langTable == 1) {
			$excludes = [];
		}
		$must_have_fields = get_must_have_columns($xtabel);

		if (isset($_POST['submit_muuda'])) { //tahetakse muuta kirjet andmebaasis
			$lisatudKeel = '';
			$sql = [];
			foreach (get_full_columns($xtabel) as $column) {
				$n = trim($_POST[$column['Field']] ?? ''); //eemalda whitespace'd ja muuda ära erisümbolid
				if (
					$n == '' &&
					!in_array($column['Field'], $excludes) &&
					in_array($column['Field'], $must_have_fields)
				) {
						$viga = kriips($column['Field']) . ' ' . $lang['on_kohustuslik'];
						break;
				}
				if ($langTable == 1) {
					$sql[$column['Field']] = $n;
				} elseif (!in_array($column['Field'], $excludes)) { //et siis igasugu jama ei taha me päringusse
					$fieldType = explode('(', $column['Type'])[0]; //int, varchar, date, datetime etc
					if ($fieldType == 'date') {
						if (!in_array($column['Field'], $must_have_fields) && $n == '') {
							$sql[$column['Field']] = null;
						} elseif ($kuup = date_to_mysql($n)) {
							$sql[$column['Field']] = $kuup;
						}
					} elseif ($column['Field'] == 'kestus') {
						if (!in_array($column['Field'], $must_have_fields) && $n == '') {
							$sql[$column['Field']] = null;
						} else {
							$tykid = array_reverse(explode(':', $n));
							$korda = 1;
							$secs = 0;
							foreach ($tykid as $tykk) {
								$secs += $tykk * $korda;
								$korda *= 60;
							}
							$sql['kestus'] = $secs;
						}
					} elseif ($column['Field'] == 'keel' && $xtabel != 'materjal_keel') {
						$lisatudKeel = $n;
					} else {
						if (!in_array($column['Field'], $must_have_fields) && $n == '') {
							$sql[$column['Field']] = null;
						} else {
							$sql[$column['Field']] = $n;
						}
					}
				}
			}
			if (!isset($viga)) {
				if (m_update($xtabel, $sql, ['id' => $t_id])) {
					if ($lisatudKeel) {
						m_insert('materjal_keel', [
							'keel' => $lisatudKeel,
							'materjal' => $xtabel,
							'materjal_id' => $t_id
						]);
					}
					m_insert('logi', [
						'ip' => $_SERVER['REMOTE_ADDR'],
						'syndmus' => '<a href="naita.php?t=kasutaja&id=' . $sess['id'] . '">' . $sess['email'].  '</a> muutis <a href="naita.php?t=' . $xtabel . '&id=' . $t_id . '">' . kriips($xtabel) . ' id ' . $t_id . '</a>'
					], ['andmebaasi_lisatud' => 'NOW()']);
					$muutmineOnnestus = 1;
					//muuda arhiivinumber ära serveri kõhus
					if (isset($_POST['arhiivinumber'])) {
						$uusArhNr = $_POST['arhiivinumber'];
						$uusArhNr1 = explode('-', substr($uusArhNr, 0, -1));
						$uusSarjaTahis = trim(str_replace(range(0,9), '', $uusArhNr1[0]));
						if ($rida['arhiivinumber'] != $uusArhNr) {
							if ($xtabel == 'audio_trakk' || $xtabel == 'audio') {
								if ($xtabel == 'audio_trakk') {
									$sari = m_a(m_q('SELECT * FROM audio WHERE id = :id', ['id' => $rida['audio']]));
								} else {
									$sari['sari'] = $rida['sari'];
								}
								$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $sari['sari']]));
								$eemaldaSari = explode($sarjaParing['sarja_tahis'], $rida['arhiivinumber']);
								$filedir = AUDIO_PATH . $sarjaParing['sarja_tahis'] . '_WAV/' . $sarjaParing['sarja_tahis'] . substr($eemaldaSari[1], 0, 2);
								$publicDirs = [CONVERTED_AUDIO_PATH];
							} elseif ($xtabel == 'foto' || $xtabel == 'fotoalbum') {
								if ($xtabel == 'foto') {
									$album = m_a(m_q('SELECT * FROM fotoalbum WHERE id = :id', ['id' => $rida['fotoalbum']]));
								} else {
									$album['sari'] = $rida['sari'];
								}
								$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $album['sari']]));
								$filedir = PHOTO_PATH . $sarjaParing['sarja_tahis'] . '/' . $album['arhiivinumber'];
								$publicDirs = [IMAGE_PATH];
							} elseif ($xtabel == 'kasikiri') {
								$sarjaParing = m_a(m_q('SELECT * FROM sari WHERE id = :id', ['id' => $rida['sari']]));
								$filedir = KASIKIRI_PATH . $sarjaParing['sarja_tahis'] . '/' . $rida['arhiivinumber'];
								$publicDirs = [IMAGE_PATH, 'zip_archive'];
							}
							if (is_dir($filedir)) {
								if ($xtabel == 'kasikiri' || $xtabel == 'audio' || $xtabel == 'fotoalbum') {
									$finalNewFileDir = str_replace($rida['arhiivinumber'], $uusArhNr, $filedir);
									if ($uusSarjaTahis != $sarjaParing['sarja_tahis']) {
										$finalNewFileDir = str_replace($sarjaParing['sarja_tahis'], $uusSarjaTahis, $finalNewFileDir);
									}
									if ($xtabel == 'audio_trakk' || $xtabel == 'audio') {
										$vanaKaheKohaline = substr($eemaldaSari[1], 0, 2);
										$eemaldaSari = explode($uusSarjaTahis, $uusArhNr);
										$uusKaheKohaline = substr($eemaldaSari[1], 0, 2);
										if ($vanaKaheKohaline != $uusKaheKohaline) {
											$finalNewFileDir = str_replace($uusSarjaTahis . '/' . $vanaKaheKohaline, $uusSarjaTahis . '/' . $uusKaheKohaline, $finalNewFileDir);
										}
									}
								} else {
									$handler = opendir($filedir);
									while ($file = readdir($handler)) {
										if (strpos($file, '.') !== 0) {
											if (stripos($file, $rida['arhiivinumber']) !== false && !isset($finalOrigFile)) {
												$finalOrigFile = $filedir . '/' . $file;
												$finalOrigFileName = $file;
											}
											if (stripos($file, $uusArhNr) !== false && !isset($finalNewFile)) {
												$finalNewFile = $filedir . '/' . $file;
											}
										}
									}
									closedir($handler);
								}
								if (isset($finalNewFile) && isset($finalOrigFile) || (($xtabel == 'kasikiri' || $xtabel == 'audio' || $xtabel == 'fotoalbum') && is_dir($finalNewFileDir))) {
									m_insert('logi', [
										'ip' => $_SERVER['REMOTE_ADDR'],
										'syndmus' => 'Materjali <a href="naita.php?t=' . $xtabel . '&id=' . $t_id . '">' . kriips($xtabel) . ' id ' . $t_id . '</a> failinime serveris ei saanud muuta, sest sama arhiivinumbriga fail on juba olemas'
									], ['andmebaasi_lisatud' => 'NOW()']);
									$muutmineOnnestus = 0;
								} elseif (isset($finalOrigFile)) {
									if ($xtabel == 'foto') {
										$filedir = str_replace($rida['arhiivinumber'], $uusArhNr, $filedir);
									}
									$finalNewFileName = $filedir . '/' . str_replace($rida['arhiivinumber'], $uusArhNr, $finalOrigFileName);
									if ($uusSarjaTahis != $sarjaParing['sarja_tahis']) {
										$finalNewFileName = str_replace($sarjaParing['sarja_tahis'], $uusSarjaTahis, $finalNewFileName);
									}
									if ($xtabel == 'audio_trakk' || $xtabel == 'audio') {
										$vanaKaheKohaline = substr($eemaldaSari[1], 0, 2);
										$eemaldaSari = explode($uusSarjaTahis, $uusArhNr);
										$uusKaheKohaline = substr($eemaldaSari[1], 0, 2);
										if ($vanaKaheKohaline != $uusKaheKohaline) {
											$finalNewFileName = str_replace($uusSarjaTahis.$vanaKaheKohaline, $uusSarjaTahis.$uusKaheKohaline, $finalNewFileName);
										}
									}
									$finalNewFileDir = array_reverse(explode('/', $finalNewFileName));
									unset($finalNewFileDir[0]);
									$finalNewFileDir = implode('/', array_reverse($finalNewFileDir));
									if (!is_dir($finalNewFileDir)) {
										mkdir($finalNewFileDir, 0777, true);
									}
									rename($finalOrigFile, $finalNewFileName);
									foreach ($publicDirs as $publicDir) {
										$handler = opendir($publicDir);
										while ($file = readdir($handler)) {
											if (strpos($file, '.') !== 0) {
												if (stripos($file, $rida['arhiivinumber']) !== false) {
													rename($publicDir . '/' . $file, $publicDir . '/' . str_replace($rida['arhiivinumber'], $uusArhNr, $file));
													break;
												}
											}
										}
										closedir($handler);
									}
									m_insert('logi', [
										'ip' => $_SERVER['REMOTE_ADDR'],
										'syndmus' => 'Materjali <a href="naita.php?t=' . $xtabel . '&id=' . $t_id . '">' . kriips($xtabel) . ' id ' . $t_id . '</a> failinimi serveris on muudetud'
									], ['andmebaasi_lisatud' => 'NOW()']);
								} elseif (($xtabel == 'kasikiri' || $xtabel == 'audio' || $xtabel == 'fotoalbum') && !is_dir($finalNewFileDir)) {
									$handler = opendir($filedir);
									while ($file = readdir($handler)) {
										if (strpos($file, '.') !== 0) {
											if (stripos($file, $rida['arhiivinumber']) !== false) {
												rename($filedir . '/' . $file, $filedir . '/' . str_replace($rida['arhiivinumber'], $uusArhNr, $file));
											}
										}
									}
									closedir($handler);
									rename($filedir, $finalNewFileDir);
									foreach ($publicDirs as $publicDir) {
										$handler = opendir($publicDir);
										while ($file = readdir($handler)) {
											if (strpos($file, '.') !== 0) {
												if (stripos($file, $rida['arhiivinumber']) !== false) {
													rename($publicDir . '/' . $file, $publicDir . '/' . str_replace($rida['arhiivinumber'], $uusArhNr, $file));
												}
											}
										}
										closedir($handler);
									}
									m_insert('logi', [
										'ip' => $_SERVER['REMOTE_ADDR'],
										'syndmus' => 'Materjali <a href="naita.php?t=' . $xtabel . '&id=' . $t_id . '">' . kriips($xtabel) . ' id ' . $t_id . '</a> failinimi serveris on muudetud'
									], ['andmebaasi_lisatud' => 'NOW()']);
								}
							}
						}
					}
					if (lang_exists($t_id)) {
						header('Location: haldus.php?keelte_haldus=1');
						die();
					} else {
						header('Location: naita.php?t=' . $xtabel . '&id=' . $t_id . '&muuda=' . $muutmineOnnestus);
						die();
					}
				}
				else {
					$viga = m_err();
				}
			}
		} //taheti muuta kirjet andmebaasis

		$sisu = '
			<form action="" method="POST">
				<table class="colspace">';
		$r = $rida;
		foreach (get_full_columns($xtabel) as $column) { //genereeritakse html vorm
			if (!in_array($column['Field'], $excludes)) {
				if (isset($_POST[$column['Field']])) {
					$n = $_POST[$column['Field']];
				} else {
					$n = $r[$column['Field']];
				}
				$leiatyyp = explode('(', $column['Type']);
				$fieldType = $leiatyyp[0];
				$fieldTypeLength = 0;
				if (isset($leiatyyp[1])) {
					$fieldTypeLength = explode(')', $leiatyyp[1])[0];
				}
				if ($column['Null'] == 'NO') {
					$tarn = '*';
				} else {
					$tarn = '';
				}
				$sisu .= '<tr><td>' . kriips($column['Field']) . ' ' . $tarn . '</td><td>';
				if ($langTable == 1) {
					$sisu .= '<textarea name="' . $column['Field'] . '" cols="25" rows="3">' . $n . '</textarea>' . pretty_fieldtype($fieldType);
				} elseif (in_array($column['Field'], $allTables)) {
					$shows = get_must_show_columns($column['Field']);
					$kysi_teisest_tabelist = m_select($column['Field'], null, null, ' ORDER BY `' . $shows[0] . '`');
					$sisu .= '<select name="' . $column['Field'] . '" width="300" style="width: 300px"><option value=""></option>';
					while ($rida = m_a($kysi_teisest_tabelist)) {
						if ($rida['id'] == $n) {
							$selectedF = ' selected';
						} else {
							$selectedF = '';
						}
						if ($selectedF == '' && in_array($column['Field'], $infoTables)) {
							if ($n && ucfirst_utf8($n) == $rida['id']) {
								$selectedF = ' selected';
							} else {
								$selectedF = '';
							}
						}
						$sisu .= '
							<option value="' . $rida['id'] . '"' . $selectedF . '>';
						foreach ($shows as $show) {
							$sisu .= ' ' . $rida[$show];
						}
						$sisu .= '</option>';
					}
					$sisu .= '</select>';
					$sisu .= ' <a href="lisa.php?t=' . $column['Field'] . '&u=' . $xtabel . '&id=' . $t_id . '&ref=muuda">' . $lang['lisa_uus'] . '</a>';
				} elseif ($column['Field'] == 'ressursi_tyyp') {
					$n2 = 'ressursi_tyyp' . $n;
					$$n2 = 'selected';
					$sisu .= '
					<select name="ressursi_tyyp">
						<option value=""></option>
						<option value="1" ' . ($ressursi_tyyp1 ?? '') . '>' . $lang['fyysiline'] . '</option>
						<option value="2" ' . ($ressursi_tyyp2 ?? '') . '>' . $lang['digitaalne'] . '</option>
					</select>';
				} elseif ($column['Field'] == 'fyysiliselt_olemas') {
					$n2 = 'fyysiliselt_olemas' . $n;
					$$n2 = 'selected';
					$sisu .= '
					<select name="fyysiliselt_olemas">
						<option value=""></option>
						<option value="1" ' . ($fyysiliselt_olemas1 ?? '') . '>' . $lang['olemas'] . '</option>
						<option value="2" ' . ($fyysiliselt_olemas2 ?? '') . '>' . $lang['kadunud'] . '</option>
						<option value="3" ' . ($fyysiliselt_olemas3 ?? '') . '>' . $lang['laenatud'] . '</option>
					</select>';
				} elseif ($column['Field'] == 'digitaalselt_olemas') {
					$n2 = 'digitaalselt_olemas' . $n;
					$$n2 = 'selected';
					$sisu .= '
					<select name="digitaalselt_olemas">
						<option value=""></option>
						<option value="1" ' . ($digitaalselt_olemas1 ?? '') . '>' . $lang['jah'] . '</option>
						<option value="2" ' . ($digitaalselt_olemas2 ?? '') . '>' . $lang['ei'] . '</option>
					</select>';
				} elseif ($column['Field'] == 'oigused') {
					$n2 = 'oigused' . $n;
					$$n2 = 'selected';
					$sisu .= '
						<select name="oigused">
							<option value="avalik" ' . ($oigusedavalik ?? '') . '>' . $lang['avalik'] . '</option>
							<option value="mitteavalik" ' . ($oigusedmitteavalik ?? '') . '>' . $lang['mitteavalik'] . '</option>
						</select>';
				} elseif ($column['Field'] == 'materjal') {
					$sisu .= '
						<select name="materjal" onchange="generate_list(this.value, \'' . $xtabel . '\');">';
					foreach ($publicTables as $value) {
						if ($n == $value) {
							$selected = ' selected';
						} else {
							$selected = '';
						}
						$sisu .= '
							<option value="' . $value . '"' . $selected . '>' . kriips($value) . '</option>';
					}
					$sisu .= '</select>';
				} elseif ($column['Field'] == 'materjal_id') {
					$must_have_columns = get_must_show_columns($r['materjal']);
					$paring = m_select($r["materjal"], null, null, ' ORDER BY `' . $must_have_columns[0] . '`');
					$sisu .= '<span id="uus_' . $xtabel . '_' . $xtabel . '"><select name="materjal_id" width="300" style="width: 300px">';
					while ($rida = m_a($paring)) {
						if ($n == $rida['id']) {
							$selected = ' selected';
						} else {
							$selected = '';
						}
						$sisu .= '
						<option value="' . $rida['id'] . '"' . $selected . '>';
						foreach ($must_have_columns as $value) {
							if ($value == 'kestus') {
								$sisu .= pretty_length($rida[$value]) . ' ';
							} else {
								$sisu .= $rida[$value] . ' ';
							}
						}
						$sisu .= '</option>';
					}
					$sisu .= '</select></span>';
				} elseif ($column['Field'] == 'kestus') {
					$sisu .= '<input type="text" name="kestus" value="' . pretty_length($n) . '"> numbrid [TT:][MM:]SS';
				} elseif ($column['Field'] == 'materjali_tyyp') {
					$sisu .= '
						<select name="materjali_tyyp" onchange="ab(\'' . $xtabel . $u . '\');">';
					foreach ($publicTables as $public_table) {
						if ($n == $public_table) {
							$selected = ' selected';
						} else {
							$selected = '';
						}
						$sisu .= '<option value="' . $public_table . '"' . $selected . '>' . kriips($public_table) . '</option>';
					}
					$sisu .= '</select>';
				} elseif ($fieldType == 'text') {
					$sisu .= '<textarea name="' . $column['Field'] . '" cols="25" rows="3">' . $n . '</textarea>' . pretty_fieldtype($fieldType);
				} elseif ($fieldType == 'date') {
					$sisu .= '<input type="text" name="' . $column['Field'] . '" value="' . date_to_est($n) . '">' . pretty_fieldtype($fieldType);
				} else {
					$sisu .= '<input type="text" name="' . $column['Field'] . '" value="' . $n . '">' . pretty_fieldtype($fieldType, $fieldTypeLength);
				}
				$sisu .= '</td></tr>';
			}
		} //genereeriti html vorm
		$sisu .= '
				<tr><td></td><td><input type="submit" name="submit_muuda" value="' . $lang['muuda'] . ' ' . $tabel1 . '"></td></tr>
				</table>
			</form>';
	} else {
		$viga = $lang['tabeli_kirje_valimata_voi_kirjet_pole_olemas'];
	}
} else {
	$viga = $lang['tabel_valimata_voi_tabelit_pole_olemas'];
}
include('kujundus.php');
