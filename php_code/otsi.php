﻿<?php

function gentime() {
	static $a;
	if ($a == 0) {
		$a = microtime(true);
		return null;
	}
	return (string)(microtime(true) - $a);
}
gentime();

include('base.php');
if ($sess = sisse_logitud()) { //sisse logitud
	if (!$minu_grupi_oigused = grupi_oigused($sess)) {
		$minu_grupi_oigused = 0;
	}
	if ($minu_grupi_oigused == 2) {
		$tabelid = get_searchable_tables();
		$excludes = excludes();
	} elseif ($minu_grupi_oigused == 3) {
		$tabelid = get_searchable_tables();
		$excludes = excludes(0);
	} else {
		$tabelid = get_public_tables();
		$excludes = excludes();
	}
	$where = '';

	$minuLemmikud = m_q('SELECT * FROM lemmikud WHERE lisaja = :lisaja', ['lisaja' => $sess['id']]);
	$lemmikud = [];
	while ($lemmik = m_a($minuLemmikud)) {
		$lemmikud[] = $lemmik['tabel'] . '-' . $lemmik['vali'];
	}
	$jsLemmik = '';
	$minuLemmikudLink = 'seaded.php';
} else { //välja logitud
	$minu_grupi_oigused = 0;
	$tabelid = get_public_tables();
	if (isset($_SESSION['lemmikud'])) {
		$lemmikud = $_SESSION['lemmikud'];
	} else {
		$lemmikud = [];
	}
	$jsLemmik = '_a';
	$minuLemmikudLink = 'minu_lemmikud.php';
	$where = " oigused='avalik' AND ";
	$excludes = excludes();
}

$nonSystemTables = get_non_system_tables();
$publicTables = get_public_tables();

$orig_aeg = [
	'audio_trakk' => 'salvestusaeg',
	'video_trakk' => 'salvestusaeg',
	'foto' => 'pildistamisaeg',
	'kasikiri' => 'koostamise_aeg'
];

$orderRollidBy = ' ORDER BY FIELD(rolli_nimetus';
$rolli_nimetus = m_q('SELECT id FROM rolli_nimetus ORDER BY jrknr DESC');
while ($rolli_nimi = m_a($rolli_nimetus)) {
	$orderRollidBy .= ',\'' . $rolli_nimi['id'] . '\'';
}
$orderRollidBy .= ') DESC';

if (isset($_GET['otsi_andmebaasist'])) {
	$_SESSION['otsing'] = $_SERVER['REQUEST_URI'];
	$pealkiri1 = $lang['otsi'];
	$mida = $_GET['mida'];
	m_insert('otsingu_logi', [
		'ip' => $_SERVER['REMOTE_ADDR'],
		'sona' => htmlspecialchars($mida),
		'tabel' => 'lihtotsing',
		'grupp' => $minu_grupi_oigused
	], ['andmebaasi_lisatud' => 'NOW()']);
	$mida = str_replace('*', '%', $mida);
	$equals = $_GET['equals'] ?? null;
	if ($equals == 'jah') {
		$checked_box = 'checked';
	} else {
		$checked_box = '';
	}
	if ($mida == '') {
		$viga = $lang['tyhi_otsing'];
	} else {
		$sisu = <<<SISU
<form action="" method="GET">
	<table width="100%">
	<tr><td><a href="{$minuLemmikudLink}">{$lang['minu_lemmikud']}</a></td></tr>
	<tr><td>
	{$lang['otsi_andmebaasist']}<br />
	{$lang['tapne_vaste']} <input type="checkbox" name="equals" value="jah" {$checked_box} /><br />
	<input type="text" name="mida" value="{$mida}"> <input type="submit" name="otsi_andmebaasist" value="{$lang['otsi']}">
	</td></tr>
	<tr><td>{$lang['otsi_paremalt']}</td></tr>
	</table>
</form>
SISU;
		$kestusKokku = 0;
		$kestusKokku2 = 0;
		$lehekylgiKokku = 0;
		$finalResults = [];
		$rowCounts = [];
		$collect = explode(' ', $mida);
		if ($equals != 'jah') {
			foreach ($collect as $i => $item) {
				$collect[$i] = '%' . $item . '%';
			}
		}
		foreach ($tabelid as $tabel) {
			$whereIn = [];
			if (in_array($tabel, $publicTables)) {
				$params = [
					'materjal' => $tabel
				];
				$searches = [];
				$veerud = get_columns('osaleja', $excludes);
				foreach ($collect as $i => $item) {
					$whereIn2 = [];
					$query = m_q('SELECT id FROM kihelkond WHERE kihelkonna_nimi LIKE :kihelkonna_nimi', ['kihelkonna_nimi' => $collect[$i]]);
					while ($row = m_a($query)) {
						$whereIn2[] = $row['id'];
					}
					$search = [];
					foreach ($veerud as $veerg) {
						$search[] = '`' . $veerg . '` LIKE :' . $veerg . $i;
						$params[$veerg . $i] = $collect[$i];
						if ($veerg == 'kihelkond' && !empty($whereIn2)) {
							$search[] = '`' . $veerg . '` IN (:' . $veerg . '_in_' . $i . ')';
							$params[$veerg . '_in_' . $i] = $whereIn2;
						}
					}
					$searches[] = '(' . implode(' OR ', $search) . ')';
				}
				if ($otsi = m_qp('SELECT materjal_id FROM roll WHERE materjal = :materjal AND osaleja IN (SELECT id FROM osaleja WHERE ' . implode(' AND ', $searches) . ')', $params)) {
					while ($rida = m_a($otsi)) {
						$whereIn[] = $rida['materjal_id'];
					}
				}
			}
			$sqlPlus = '';
			$params = [];
			$searches = [];
			$veerud = get_columns($tabel, $excludes);
			foreach ($collect as $i => $item) {
				$search = [];
				foreach ($veerud as $veerg) {
					if ($veerg != 'keel') {
						$search[] = '`' . $veerg . '` LIKE :' . $veerg . $i;
						$params[$veerg . $i] = $item;
					}
				}
				$searches[] = '(' . implode(' OR ', $search) . ')';
			}
			$sqlPlus = implode(' AND ', $searches);
			if (in_array('keel', $veerud)) {
				$sqlKeel = [];
				$paramsKeel = [
					'materjal' => $tabel
				];
				foreach ($collect as $i => $mida) {
					$sqlKeel[] = 'keel LIKE :keel' . $i;
					$paramsKeel['keel' . $i] = $mida;
				}
				$otsi = m_q('SELECT materjal_id FROM materjal_keel WHERE materjal = :materjal AND (' . implode(' OR ', $sqlKeel) . ')', $paramsKeel);
				while ($rida = m_a($otsi)) {
					$whereIn[] = $rida['materjal_id'];
				}
			}
			if (!empty($whereIn)) {
				$sqlPlus .= ' OR id IN (:id)';
				$params['id'] = $whereIn;
			}
			if ($otsi = m_qp('SELECT * FROM ' . $tabel . ' WHERE ' . $where . ' (' . $sqlPlus . ') ORDER BY id DESC', $params)) {
				while ($rida = m_a($otsi)) {
					$finalResults[] = [$rida, $tabel];
				}
				$rowCounts[$tabel] = m_r($otsi);
			}
			unset($whereIn);
		}
		//teeme kokkuvõtted ära ja loeme üle mitu vastust leiti
		$whereIn = [];
		$showOnMap = [];
		$participants = [];
		for ($i = 0; isset($finalResults[$i]); $i++) {
			list($finalResultRow, $finalResultTable) = $finalResults[$i];
			if (!isset($currentTable)) {
				$currentTable = $finalResultTable;
			}
			if (in_array($currentTable, $publicTables)) {
				$whereIn[] = $finalResultRow['id'];
			}
			if (!isset($finalResults[$i+1]) || $finalResults[$i+1][1] != $currentTable) {
				if (!empty($whereIn)) {
					if ($currentTable == 'audio_trakk') {
						$paring = m_qp('SELECT SUM(kestus) AS kestus_kokku FROM audio_trakk WHERE id IN (:id)', ['id' => $whereIn]);
						if ($paring && $rida = m_a($paring)) {
							$kestusKokku += $rida['kestus_kokku'];
						}
					} elseif ($currentTable == 'video_trakk') {
						$paring = m_qp('SELECT SUM(kestus) AS kestus_kokku FROM video_trakk WHERE id IN (:id)', ['id' => $whereIn]);
						if ($paring && $rida = m_a($paring)) {
							$kestusKokku2 += $rida['kestus_kokku'];
						}
					} elseif ($currentTable == 'kasikiri') {
						$paring = m_qp('SELECT SUM(lehekylgede_arv) AS lehekylgede_arv_kokku FROM kasikiri WHERE id IN (:id)', ['id' => $whereIn]);
						if ($paring && $rida = m_a($paring)) {
							$lehekylgiKokku += $rida['lehekylgede_arv_kokku'];
						}
					}
					$rolesQuery = m_qp('SELECT * FROM roll JOIN osaleja ON osaleja.id = roll.osaleja JOIN rolli_nimetus ON rolli_nimetus.id = roll.rolli_nimetus WHERE roll.materjal = :materjal AND roll.materjal_id IN (:materjal_id)' . $orderRollidBy, [
						'materjal' => $currentTable,
						'materjal_id' => $whereIn
					]);
					while ($rolesQuery && $roleRow = m_a($rolesQuery)) {
						$participants[$currentTable][$roleRow['materjal_id']][] = $roleRow;
					}
				}
				unset($currentTable);
				$whereIn = [];
			}
			if (!empty($finalResultRow['lat']) && !empty($finalResultRow['lon'])) {
				$mustShowColumns = get_must_show_columns($finalResultTable);
				$popup = [];
				foreach ($mustShowColumns as $mustShowColumn) {
					if (isset($finalResultRow[$mustShowColumn])) {
						$popup[] = $finalResultRow[$mustShowColumn];
					}
				}
				$key = $finalResultRow['lat'] . '/' . $finalResultRow['lon'];
				$popup = '<a href="naita.php?t=' . $finalResultTable . '&amp;id=' . $finalResultRow['id'] . '">' . implode(' ', $popup);
				if (!isset($showOnMap[$key])) {
					$showOnMap[$key] = $popup;
				} else {
					$showOnMap[$key] .= '<br>' . $popup;
				}
			}
		}
		for ($i = 0; isset($finalResults[$i]); $i++) {
			list($finalResultRow, $finalResultTable) = $finalResults[$i];
			if (isset($participants[$finalResultTable][$finalResultRow['id']])) {
				foreach ($participants[$finalResultTable][$finalResultRow['id']] as $participant) {
					if ($participant['materjali_geoinfo'] && !empty($participant['lat']) && !empty($participant['lon'])) {
						$mustShowColumns = get_must_show_columns($finalResultTable);
						$popup = [];
						foreach ($mustShowColumns as $mustShowColumn) {
							if (isset($finalResultRow[$mustShowColumn])) {
								$popup[] = $finalResultRow[$mustShowColumn];
							}
						}
						$key = $participant['lat'] . '/' . $participant['lon'];
						$popup = '<a href="naita.php?t=' . $finalResultTable . '&amp;id=' . $finalResultRow['id'] . '">' . implode(' ', $popup);
						if (!isset($showOnMap[$key])) {
							$showOnMap[$key] = $popup;
						} else {
							$showOnMap[$key] .= '<br>' . $popup;
						}
					}
				}
			}
		}
		$noOfResults = count($finalResults);
		if ($noOfResults == 0) {
			$viga = $lang['midagi_ei_leitud'];
		} else {
			$teade = $lang['leiti'] . ' ' . $noOfResults . ' ' . $lang['vastust'];
			foreach ($rowCounts as $tabel => $rowCount) {
				if ($rowCount) {
					$teade .= '<br />' . kriips($tabel) . ': ' . $rowCount . ' ' . $lang['rida'];
				}
				if ($tabel == 'audio_trakk' && $kestusKokku > 0) {
					$teade .= '<br />' . $lang['trakkide_kogupikkus_on'] . ' ' . pretty_length($kestusKokku);
				}
				if ($tabel == 'video_trakk' && $kestusKokku2 > 0) {
					$teade .= '<br />' . $lang['video_trakkide_kogupikkus_on'] . ' ' . pretty_length($kestusKokku2);
				}
				if ($tabel == 'kasikiri' && $lehekylgiKokku > 0) {
					$teade .= '<br />' . $lang['lehekylgi_kokku_on'] . ' ' . $lehekylgiKokku;
				}
			}

			$riduLehel = 25; //ühel lehel näidatakse nii mitu rida
			if (isset($_GET['l'])) {
				$leheNr = (int)$_GET['l'];
			} else {
				$leheNr = 0;
			}
			$naitaAlates = $leheNr * $riduLehel; //arvuta mitmendast reast näitama hakatakse
			if ($noOfResults > $riduLehel) {
				if (isset($_GET['l'])) {
					$otsinguParing = explode('&l=', $_SERVER['REQUEST_URI']);
					$sisu .= pages(rawurldecode($otsinguParing[0]) . '&l=%d', $leheNr, $noOfResults, $riduLehel);
				} else {
					$sisu .= pages(rawurldecode($_SERVER['REQUEST_URI']) . '&l=%d', $leheNr, $noOfResults, $riduLehel);
				}
			}
			//kuvame lehekülje jagu tulemusi
			$rowsToShow = [];
			$naitaKuni = $naitaAlates + $riduLehel;
			unset($currentTable);
			for ($whereIn = []; $naitaAlates < $naitaKuni; $naitaAlates++) {
				if (!isset($finalResults[$naitaAlates])) {
					break;
				}
				list($finalResultRow, $finalResultTable) = $finalResults[$naitaAlates];
				if (!isset($currentTable)) {
					$currentTable = $finalResultTable;
				}
				$whereIn[] = $finalResultRow['id'];
				if ($naitaAlates == $naitaKuni - 1 || !isset($finalResults[$naitaAlates + 1][1]) || $finalResults[$naitaAlates + 1][1] != $currentTable) {
					if (!empty($whereIn)) {
						$rowsToShow[$currentTable] = m_select($currentTable, ['id' => $whereIn]);
					}
					unset($currentTable);
					$whereIn = [];
				}
			}
			if (count($showOnMap)) {
				$points = json_encode($showOnMap);
				$injectCssFiles = ['<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />'];
				$sisu .= '<table><tr><td><div id="terrain" style="width: 600px;height: 400px;margin: 0 0 1em 0;"></div></td></tr>';
				$sisu .= '<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>';
				$sisu .= <<<MAPJS
				<script type="text/javascript">
					var mymap = L.map('terrain').setView([58.55, 25.05], 7);
					L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
						attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
					}).addTo(mymap);

					var points = {$points};
					var markers = [];
					for (var pointKey in points) {
						var point = pointKey.split('/');
						markers.push(L.marker({ lat: point[0], lon: point[1] }).bindPopup(points[pointKey]).addTo(mymap));
					}

					var group = new L.featureGroup(markers);
					mymap.fitBounds(group.getBounds(), {
						maxZoom: 9
					});
				</script>
MAPJS;
			}
			foreach ($rowsToShow as $tabel => $read) {
				$must_show_columns = get_must_show_columns($tabel);
				if (in_array($tabel, $publicTables)) {
					$must_show_columns[] = 'roll';
				}
				$sisu .= '<table class="colspace">';
				$sisu .= '<tr><th><h2>' . kriips($tabel) . '</h2></th></tr>';
				$sisu .= '<tr>';
				$sisu .= '<td></td>';
				foreach ($must_show_columns as $value) {
					$sisu .= '<td>' . kriips($value) . '</td>';
				}
				if ($tabel == 'osaleja') {
					$sisu .= '<td>' . $lang['arhiivimaterjal'] . '</td>';
				}
				$sisu .= '</tr>';
				while ($rida = m_a($read)) {
					$resource_name = $tabel . '-' . $rida['id'];
					$sisu .= '<tr>';
					$sisu .= '<td style="white-space: nowrap;">';
					if ($minu_grupi_oigused > 2 || $rida['lisaja'] == $sess['id']) {
						$sisu .= '
						<a href="muuda.php?t=' . $tabel . '&id=' . $rida['id'] . '"><img src="kujundus/pildid/muuda.png" title="' . $lang['muuda'] . '" alt="' . $lang['muuda'] . '" /></a> 
						<a href="kustuta.php?t=' . $tabel . '&id=' . $rida['id'] . '"><img src="kujundus/pildid/kustuta.png" title="' . $lang['kustuta'] . '" alt="' . $lang['kustuta'] . '" /></a> ';
					}
					$sisu .= '
						<span id="' . $resource_name . '">';
					if (in_array($resource_name, $lemmikud)) {
						$sisu .= '<img src="kujundus/pildid/ok.png" alt="' . $lang['sinu_lemmik'] . '" title="' . $lang['sinu_lemmik'] . '">';
					} else {
						$sisu .= '<img src="kujundus/pildid/lemmik.png" title="' . $lang['lisa_lemmikutesse'] . '" alt="' . $lang['lisa_lemmikutesse'] . '" onclick="lisa_lemmik' . $jsLemmik . '(\'' . $tabel . '\', \'' . $rida['id'] . '\'); this.style.display=\'none\'; " />';
					}
					$sisu .= '</span>';
					if (isset($rida['digitaalselt_olemas']) && $rida['digitaalselt_olemas'] == 1) {
						$sisu .= ' <img src="kujundus/pildid/digi.png" alt="' . kriips('digitaalselt_olemas') . '" title="' . kriips('digitaalselt_olemas') . '" />';
					} elseif (isset($rida['fyysiliselt_olemas']) && $rida['fyysiliselt_olemas'] == 1) {
						$sisu .= ' <img src="kujundus/pildid/fyysiline.png" alt="' . kriips('fyysiliselt_olemas') . '" title="' . kriips('fyysiliselt_olemas') . '" />';
					} elseif (isset($rida['ressursi_tyyp']) && ($rida['ressursi_tyyp'] == 1 && $rida['fyysiliselt_olemas'] == 2 || $rida['ressursi_tyyp'] == 2 && $rida['digitaalselt_olemas'] == 2)) {
						$sisu .= ' <img src="kujundus/pildid/kadunud.png" alt="' . $lang['pole_saadaval'] . '" title="' . $lang['pole_saadaval'] . '" />';
					}
					$sisu .= '</td>';
					foreach ($must_show_columns as $value) {
						$sisu .= '<td>';
						if ($minu_grupi_oigused > 0 || $tabel != 'osaleja') {
							$sisu .= '<a href="naita.php?t=' . $tabel . '&id=' . $rida['id'] . '">';
						}
						if ($value == 'kestus') {
							$sisu .= ' ' . pretty_length($rida[$value]);
						} elseif ($value == 'materjal') {
							if ($query = m_select($rida['materjal'], ['id' => $rida['materjal_id']])) {
								$sisu .= kriips($rida['materjal']);
								$row = m_a($query);
								$must_show_columns2 = get_must_show_columns($rida['materjal']);
								foreach ($must_show_columns2 as $value2) {
									if ($value2 == 'kestus') {
										$sisu .= ' ' . pretty_length($row[$value2]);
									} else {
										$sisu .= ' ' . $row[$value2];
									}
								}
							}
						} elseif ($value == 'roll') {
							if (isset($participants[$tabel][$rida['id']])) {
								foreach ($participants[$tabel][$rida['id']] as $participant) {
									$must_show_columns2 = get_must_show_columns('roll');
									$must_show_columns3 = get_must_show_columns('osaleja');
									foreach ($must_show_columns2 as $value2) {
										if ($value2 == 'vanus' && ($participant['vanus'] || $participant['vanus'] = vanus($participant['synniaasta'], $rida[$orig_aeg[$tabel]]))) {
											$sisu .= ' (' . $participant[$value2] . ')';
										} elseif ($value2 == 'materjal' || $value2 == 'materjal_id') {
										} elseif ($value2 == 'osaleja') {
											foreach ($must_show_columns3 as $value3) {
												$sisu .= ' ' . $participant[$value3];
											}
										} else {
											$sisu .= ' ' . $participant[$value2];
										}
									}
									$sisu .= '<br />';
								}
							}
						} elseif (in_array($value, $nonSystemTables)) {
							if ($query = m_select($value, ['id' => $rida[$value]])) {
								$row = m_a($query);
								$must_show_columns2 = get_must_show_columns($value);
								foreach ($must_show_columns2 as $value2) {
									$sisu .= ' ' . $row[$value2];
								}
							}
						} else {
							$sisu .= korrasta($rida[$value], 40);
						}
						$sisu .= '</a></td>';
					}
					if ($tabel == 'osaleja') {
						$sisu .= '<td>';
						$rollidParing = m_q('SELECT * FROM roll WHERE osaleja = :osaleja', ['osaleja' => $rida['id']]);
						while ($rollidRida = m_a($rollidParing)) {
							$sisu .= kriips($rollidRida['materjal']) . ': ';
							if ($teineParing = m_select($rollidRida['materjal'], ['id' => $rollidRida["materjal_id"]])) {
								$teineRida = m_a($teineParing);
								$must_show_columns2 = get_must_show_columns($rollidRida['materjal']);
								$sisu .= '<a href="naita.php?t=' . $rollidRida['materjal'] . '&id=' . $rollidRida['materjal_id'] . '">';
								foreach ($must_show_columns2 as $value2) {
									if ($value2 == 'kestus') {
										$sisu .= ' ' . pretty_length($teineRida[$value2]);
									} else {
										$sisu .= ' ' . $teineRida[$value2];
									}
								}
								$sisu .= '</a>';
							}
							$sisu .= '<br />';
						}
						$sisu .= '</td>';
					}
					$sisu .= '</tr>';
				}
				$sisu .= '<tr><td><br /></td></tr>';
				$sisu .= '</table>';
			}
		}
	}
} elseif (isset($_GET['tabel'])) { //tahetkse saada otsimise vormi
	$xtabel = $_GET['tabel'];
	if (!in_array($xtabel, $tabelid)) {
		$viga = $lang['tabelit_ei_leitud'];
	} else {
		$sisu = '';
		$columns1 = get_full_columns($xtabel, $excludes);
		if (in_array($xtabel, $publicTables)) {
			if ($xtabel != 'kasikiri') {
				array_unshift($columns1, ['Field' => 'sari', 'Type' => '']);
			}
			$columns2 = get_full_columns('osaleja', $excludes);
		}
		if (isset($_GET[$xtabel])) { //tahetakse otsida
			if (in_array($xtabel, $publicTables)) {
				$veeruNimed = array_merge($columns1, $columns2);
			} else {
				$veeruNimed = $columns1;
			}
			$_SESSION['otsing'] = $_SERVER['REQUEST_URI'];
			$r = $_GET;
			$count1 = 0;
			$count2 = 0;
			$sql1 = ''; // päring peatabelisse
			$sql1Params = [];
			$sql2 = ''; // päring osalejate tabelisse
			$sql2Params = [];
			$searchString = '';
			$whereIn = [];
			foreach ($veeruNimed as $columns) {
				if (in_array($columns, $columns1)) {
					$sql = $sql1;
					$params = $sql1Params;
					$count = $count1;
				} else {
					$sql = $sql2;
					$params = $sql2Params;
					$count = $count2;
				}
				$n = $r[$columns['Field']]; //muuda ära erisümbolid
				$tyyp = $columns['Type']; //otsitava andme tüüp
				$tyyp = explode('(', $tyyp);
				$tyyp = $tyyp[0]; //int, varchar, date, datetime etc
				if ($tyyp != 'date' && (strpos($columns['Field'], 'aeg') !== false || strpos($columns['Field'], 'aasta') !== false)) {
					$n22 = $columns['Field'] . '2';
					$n2 = $r[$n22];
					if ($n2 != '' && $n == '') {
						$n = '0';
					}
				}
				if ($tyyp != 'date' && !in_array($columns['Field'], $excludes) && (($r[$columns['Field'] . '_equals'] ?? null) == 'checked' || $n != '')) { //eemaldame päringust mittevajaliku
					if ($count > 0) {
						$sql .= ' AND ';
					}
					if ($tyyp == 'int') {
						$n33 = $columns['Field'] . '_equals';
						$n3 = $r[$n33];
						if ($n3 == 'checked') {
							$sql .= $columns['Field'] . ' LIKE :' . $columns['Field'];
							$params[$columns['Field']] = $n;
							$searchString .= $columns['Field'] . '=' . $n . ', ';
						} else {
							$sql .= $columns['Field'] . ' >= :' . $columns['Field'];
							$params[$columns['Field']] = $n;
							$searchString .= $columns['Field'] . ' >= ' . $n . ', ';
							$n22 = $columns['Field'] . '2';
							$n2 = $r[$n22];
							if ($n2 != '') {
								$sql .= ' AND ' . $columns['Field'] . ' <= :' . $n22;
								$params[$n22] = $n2;
								$searchString .= $columns['Field'] . '<=' . $n . ', ';
								$count++;
							}
						}
					} else {
						$n = str_replace('*', '%', $n);
						$n33 = $columns['Field'] . '_equals';
						$n3 = $r[$n33] ?? null;
						if (strpos($columns['Field'], 'aeg') !== false || strpos($columns['Field'], 'aasta') !== false) {
							$n22 = $columns['Field'] . '2';
							$n2 = $r[$n22];
							if ($n3 == 'checked') {
								$sql .= 'SUBSTR(' . $columns['Field'] . ', -4) LIKE :' . $columns['Field'];
								$params[$columns['Field']] = $n;
								$searchString .= $columns['Field'] . '=' . $n . ', ';
							} else {
								$sql .= 'SUBSTR(' . $columns['Field'] . ', -4) >= :' . $columns['Field'];
								$params[$columns['Field']] = $n;
								$searchString .= $columns['Field'] . '>=' . $n . ', ';
								if ($n2 != '') {
									$sql .= ' AND SUBSTR(' . $columns['Field'] . ', -4) <= :' . $n22;
									$params[$n22] = $n2;
									$searchString .= $columns['Field'] . '<=' . $n . ', ';
									$count++;
								}
							}
						} elseif ($columns['Field'] == 'keel') {
							if ($n3 == 'checked') {
								$n = '%' . $n . '%';
							}
							$query = m_q('SELECT materjal_id FROM materjal_keel WHERE materjal = :materjal AND keel LIKE :keel', [
								'materjal' => $xtabel,
								'keel' => $n
							]);
							while ($row = m_a($query)) {
								$whereIn[] = $row['materjal_id'];
							}
							if (!empty($whereIn)) {
								$sql .= 'id IN (:id)';
								$params['id'] = $whereIn;
							}
							$searchString .= $columns['Field'] . '=' . $n . ', ';
						} elseif ($columns['Field'] == 'sari' && in_array($xtabel, $publicTables) && $xtabel != 'kasikiri') {
							if ($xtabel == 'audio_trakk') {
								$field = 'audio';
							} elseif ($xtabel == 'video_trakk') {
								$field = 'video';
							} elseif ($xtabel == 'foto') {
								$field = 'fotoalbum';
							} else {
								continue;
							}
							$sql .= $field . ' IN (SELECT id FROM ' . $field . ' WHERE sari LIKE :sari)';
							$params['sari'] = $n;
							$searchString .= $columns['Field'] . '=' . $n . ', ';
						} elseif ($n3 == 'checked') {
							$sql .= $columns['Field'] . ' LIKE :' . $columns['Field'];
							$params[$columns['Field']] = $n;
							$searchString .= $columns['Field'] . '=' . $n . ', ';
						} else {
							$sql .= $columns['Field'] . ' LIKE :' . $columns['Field'];
							$params[$columns['Field']] = '%' . $n . '%';
							$searchString .= $columns['Field'] . '=*' . $n . '*, ';
						}
					}
					$count++;
				}
				if ($tyyp == 'date') {
					$n33 = $columns['Field'] . '_equals';
					$n3 = $r[$n33] ?? null;
					if ($n3 == 'checked') {
						if ($count > 0) {
							$sql .= ' AND ';
						}
						$sql .= $columns['Field'] . ' LIKE :' . $columns['Field'];
						$params[$columns['Field']] = date_to_mysql($n);
						$searchString .= $columns['Field'] . '=' . $n . ', ';
						$count++;
					} else {
						if ($n != '' && $kuup = date_to_mysql($n)) {
							if ($count > 0) {
								$sql .= ' AND ';
							}
							$sql .= $columns['Field'] . ' >= :' . $columns['Field'];
							$params[$columns['Field']] = $kuup;
							$searchString .= $columns['Field'] . '>=' . $n . ', ';
							$count++;
						}
						$n22 = $columns['Field'] . '2';
						$n2 = $r[$n22];
						if ($n2 != '' && $kuup = date_to_mysql($n2)) {
							if ($count > 0) {
								$sql .= ' AND ';
							}
							$sql .= $columns['Field'] . ' <= :' . $n22;
							$params[$n22] = $kuup;
							$searchString .= $columns['Field'] . '<=' . $n . ', ';
							$count++;
						}
					}
				}
				if (in_array($columns, $columns1)) {
					$sql1 = $sql;
					$sql1Params = $params;
					$count1 = $count;
				} else {
					$sql2 = $sql;
					$sql2Params = $params;
					$count2 = $count;
				}
			}
			$count = $count1 + $count2;

			$must_show_columns = get_must_show_columns($xtabel);
			if (in_array($xtabel, $publicTables)) {
				$must_show_columns[] = 'roll';
			}
			$rolli_nimetus = $r['rolli_nimetus'] ?? null;
			$sql3 = '';
			$sql3Params = [];
			if ($rolli_nimetus) {
				if ($r['rolli_nimetus_equals'] == 'checked') {
					$sql3 = ' AND rolli_nimetus LIKE :rolli_nimetus';
					$sql3Params['rolli_nimetus'] = $rolli_nimetus;
					$searchString .= 'rolli_nimetus=' . $rolli_nimetus . ', ';
				} else {
					$sql3 = ' AND rolli_nimetus LIKE :rolli_nimetus';
					$sql3Params['rolli_nimetus'] = '%' . $rolli_nimetus . '%';
					$searchString .= 'rolli_nimetus=*' . $rolli_nimetus . '*, ';
				}
				$count++;
			}

			$kestusKokku = 0;
			$lehekylgiKokku = 0;
			$korduvateKontroll = [];
			$whereIn = [];
			if ($sql2 || $sql3) {
				if ($sql2) {
					$sql2 = ' AND osaleja IN (SELECT id FROM osaleja WHERE (' . $sql2 . '))';
				}
				$paring3 = m_qp('SELECT materjal_id FROM roll WHERE materjal = :materjal1' . $sql3 . $sql2, [
					'materjal1' => $xtabel
				] + $sql2Params + $sql3Params);
				while ($kokkuvotteRida3 = m_a($paring3)) {
					$whereIn[] = $kokkuvotteRida3['materjal_id'];
				}
			}
			if ($sql1 || $sql2 && !empty($whereIn) || $sql3 && !empty($whereIn)) {
				if (!empty($whereIn)) {
					if ($sql1) {
						$sql1 .= ' AND ';
					}
					$sql1 .= 'id IN (:id2)';
					$sql1Params['id2'] = $whereIn;
				}
				if ($xtabel == 'audio_trakk' || $xtabel == 'video_trakk') {
					$paring = m_qp('SELECT SUM(kestus) AS kestus_summa FROM ' . $xtabel . ' WHERE ' . $where . '(' . $sql1 . ')', $sql1Params) or $viga = m_err();
					$paring = m_a($paring);
					$kestusKokku = $paring['kestus_summa'];
				} elseif ($xtabel == "kasikiri") {
					$paring = m_qp('SELECT SUM(lehekylgede_arv) AS lehekylgede_arv_summa FROM ' . $xtabel . ' WHERE ' . $where . '(' . $sql1 . ')', $sql1Params) or $viga = m_err();
					$paring = m_a($paring);
					$lehekylgiKokku = $paring['lehekylgede_arv_summa'];
				}
				if ($searchString) {
					$searchString = rtrim($searchString, ', ');
					m_insert('otsingu_logi', [
						'ip' => $_SERVER['REMOTE_ADDR'],
						'sona' => htmlspecialchars($searchString),
						'tabel' => $xtabel,
						'grupp' => $minu_grupi_oigused
					], ['andmebaasi_lisatud' => 'NOW()']);
				}
				$paring = m_qp('SELECT id FROM ' . $xtabel . ' WHERE ' . $where . '(' . $sql1 . ')', $sql1Params) or $viga = m_err();
				while ($rida = m_a($paring)) {
					$korduvateKontroll[] = $rida['id'];
				}
			}
			$riduLehel = 25; //ühel lehel näidatakse nii mitu rida
			if (isset($_GET['l'])) {
				$leheNr = (int)$_GET['l'];
			} else {
				$leheNr = 0;
			}
			$naitaAlates = $leheNr * $riduLehel; //arvuta mitmendast reast näitama hakatakse
			$naitaRidu = count($korduvateKontroll);
			if ($naitaRidu > $riduLehel) {
				if (isset($_GET['l'])) {
					$otsinguParing = explode('&l=', $_SERVER['REQUEST_URI']);
					$lehed = pages(rawurldecode($otsinguParing[0]) . '&l=%d', $leheNr, $naitaRidu, $riduLehel);
				} else {
					$lehed = pages(rawurldecode($_SERVER['REQUEST_URI']) . '&l=%d', $leheNr, $naitaRidu, $riduLehel);
				}
			} else {
				$lehed = '';
			}
			if ($naitaRidu > 0) {
				rsort($korduvateKontroll);
				$sisu .= $lehed . '<table class="colspace"><tr><th></th>';
				foreach ($must_show_columns as $value) {
					$sisu .= '<th>' . kriips($value) . '</th>';
				}
				$sisu .= '</tr>';
				$naitaKuni = $naitaAlates + $riduLehel;
				for ($whereIn = []; $naitaAlates < $naitaKuni; $naitaAlates++) {
					if (isset($korduvateKontroll[$naitaAlates])) {
						$whereIn[] = $korduvateKontroll[$naitaAlates];
					}
				}
				$paring = m_select($xtabel, ['id' => $whereIn]);
				while ($rida = m_a($paring)) {
					$resource_name = $xtabel . '-' . $rida['id'];
					$sisu .= '<tr><td style="white-space: nowrap;">';
					if ($minu_grupi_oigused > 1) {
						$sisu .= '
						<a href="muuda.php?t=' . $xtabel . '&id=' . $rida['id'] . '"><img src="kujundus/pildid/muuda.png" title="'. $lang['muuda'] . '" alt="' . $lang['muuda'] . '" /></a> 
						<a href="kustuta.php?t=' . $xtabel . '&id=' . $rida['id'] . '"><img src="kujundus/pildid/kustuta.png" title="' . $lang['kustuta'] . '" alt="' . $lang['kustuta'] . '" /></a> ';
					}
					$sisu .= '
						<span id="' . $resource_name . '">';
					if (in_array($resource_name, $lemmikud)) {
						$sisu .= '<img src="kujundus/pildid/ok.png" alt="' . $lang['sinu_lemmik'] . '" title="' . $lang['sinu_lemmik'] . '" />';
					} else {
						$sisu .= '<img src="kujundus/pildid/lemmik.png" title="' . $lang['lisa_lemmikutesse'] . '" alt="' . $lang['lisa_lemmikutesse'] . '" onclick="lisa_lemmik' . $jsLemmik . '(\'' . $xtabel . '\', \'' . $rida['id'] . '\'); this.style.display=\'none\'; " />';
					}
					$sisu .= '</span>';
					if (isset($rida['digitaalselt_olemas']) && $rida['digitaalselt_olemas'] == 1) {
						$sisu .= ' <img src="kujundus/pildid/digi.png" alt="' . kriips('digitaalselt_olemas') . '" title="' . kriips('digitaalselt_olemas') . '" />';
					} elseif (isset($rida['fyysiliselt_olemas']) && $rida['fyysiliselt_olemas'] == 1) {
						$sisu .= ' <img src="kujundus/pildid/fyysiline.png" alt="' . kriips('fyysiliselt_olemas') . '" title="' . kriips('fyysiliselt_olemas') . '" />';
					} elseif (isset($rida['ressursi_tyyp']) && ($rida['ressursi_tyyp'] == 1 && $rida['fyysiliselt_olemas'] == 2 || $rida['ressursi_tyyp'] == 2 && $rida['digitaalselt_olemas'] == 2)) {
						$sisu .= ' <img src="kujundus/pildid/kadunud.png" alt="' . kriips('pole_saadaval') . '" title="' . kriips('pole_saadaval') . '" />';
					}
					$sisu .= '</td>';
					foreach ($must_show_columns as $value) {
						$sisu .= '<td><a href="naita.php?t=' . $xtabel . '&id=' . $rida['id'] . '">';
						if ($value == 'kestus') {
							$sisu .= ' ' . pretty_length($rida[$value]);
						} elseif ($value == 'materjal') {
							if ($query = m_select($rida['materjal'], ['id' => $rida['materjal_id']])) {
								$sisu .= kriips($rida['materjal']);
								$row = m_a($query);
								$must_show_columns2 = get_must_show_columns($rida['materjal']);
								foreach ($must_show_columns2 as $value2) {
									if ($value2 == 'kestus') {
										$sisu .= ' ' . pretty_length($row[$value2]);
									} else {
										$sisu .= ' ' . $row[$value2];
									}
								}
							}
						} elseif ($value == 'roll') {
							if ($query = m_select($value, ['materjal' => $xtabel, 'materjal_id' => $rida['id']])) {
								$must_show_columns2 = get_must_show_columns($value);
								$must_show_columns3 = get_must_show_columns('osaleja');
								while ($row = m_a($query)) {
									$osalejaParing = m_a(m_q('SELECT * FROM osaleja WHERE id = :id', ['id' => $row['osaleja']]));
									foreach ($must_show_columns2 as $value2) {
										if ($value2 == 'vanus' && ($row['vanus'] || $row['vanus'] = vanus($osalejaParing['synniaasta'], $rida[$orig_aeg[$xtabel]]))) {
											$sisu .= ' (' . $row[$value2] . ')';
										} elseif ($value2 == 'materjal' || $value2 == 'materjal_id') {
										} elseif ($value2 == 'osaleja') {
											foreach ($must_show_columns3 as $value3) {
												$sisu .= ' ' . $osalejaParing[$value3];
											}
										} else {
											$sisu .= ' ' . $row[$value2];
										}
									}
									$sisu .= '<br />';
								}
							}
						} elseif (in_array($value, $nonSystemTables)) {
							if ($query = m_select($value, ['id' => $rida[$value]])) {
								$row = m_a($query);
								$must_show_columns2 = get_must_show_columns($value);
								foreach ($must_show_columns2 as $value2) {
									$sisu .= ' ' . $row[$value2];
								}
							}
						} else {
							$sisu .= ' ' . korrasta($rida[$value]);
						}
						$sisu .= '</a></td>';
					}
					$sisu .= '</tr>';
				}
				$sisu .= '</table>';
			}
			$teade = $lang['otsiti'] . ' ' . $count . ' ' . $lang['parameetri_jargi_leiti'] . ' ' . $naitaRidu . ' ' . $lang['rida'] . '.';
			if ($kestusKokku > 0) {
				$teade .= '<br />' . $lang['trakkide_kogupikkus_on'] . ' ' . pretty_length($kestusKokku);
			} elseif ($lehekylgiKokku > 0) {
				$teade .= '<br />' . $lang['lehekylgi_kokku_on'] . ' ' . $lehekylgiKokku;
			}
		}

		$xtabel1 = kriips($xtabel, 0);
		$pealkiri1 = $lang['otsi'] . ' ' . $xtabel1;
		$sisu .= <<<SISU
<form action="" method="GET">
<table width="100%">
<tr><td><a href="{$minuLemmikudLink}">{$lang['minu_lemmikud']}</a></td></tr>
<tr><th>{$lang['valja_nimi']}</th><th>{$lang['tapne_vaste']}</th><th>{$lang['otsitavad_vaartused']}</th></tr>
SISU;
		function genereeriOtsimiseVorm($veeruNimed) {
			global $sisu;
			global $nonSystemTables;
			global $excludes;
			global $r;
			global $lang;
			global $xtabel;
			global $publicTables;
			foreach ($veeruNimed as $columns) { //genereeritakse html vorm
				if (!in_array($columns['Field'], $excludes)) {
					$n = $r[$columns['Field']]; //eemalda whitespace'd ja muuda ära erisümbolid
					$leiatyyp = explode('(', $columns['Type']);
					$tyyp = $leiatyyp[0];
					$n3 = $r[$columns['Field'] . '_equals'] ?? null;
					$sisu .= '<tr><td>' . kriips($columns['Field']) . '</td><td>';
					if (in_array($columns['Field'], $nonSystemTables)) {
						$must_show_columns = get_must_show_columns($columns['Field']);
						$kysi_teisest_tabelist = m_select($columns['Field'], null, null, ' ORDER BY `' . $must_show_columns[0] . '`');
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
							<select name="' . $columns['Field'] . '" style="width: 300px;" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked = true; }">
								<option value=""></option>';
						while ($rida = m_a($kysi_teisest_tabelist)) {
							if ($columns['Field'] != 'sari' || $rida['materjali_tyyp'] == $xtabel || $rida['materjali_tyyp'] == $xtabel . '_trakk' || $rida['materjali_tyyp'] . 'album' == $xtabel) {
								if ($n == $rida['id']) {
									$selected = 'selected';
								} else {
									$selected = '';
								}
								$sisu .= '
									<option value="' . $rida['id'] . '" ' . $selected . '>';
								foreach ($must_show_columns as $value) {
									$sisu .= $rida[$value] . ' ';
								}
								$sisu .= '</option>';
							}
						}
						$sisu .= '</select>';
					} elseif ($columns['Field'] == 'ressursi_tyyp') {
						$n22 = 'selected_ressursi_tyyp' . $n;
						$$n22 = 'selected';
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
						<select name="ressursi_tyyp" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked=true; }">
							<option value=""></option>
							<option value="1" ' . ($selected_ressursi_tyyp1 ?? '') . '>' . $lang['fyysiline'] . '</option>
							<option value="2" ' . ($selected_ressursi_tyyp2 ?? '') . '>' . $lang['digitaalne'] . '</option>
						</select>';
					} elseif ($columns['Field'] == 'fyysiliselt_olemas') {
						$n22 = 'selected_fyysiliselt_olemas' . $n;
						$$n22 = 'selected';
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
						<select name="fyysiliselt_olemas" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked = true; }">
							<option value=""></option>
							<option value="1" ' . ($selected_fyysiliselt_olemas1 ?? '') . '>' . $lang['olemas'] . '</option>
							<option value="2" ' . ($selected_fyysiliselt_olemas2 ?? '') . '>' . $lang['kadunud'] . '</option>
							<option value="3" ' . ($selected_fyysiliselt_olemas3 ?? '') . '>' . $lang['laenatud'] . '</option>
						</select>';
					} elseif ($columns['Field'] == 'digitaalselt_olemas') {
						$n22 = 'selected_digitaalselt_olemas' . $n;
						$$n22 = 'selected';
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
						<select name="digitaalselt_olemas" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked = true; }">
							<option value=""></option>
							<option value="1" ' . ($selected_digitaalselt_olemas1 ?? '') . '>' . $lang['jah'] . '</option>
							<option value="2" ' . ($selected_digitaalselt_olemas2 ?? '') . '>' . $lang['ei'] . '</option>
						</select>';
					} elseif ($columns['Field'] == 'kestus') {
						$n22 = $columns['Field'] . '2';
						$n2 = $r[$n22];
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><input type="text" name="' . $columns['Field'] . '" value="' . $n . '"> kuni <input type="text" name="' . $n22 . '" value="' . $n2 . '"> ' . $lang['sekundid'];
					} elseif ($columns['Field'] == 'oigused') {
						$n22 = 'selected_oigused' . $n;
						$$n22 = 'selected';
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
							<select name="' . $columns['Field'] . '" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked = true; }">
								<option value=""></option>
								<option value="avalik" ' . ($selected_oigusedavalik ?? '') . '>' . $lang['avalik'] . '</option>
								<option value="mitteavalik" ' . ($selected_oigusedmitteavalik ?? '') . '>' . $lang['mitteavalik'] . '</option>
							</select>';
					} elseif ($tyyp == 'text') {
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><textarea name="' . $columns['Field'] . '" cols="25" rows="3">' . $n . '</textarea>';
					} elseif ($tyyp == 'int') {
						$n22 = $columns['Field'] . '2';
						$n2 = $r[$n22];
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><input type="text" name="' . $columns['Field'] . '" value="' . $n . '"> ' . $lang['kuni'] . ' <input type="text" name="' . $n22 . '" value="' . $n2 . '">' . pretty_fieldtype($tyyp);
					} elseif ($tyyp == 'date') {
						$n22 = $columns['Field'] . '2';
						$n2 = $r[$n22];
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><input type="text" name="' . $columns['Field'] . '" value="' . $n . '"> ' . $lang['kuni'] . ' <input type="text" name="' . $n22 . '" value="' . $n2 . '">' . pretty_fieldtype($tyyp);
					} elseif (strpos($columns['Field'], 'aeg') !== false || strpos($columns['Field'], 'aasta') !== false) {
						$n22 = $columns['Field'] . '2';
						$n2 = $r[$n22];
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><input type="text" name="' . $columns['Field'] . '" value="' . $n . '"> ' . $lang['kuni'] . ' <input type="text" name="' . $n22 . '" value="' . $n2 . '"> ' . $lang['aasta'] . ' ' . $lang['AAAA'];
					} elseif ($columns['Field'] == 'materjal') {
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td>
							<select name="' . $columns['Field'] . '" onchange="if (!this.value) { this.form.' . $columns['Field'] . '_equals.checked = false; } else { this.form.' . $columns['Field'] . '_equals.checked=true; }">
								<option value=""></option>';
							foreach ($publicTables as $value) {
								if ($value == $n) {
									$selected = ' selected';
								} else {
									$selected = '';
								}
								$sisu .= '
									<option value="' . $value . '"' . $selected . '>' . kriips($value) . '</option>';
							}
							$sisu .= '</select>';
					} else {
						$sisu .= '<input type="checkbox" name="' . $columns['Field'] . '_equals" value="checked" ' . $n3 . ' /></td><td><input type="text" name="' . $columns['Field'] . '" value="' . $n . '">' . pretty_fieldtype($tyyp);
					}
					$sisu .= '</td></tr>';
				}
			}
		}
		genereeriOtsimiseVorm($columns1);
		if (in_array($xtabel, $publicTables)) {
			$sisu .= '<tr><td colspan="3"><br /></td></tr>';
			$sisu .= '<tr><td colspan="3"><b>' . $lang['andmed_osaleja_kohta'] . '</b></td></tr>';
			$columns2[] = ['Field' => 'rolli_nimetus', 'Type' => 'varchar(225)'];
			genereeriOtsimiseVorm($columns2);
		}
		$sisu .= <<<SISU
<tr><td></td><td></td><td><input type="submit" name="{$xtabel}" value="{$lang['otsi']}"></td></tr>
</table>
<input type="hidden" name="tabel" value="{$xtabel}">
</form>
SISU;
	} //tabel oli olemas
} else { //tahetakse näha otsimise avalehte
	$pealkiri1 = $lang['otsi'];
	if (isset($_SESSION['otsing'])) {
		$viimaneOtsing = '<tr><td><a href="' . $_SESSION['otsing'] . '">' . $lang['viimane_otsing'] . '</a></td></tr>';
	} else {
		$viimaneOtsing = '';
	}
	$sisu = <<<SISU
<form action="" method="GET">
	<table width="100%">
	{$viimaneOtsing}
	<tr><td><a href="abi.php?t=otsi" target="_blank">{$lang['otsinguabi']}</a></td></tr>
	<tr><td><a href="{$minuLemmikudLink}">{$lang['minu_lemmikud']}</a></td></tr>
	<tr><td>
	{$lang['otsi_andmebaasist']}<br />
	{$lang['tapne_vaste']} <input type="checkbox" name="equals" value="jah" /><br />
	<input type="text" name="mida"> <input type="submit" name="otsi_andmebaasist" value="{$lang['otsi']}">
	</td></tr>
	<tr><td>{$lang['otsi_paremalt']}</td></tr>
	</table>
</form>
SISU;
}
$gentime = gentime();
include('kujundus.php');
