<?php
if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = 'index.html';
}
if (file_exists('../usage/' . $page)) {
	echo str_replace(['IMG SRC="', 'A HREF="'], ['IMG SRC="stat_img.php?fail=', 'A HREF="stat.php?page='], implode("\n", file('../usage/' . $page)));
}
