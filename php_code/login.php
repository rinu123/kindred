﻿<?php
include('base.php');
if (sisse_logitud()) {
	header('Location: index.php');
	die();
}
if (isset($_POST['login']) && (!isset($_SESSION['login']) || $_SESSION['login'] < 5)) {
	$paring = m_select('kasutaja', [
		'email' => $_POST['email']
	]);
	$kasutaja = m_a($paring);
	if (!$kasutaja || !password_verify($_POST['parool'], $kasutaja['parool'])) {
		$viga = $lang['vale_email_voi_parool'];
		if (isset($_SESSION['login'])) {
			$_SESSION['login']++;
		} else {
			$_SESSION['login'] = 1;
		}
	} else {
		if ($kasutaja['aegub'] != '0000-00-00' && strtotime($kasutaja['aegub']) < time()) {
			$viga = $lang['kasutaja_on_aegunud'];
		} else {
			$_SESSION['kindred'] = $kasutaja;
			$_SESSION['login'] = 0;
			header('Location: index.php');
			die();
		}
	}
} elseif (isset($_POST['login'], $_SESSION['login'])) {
	$viga = $lang['vale_email_voi_parool'];
}

$pealkiri1 = $lang['logi_sisse'];
$sisu = <<<SISU
<form action="" method="POST">
<table width="100%">
<tr><td width="100">{$lang['email']}</td><td><input type="text" name="email"></td></tr>
<tr><td>{$lang['parool']}</td><td><input type="password" name="parool"></td></tr>
<tr><td><a href="uusparool.php">{$lang['unustasin_parooli']}</a></td><td><input type="submit" name="login" value="{$lang['logi_sisse']}"></td></tr>
</table>
</form>
SISU;
include('kujundus.php');
