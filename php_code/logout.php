﻿<?php
session_start();
$lang = ($_SESSION['lang'] ?? null);
session_destroy();
if ($lang != 'est') {
	session_start();
	$_SESSION['lang'] = $lang;
}
header('Location: index.php');
