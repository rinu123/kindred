<?php header('Content-Type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$lang['lehe_pealkiri']?></title>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Rauno Moisto, Hannes Tamme, Mait Sarv" />
	<link rel="stylesheet" type="text/css" title="Brown" href="kujundus/style1.css" />
	<?php
	if (isset($injectCssFiles)) {
		foreach ($injectCssFiles as $cssFile) {
			echo $cssFile;
		}
	}
	?>
</head>
<?php
if (isset($_GET['t'], $_GET['u'], $_GET['id'], $_GET['ref'])) {
  if (isset($_GET['typ']) && $_GET['typ'] == 'alam') {
    echo '<body onload="lisa_vorm(\'' . $_GET['t'] . '\', \''.  $_GET['u'] . '\', 0, \'' . $_GET['id'] . '\');">';
  } else {
    if ($_GET['u'] == 'osaleja') {
      echo '<body onload="lisa_vorm(\'' . $_GET['t'] . '\', \''.  $_GET['u'] . '\', 0, \'' . $_GET['id'] . '\');">';
    } else {
      echo '<body onload="lisa_vorm(\'' . $_GET['t'] . '\', 0);">';
    }
  }
} else {
  echo '<body>';
}
?>
<div id="header">
  <div id="header_inner" class="fixed">
  <div id="sel_lang">
  <?php
    $_CONSTANTS = get_defined_constants();
    for ($i = 1; isset($_CONSTANTS['UI_LANG_' . $i]); $i++) {
      $ui_lang = explode(',', $_CONSTANTS['UI_LANG_' . $i]);
      echo '<a href="index.php?setlan=' . $ui_lang[0] . '"><img src="kujundus/pildid/' .$ui_lang[0] . '.png" alt="' . $ui_lang[1] . '" title="' . $ui_lang[1] . '" /></a>';
    }
  ?>
  </div>
  <div id="logo">
    <h1><span><?=$lang['lehe_pealkiri']?></span></h1>
  </div>
  <div id="menu">
    <ul>
      <?php
        $failinimi = array_reverse(explode('/', $_SERVER['PHP_SELF']));
        $failinimi = rtrim($failinimi[0], '.php') . '_active';
        $$failinimi = ' class="active"';
      ?>
        <li><a href="index.php"<?=($index_active ?? '')?>><?=$lang['avaleht']?></a></li>
        <li><a href="otsi.php"<?=($otsi_active ?? '')?>><?=$lang['otsi']?></a></li>
      <?php if ($sess = sisse_logitud()) {
        if (!$grupi_oigused = grupi_oigused($sess)) {
          session_destroy();
          header('Location: index.php');
          die();
        }
        ?>
        <?php if($grupi_oigused >= 2) { ?>
          <li><a href="lisa.php"<?=($lisa_active ?? '')?>><?=$lang['lisa']?></a></li>
        <?php } ?>
        <?php if($grupi_oigused == 3) { ?>
          <li><a href="haldus.php"<?=($haldus_active ?? '')?>><?=$lang['haldus']?></a></li>
        <?php } ?>
        <li><a href="seaded.php"<?=($seaded_active ?? '')?>><?=$lang['seaded']?></a></li>
        <li><a href="logout.php"><?=$lang['logi_valja']?></a></li>
      <?php }
      else { ?>
        <li><a href="login.php"<?=($login_active ?? '')?>><?=$lang['logi_sisse']?></a></li>
      <?php } ?>
      </ul>
    </div>
  </div>
</div>
<div id="main">
<?php
$class1 = 'primaryContent_2columns';
$class2 = 'columnA_2columns';
if (isset($naita_active)) {
    $class1 = 'primaryContent_columnless';
    $class2 = 'columnA_columnless';
}
?>
  <div id="main_inner" class="fixed">
    <div id="<?=$class1?>">
      <div id="<?=$class2?>">
        <h3><?=($pealkiri1 ?? 'Pealkiri')?></h3>
        <?php
          if (isset($_SESSION['viga'])) {
            $viga = $_SESSION['viga'];
            unset($_SESSION['viga']);
          }
          if (isset($_SESSION['teade'])) {
            $teade = $_SESSION['teade'];
            unset($_SESSION['teade']);
          }
          if (isset($viga)) {
            echo '<div class="center"><img src="kujundus/pildid/viga.png"><br />' . $viga . '</div>';
          } elseif (isset($teade)) {
            echo '<div class="center"><img src="kujundus/pildid/teade.png"><br />' . $teade . '</div>';
          }
          if (isset($otsiLehed)) {
            echo $otsiLehed;
          }
        ?>
        <span id="teade"></span>
        <?=$sisu?>
        <br class="clear" />
      </div>
    </div>
    <?php if (!isset($naita_active)) { ?>
      <div id="secondaryContent_2columns">
        <div id="columnC_2columns">
        <?php
          if (isset($lisa_active)) { ?>
            <h4><span><?=$lang['lisa']?></span></h4>
              <ul class="links">
                <?php
                  if (sisse_logitud() && $sess['grupp'] > 1) {
                    foreach (get_non_system_tables() as $table) {
                      echo '<li> <a href="lisa.php?tabel=' . $table . "\" onclick=\"lisa_vorm('" . $table . "', 0); return false;\">" . $lang['lisa'] . ' ' . kriips($table, 0) . '</a></li>';
                    }
                  }
                ?>
              </ul>
        <?php
          } elseif (isset($otsi_active)) {
        ?>
          <h4><span><?=$lang['otsi']?></span></h4>
          <ul class="links">
          <?php
            if (sisse_logitud() && $sess['grupp'] > 1) {
              foreach (get_searchable_tables() as $table) {
                echo '<li> <a href="otsi.php?tabel=' . $table . '">' . $lang['otsi'] . ' ' . kriips($table, 0) . '</a></li>';
              }
            } else {
              $public_tables = get_public_tables();
                foreach ($public_tables as $table) {
                  echo '<li> <a href="otsi.php?tabel=' . $table . '">' . $lang['otsi'] . ' ' . kriips($table, 0) . '</a></li>';
                }
            }
          ?>
        </ul>
      <?php
        } elseif (isset($haldus_active)) {
      ?>
        <h4><span><?=$lang['haldus']?></span></h4>
        <ul class="links">
          <li> <a href="haldus.php?baasi_haldus=1">Vaata tabeleid</a></li>
          <li> <a href="haldus.php?baasi_haldus=2">Lisa tabel</a></li>
          <li> <a href="haldus.php?baasi_haldus=3">Eemalda tabel</a></li>
          <li> <a href="haldus.php?baasi_haldus=4">Modifitseeri tabelit</a></li>
          <li> <a href="haldus.php?baasi_haldus=6">Muuda ridu</a></li>

          <br />

          <li> <a href="haldus.php?kasutaja_haldus=1">Vaata kasutajaid</a></li>
          <li> <a href="haldus.php?kasutaja_haldus=2">Lisa kasutaja</a></li>

          <br />

          <li> <a href="haldus.php?grupi_haldus=1">Vaata gruppe</a></li>
          <li> <a href="haldus.php?grupi_haldus=2">Lisa grupp</a></li>

          <br />

          <li> <a href="haldus.php?sarnased_osalejad=1">Sarnased osalejad</a></li>
          <li> <a href="haldus.php?show_full_content=logi">Vaata logi</a></li>
          <li> <a href="stat.php" target="_blank">Statistika</a></li>
          <li> <a href="haldus.php?keelte_haldus=1">Keelte haldus</a></li>
          <li> <a href="abi.php?t=haldus" target="_blank">Haldusjuhend</a></li>
          <li> <a href="haldus.php?baasi_haldus=5">Erilised väärtused</a></li>
        </ul>
      <?php
        }
      ?>
      </div>
    </div>
    <?php } ?>
    <br class="clear" />
  </div>
</div>
<div id="footer" class="fixed"> 2011 - 2019
<?php
  if (isset($gentime)) {
    echo '<br />' . $lang['genereeritud'] . ' ' . number_format($gentime, 4, ',', '') . ' ' . $lang['sekundiga'];
  }
?>
</div>
<?php
  if (isset($lisa_active) || isset($muuda_active)) {
    echo '
      <script type="text/javascript" src="jquery.min.js"></script>
      <script type="text/javascript" src="lisa_vorm.js"></script>
      <script type="text/javascript" src="lisa.js"></script>
    ';
  } elseif (isset($otsi_active) || isset($naita_active)) {
    echo '
      <script type="text/javascript" src="jquery.min.js"></script>
      <script type="text/javascript" src="lemmikud.js"></script>
    ';
  }
?>
</body>
</html>
