<?php

class aSql extends PDO {
	/** @var string */
	public $selectedDb;

	/** @var string */
	public $lastQuery;

	/** @var string */
	public $lastError;

	/**
	 * @param string $db
	 * @return boolean
	 */
	public function e_select_db($db) {
		if ($this->selectedDb == $db) {
			return true;
		}
		$this->selectedDb = $db;
		return $this->exec('USE ' . $db);
	}

	/**
	 * @param string $query
	 * @return \PDOStatement
	 */
	public function e_prepare($query) {
		$this->lastQuery = $query;
		return $this->prepare($query);
	}
}

/**
 * @param boolean $force
 * @return aSql
 */
function m_c($force = false) {
	static $eSql = null;
	if ($eSql === null || $force) {
		try {
			$eSql = new aSql('mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DB . ';charset=utf8', MYSQL_USER, MYSQL_PASSWORD);
		} catch (PDOException $e) {
			die('Error m_c');
		}
		$eSql->exec('SET CHARACTER SET UTF8, collation_connection=utf8_unicode_ci');
		$eSql->exec("SET NAMES 'utf8mb4'");
	}
	return $eSql;
}

/**
 * @param \PDOStatement $result
 * @return array|boolean
 */
function m_a($result) {
	if ($result) {
		return $result->fetch(PDO::FETCH_ASSOC);
	}
	return false;
}

/**
 * @param \PDOStatement $result
 * @return integer
 */
function m_r($result) {
	if ($result) {
		return $result->rowCount();
	}
	return 0;
}

/**
 * @param string $query
 * @return \PDOStatement|boolean
 */
function m_prepare($query) {
	return m_c()->e_prepare($query);
}

/**
 * @param \PDOStatement $query
 * @param array $vars
 * @return \PDOStatement|boolean
 */
function m_exec($query, array $vars = null) {
	if (!$query->execute($vars)) {
		$err = $query->errorInfo();
		$eSql = m_c();
		$eSql->lastError = $err[2];
		trigger_error($err[2] . ' query: ' . $eSql->lastQuery);
		return false;
	}
	return $query;
}

/**
 * @param string $query
 * @param array $vars
 * @return \PDOStatement|boolean
 */
function m_q($query, $vars = null) {
	return m_exec(m_prepare($query), $vars);
}

/**
 * @param string $query
 * @param array $vars
 * @return array|boolean
 */
function m_f($query, $vars = null) {
	return m_a(m_q($query, $vars));
}

/**
 * @return integer
 */
function m_id() {
	$eSql = m_c();
	return $eSql->lastInsertId();
}

/**
 * @return string
 */
function m_err() {
	return m_c()->lastError;
}

function columns_exist($table, array $columns) {
	$allColumns = get_all_columns($table);
	foreach ($columns as $column) {
		if (!isset($allColumns[$column])) {
			return false;
		}
	}
	return true;
}

/**
 * @param string $table
 * @param array $data
 * @param array $injections
 * @return \PDOStatement|bool
 */
function m_insert($table, array $data, array $injections = null) {
	if (!columns_exist($table, array_keys($data))) {
		return false;
	}
	if ($injections !== null && !columns_exist($table, array_keys($injections))) {
		return false;
	}

	$fields = [];
	$values = [];

	foreach ($data as $field => $value) {
		$fields[] = '`' . $field . '`';
		$values[] = '?';
	}

	if (is_array($injections)) {
		foreach ($injections as $field => $value) {
			$fields[] = '`' . $field . '`';
			$values[] = $value;
		}
	}

	$query = 'INSERT INTO `' . $table . '` (' . implode(',', $fields) . ') VALUES (' . implode(',', $values) . ')';
	return m_q($query, array_values($data));
}

/**
 * @param string $table
 * @param array $data
 * @param array $where
 * @return bool|\PDOStatement
 */
function m_update($table, array $data, array $where) {
	if (!columns_exist($table, array_keys($data))) {
		return false;
	}
	if (!columns_exist($table, array_keys($where))) {
		return false;
	}

	$fields = [];
	$values = [];
	foreach ($data as $field => $value) {
		if (is_array($value)) {
			$fields[] = '`' . $field . '` = `' . $field . '` ' . $value[0] . ' ?';
			$values[] = $value[1];
		} else {
			$fields[] = '`' . $field . '` = ?';
			$values[] = $value;
		}
	}
	$query = 'UPDATE `' . $table . '` SET ' . implode(',', $fields);

	if (!empty($where)) {
		$whereFields = [];
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				$whereFields[] = '`' . $field . '` IN (' . implode(',', array_fill(0, count($value), '?')) . ')';
				$values = array_merge($values, $value);
			} else {
				$whereFields[] = '`' . $field . '` = ?';
				$values[] = $value;
			}
		}
		$query .= ' WHERE ' . implode(' AND ', $whereFields);
	}

	return m_q($query, $values);
}

/**
 * @param string $table
 * @param array $where
 * @return bool|\PDOStatement
 */
function m_delete($table, array $where) {
	if (!columns_exist($table, array_keys($where))) {
		return false;
	}

	$values = [];
	$query = 'DELETE FROM `' . $table . '`';

	if (!empty($where)) {
		$whereFields = [];
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				$whereFields[] = '`' . $field . '` IN (' . implode(',', array_fill(0, count($value), '?')) . ')';
				$values = array_merge($values, $value);
			} else {
				$whereFields[] = '`' . $field . '` = ?';
				$values[] = $value;
			}
		}
		$query .= ' WHERE ' . implode(' AND ', $whereFields);
	}

	return m_q($query, $values);
}

/**
 * @param string $table
 * @param array $where
 * @param array $data
 * @param string $injection
 * @return bool|\PDOStatement
 */
function m_select($table, array $where = null, array $data = null, $injection = null) {
	if ($data !== null && !columns_exist($table, $data)) {
		return false;
	}
	if ($where !== null && !columns_exist($table, array_keys($where))) {
		return false;
	}

	$fields = ['*'];
	if ($data !== null) {
		$fields = [];
		foreach ($data as $field) {
			$fields[] = '`' . $field . '`';
		}
	}
	$query = 'SELECT ' . implode(',', $fields) . ' FROM `' . $table . '`';

	if (!empty($where)) {
		$whereFields = [];
		$values = [];
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				$whereFields[] = '`' . $field . '` IN (' . implode(',', array_fill(0, count($value), '?')) . ')';
				$values = array_merge($values, $value);
			} else {
				$whereFields[] = '`' . $field . '` = ?';
				$values[] = $value;
			}
		}
		$query .= ' WHERE ' . implode(' AND ', $whereFields);
		$where = $values;
	}

	if ($injection !== null) {
		$query .= $injection;
	}

	return m_q($query, $where);
}

/**
 * For use in custom queries with :label syntax
 * Supports array values
 *
 * @param string $query
 * @param array $vars
 * @return bool|PDOStatement
 */
function m_qp(string $query, array $vars = []) {
	if (empty($vars)) {
		return m_q($query);
	}

	$valuesTmp = [];
	$values = [];

	foreach ($vars as $key => $value) {
		$key = ':' . ltrim($key, ':');
		$start = 0;
		while (($pos = strpos($query, $key, $start)) !== false) {
			$valuesTmp[$pos] = [
				'key' => $key,
				'value' => $value
			];
			$start = $pos + strlen($key);
		}
		if ($start === 0) {
			trigger_error('unused key ' . $key, E_USER_WARNING);
		}
	}

	krsort($valuesTmp);
	foreach ($valuesTmp as $pos => $kv) {
		if (is_array($kv['value'])) {
			if (is_string(key($kv['value']))) {
				$tmp1 = [];
				$tmp2 = [];
				foreach ($kv['value'] as $key => $value) {
					if (!is_string($key)) {
						continue;
					}
					$fieldName = $key;
					if (strpos($fieldName, '.') === false) {
						$fieldName = '`' . $fieldName . '`';
					}
					if (is_array($value)) {
						$tmp1[] = $fieldName . ' IN (' . implode(',', array_fill(0, count($value), '?')) . ')';
						$tmp2 = array_merge($tmp2, $value);
					} else {
						$tmp1[] = $fieldName . '=?';
						$tmp2[] = $value;
					}
				}
				$query = substr_replace($query, implode(' AND ', $tmp1), $pos, strlen($kv['key']));
				$values = array_merge($values, array_reverse($tmp2));
			} else {
				$query = substr_replace($query, implode(',', array_fill(0, count($kv['value']), '?')), $pos, strlen($kv['key']));
				$values = array_merge($values, array_reverse($kv['value']));
			}
		} else {
			$query = substr_replace($query, '?', $pos, strlen($kv['key']));
			$values[] = $kv['value'];
		}
	}

	return m_q($query, array_reverse($values));
}

function get_all_tables() {
	static $allTables = [];
	if (empty($allTables)) {
		$query = m_q('SELECT table_name, table_comment FROM information_schema.tables WHERE table_schema = \'' . MYSQL_DB . '\'');
		while ($row = m_a($query)) {
			$allTables[$row['table_name']] = [
				'comment' => $row['table_comment'] ?? ''
			];
		}
	}
	return $allTables;
}

function get_system_tables() {
	$systemTables = [];
	foreach (get_all_tables() as $name => $conf) {
		if ($conf['comment'] == 'sys') {
			$systemTables[] = $name;
		}
	}
	return $systemTables;
}

function get_non_system_tables() {
	$nonSystemTables = [];
	foreach (get_all_tables() as $name => $conf) {
		if ($conf['comment'] != 'sys') {
			$nonSystemTables[] = $name;
		}
	}
	return $nonSystemTables;
}

function get_searchable_tables() {
	$searchableTables = [];
	foreach (get_all_tables() as $name => $conf) {
		if ($name != 'materjal_keel' && $conf['comment'] != 'sys' && $conf['comment'] != 'info') {
			$searchableTables[] = $name;
		}
	}
	return $searchableTables;
}

function get_info_tables() {
	$infoTables = [];
	foreach (get_all_tables() as $name => $conf) {
		if ($conf['comment'] == 'info') {
			$infoTables[] = $name;
		}
	}
	return $infoTables;
}

function get_public_tables() {
	$infoTables = [];
	foreach (get_all_tables() as $name => $conf) {
		if ($conf['comment'] == 'public') {
			$infoTables[] = $name;
		}
	}
	return $infoTables;
}

function get_table_names() {
	return array_keys(get_all_tables());
}

function get_all_columns($table) {
	static $cols = [];
	if (isset($cols[$table])) {
		return $cols[$table];
	}
	$allTables = get_all_tables();
	if (isset($allTables[$table])) {
		$query = m_q('SHOW FULL COLUMNS FROM ' . $table);
		while ($row = m_a($query)) {
			$cols[$table][$row['Field']] = $row;
		}
	}
	return $cols[$table] ?? [];
}

function get_columns($table, $excludes = []) {
	$cols = [];
	foreach (get_all_columns($table) as $name => $conf) {
		if (!in_array($name, $excludes)) {
			$cols[] = $name;
		}
	}
	return $cols;
}

function get_full_columns($table, $excludes = []) {
	$cols = [];
	foreach (get_all_columns($table) as $name => $conf) {
		if (!in_array($name, $excludes)) {
			$cols[] = $conf;
		}
	}
	return $cols;
}

function table_exists($name) {
	return isset(get_all_tables()[$name]);
}

function change_column_order($table, $column, $after) {
	$createColumn = show_create_column($table, $column);
	if ($createColumn && isset(get_all_columns($table)[$after])) {
		m_q('ALTER TABLE ' . $table . ' MODIFY ' . $createColumn . ' AFTER ' . $after);
	}
}

function get_must_have_columns($table) {
	$cols = [];
	$excludes = excludes(0);
	foreach (get_all_columns($table) as $name => $conf) {
		if (!in_array($name, $excludes) && $conf['Null'] == 'NO') {
			$cols[] = $name;
		}
	}
	return $cols;
}

function get_must_show_columns($table) {
	$cols = [];
	foreach (get_all_columns($table) as $name => $conf) {
		if (strpos($conf['Comment'], 'show') !== false) {
			$cols[] = $name;
		}
	}
	return $cols;
}

function get_must_show_wide_columns($table) {
	$cols = [];
	foreach (get_all_columns($table) as $name => $conf) {
		if (strpos($conf['Comment'], 'wide') !== false) {
			$cols[] = $name;
		}
	}
	return $cols;
}

function show_create_column($table, $column) {
	if (!isset(get_all_columns($table)[$column])) {
		return false;
	}
	$query = m_q('SHOW CREATE TABLE ' . $table)->fetch(PDO::FETCH_NUM);
	$lines = explode(",\n", $query[1]);
	foreach ($lines as $line) {
		if (strpos($line, '`' . $column . '`') !== false) {
			return $line;
		}
	}
	return false;
}

function get_roles_columns($table) { //tagastab array(digitaliseerija_väli, rolli_väli)
	$cols = ['', ''];
	foreach (get_all_columns($table) as $name => $conf) {
		if (strpos($conf['Comment'], 'digitaliseerija') !== false) {
			$cols[0] = $name;
		}
		if (strpos($conf['Comment'], 'roll') !== false) {
			$cols[1] = $name;
		}
	}
	return $cols;
}

function excludes($avalik = 1) {
	if ($avalik == 1) {
		return ['id', 'andmebaasi_lisatud', 'lisaja', 'oigused'];
	}
	return ['id', 'andmebaasi_lisatud', 'lisaja'];
}
