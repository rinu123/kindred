﻿<?php
include('base.php');
if (sisse_logitud()) {
	header('Location: index.php');
	die();
}

if (isset($_POST['parool'])) {
	$email = $_POST['email'];
	$query = m_q('SELECT * FROM kasutaja WHERE email = :email', ['email' => $email]);
	if (!$query || !$row = m_a($query)) {
		$viga = $lang['kasutajat_ei_leitud'];
	} elseif (!isset($_SESSION) || ($_SESSION['pwt'] ?? 0) + 3600 > time()) {
		$viga = $lang['oled_hiljuti_parooli_tellinud'];
	} else {
		$newpass = '';
        for ($i = 0; $i < 10; $i++) {
            $newpass .= chr(rand(65,90));
        }
		$replace = ['{PAROOL}', '{EESNIMI}', '{PERENIMI}', '{EMAIL}', '{AEGUB}'];
		$with = [$newpass, $row['eesnimi'], $row['perenimi'], $row['email'], date_to_est($row['aegub'])];
		if (mymail($email, $lang['uus_parool_kirja_pealkiri'], str_replace($replace, $with, tekst('uusparool')))) {
			m_update('kasutaja', [
				'parool' => password_hash($newpass, PASSWORD_BCRYPT)
			], [
				'email' => $email
			]);
			$teade = $lang['parool_muudetud_saadetud'];
			$_SESSION['pwt'] = time();
		} else {
			$viga = $lang['email_ei_saadetud'];
		}
	}
}

$pealkiri1 = $lang['logi_sisse'];
$sisu = <<<SISU
<form action="" method="POST">
<table width="100%">
<tr><td width="100">{$lang['email']}</td><td><input type="text" name="email"></td></tr>
<tr><td></td><td><input type="submit" name="parool" value="{$lang['telli_uus_parool']}"></td></tr>
</table>
</form>
SISU;
include('kujundus.php');
