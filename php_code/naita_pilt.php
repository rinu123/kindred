﻿<?php
include('base.php');
$html1 = <<<HTML
<!DOCTYPE html>
<html>
<head>
  <title>{$lang['lehe_pealkiri']}</title>
  <meta charset="utf-8">
  <meta name="robots" content="noindex, nofollow" />
  <meta name="author" content="Rauno Moisto, Hannes Tamme, Mait Sarv" />
  <link rel="stylesheet" type="text/css" title="Brown" href="kujundus/style1.css" />
</head>
<body>
HTML;

$html2 = <<<HTML2
</body>
</html>
HTML2;

if (isset($_GET['materjal'], $_GET['materjal_id'], $_GET['sari'])) {
	include('resize.php');
	$_GET['sari'] = str_replace('..', '', $_GET['sari']);
	if ($_GET['materjal'] == 'foto' && isset($_GET['album'])) {
		$piltide_nimekiri2 = [];
		$fotoParing = m_q('SELECT * FROM fotoalbum WHERE arhiivinumber = :arhiivinumber', ['arhiivinumber' => $_GET['album']]);
		if (m_r($fotoParing)) {
			$fotoRida = m_a($fotoParing);
			if (sisse_logitud()) {
				$fotoParing2 = m_q('SELECT * FROM foto WHERE fotoalbum = :fotoalbum ORDER BY arhiivinumber', ['fotoalbum' => $fotoRida['id']]);
			} else {
				$fotoParing2 = m_q('SELECT * FROM foto WHERE oigused=\'avalik\' AND fotoalbum = :fotoalbum ORDER BY arhiivinumber', ['fotoalbum' => $fotoRida['id']]);
			}
			while ($fotoRida2 = m_a($fotoParing2)) {
				$piltide_nimekiri2[] = $fotoRida2['arhiivinumber'];
			}
		}
		$filedir = PHOTO_PATH . $_GET['sari'];
		if (is_dir($filedir)) {
			$handle = opendir($filedir);
			while ($file = readdir($handle)) {
				if (strpos($file, $_GET['album']) !== false) {
					$filedir = PHOTO_PATH . $_GET['sari'] . '/' . $file;
					break;
				}
			}
			closedir($handle);
		}
		if (is_dir($filedir)) {
			$piltide_nimekiri = [];
			$count = 0;
			$fullFilePath = null;
			foreach ($piltide_nimekiri2 as $baasiPilt) {
				$handle = opendir($filedir); //otsi üles albumi pildid
				while ($file = readdir($handle)) {
					if (strpos($file, '.') !== 0) {
						$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
						if ($ext == 'jpg' || $ext == 'jpeg') {
							if ($_GET['materjal_id'] != 1 && strpos($file, $_GET['materjal_id']) !== false) {
								$fullFilePath = $filedir . '/' . $file;
							}
							if (stripos($file, $baasiPilt) !== false) {
								$piltide_nimekiri[] = $filedir . '/' . $file . '#' . $baasiPilt;
								$count++;
								break;
							}
						}
					}
				}
				closedir($handle);
			}
			sort($piltide_nimekiri);
			if ($fullFilePath) {
				$_GET['p'] = array_search($fullFilePath . '#' . $_GET['materjal_id'], $piltide_nimekiri);
			}
			if (!isset($_GET['p'])) {
				$p = 0;
			} else {
				$p = $_GET['p'];
			}
			$tykid = array_reverse(explode('#', $piltide_nimekiri[$p]));
			$query = m_a(m_q('SELECT * FROM foto WHERE arhiivinumber = :arhiivinumber LIMIT 1', ['arhiivinumber' => $tykid[0]]));
			unset($tykid[0]);
			$piltide_nimekiri[$p] = implode('#', array_reverse($tykid));
			$originalFileName = basename($piltide_nimekiri[$p]);
			if (!file_exists(IMAGE_PATH . $originalFileName) || filemtime($piltide_nimekiri[$p]) > filemtime(IMAGE_PATH . $originalFileName)) {
				$image = new SimpleImage();
				$image->load($piltide_nimekiri[$p]);
				$height = IMAGE_HEIGHT_DEFAULT;
				$width = IMAGE_WIDTH_DEFAULT;
				if (isset($_GET['formaat'])) {
					$_CONSTANTS = get_defined_constants();
					if (isset($_CONSTANTS['IMAGE_WIDTH_' . $_GET['formaat']], $_CONSTANTS['IMAGE_HEIGHT_' . $_GET['formaat']])) {
						$height = $_CONSTANTS['IMAGE_HEIGHT_' . $_GET['formaat']];
						$width = $_CONSTANTS['IMAGE_WIDTH_' . $_GET['formaat']];
					}
				}
				$image->resizeToHeight($height);
				if ($image->getWidth() > $width) {
					$image->resizeToWidth($width);
				}
				$image->save(IMAGE_PATH . $originalFileName);
			}
			if (isset($_GET['pisi'])) {
				xsendfile(IMAGE_PATH . $originalFileName);
			}
			list($width, $height) = getimagesize(IMAGE_PATH . $originalFileName);
			if ($width < 400) {
				$width = 400;
			}
			echo $html1;
			echo "<div style='width: ".$width."px;'>";
			if ($count > 1) {
				echo pages('naita_pilt.php?materjal=foto&materjal_id=1&sari=' . $_GET['sari'] . '&album=' . $_GET['album'] . '&formaat=' . $_GET['formaat'] . '&p=%d', $p, $count, 1);
			}
			if (sisse_logitud()) {
				echo '&nbsp;&nbsp;<a href="get_resource.php?t=foto&amp;id=' . $query['id'] . '" target="_blank">' . $lang['taissuurus'] . '</a> ';
			}
			echo '<br />&nbsp;&nbsp;<a href="naita.php?t=foto&id=' . $query['id'] . '"><img src="get_resource.php?t=foto&amp;id=' . $query['id'] . '&amp;format=PISI" /></a><br />';
			echo '&nbsp;&nbsp;';
			foreach (get_must_show_columns('foto') as $col) {
				echo $query[$col] . ' ';
			}
			echo '</div>';
			echo $html2;
		}
	} elseif ($_GET['materjal'] == 'kasikiri') {
		$_GET['materjal_id'] = str_replace('..', '', $_GET['materjal_id']);
		$filedir = KASIKIRI_PATH . $_GET['sari'] . '/' . $_GET['materjal_id'];
		if (is_dir($filedir)) {
			$handle = opendir($filedir); //otsi üles kõik käsikirja pildid
			$piltide_nimekiri = [];
			while ($file = readdir($handle)) {
				if (strpos($file, '.') !== 0) {
					$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
					if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'pdf') {
						$piltide_nimekiri[] = $filedir . '/' . $file;
					}
				}
			}
			closedir($handle);
			$count = count($piltide_nimekiri);
			sort($piltide_nimekiri);
			if (!isset($_GET['p'])) {
				$p = 0;
			} else {
				$p = $_GET['p'];
			}
			$originalFileName = basename($piltide_nimekiri[$p]);
			$fileExt = strtolower(pathinfo($originalFileName, PATHINFO_EXTENSION));
			if ($fileExt == 'pdf') {
				xsendfile($piltide_nimekiri[$p], false);
			}
			if (!file_exists(IMAGE_PATH . $originalFileName) || filemtime($piltide_nimekiri[$p]) > filemtime(IMAGE_PATH . $originalFileName)) {
				$image = new SimpleImage();
				$image->load($piltide_nimekiri[$p]);
				$height = IMAGE_HEIGHT_DEFAULT;
				$width = IMAGE_WIDTH_DEFAULT;
				if (isset($_GET['formaat'])) {
					$_CONSTANTS = get_defined_constants();
					if (isset($_CONSTANTS['IMAGE_WIDTH_' . $_GET['formaat']], $_CONSTANTS['IMAGE_HEIGHT_' . $_GET['formaat']])) {
						$height = $_CONSTANTS['IMAGE_HEIGHT_' . $_GET['formaat']];
						$width = $_CONSTANTS['IMAGE_WIDTH_' . $_GET['formaat']];
					}
				}
				$image->resizeToHeight($height);
				if ($image->getWidth() > $width) {
					$image->resizeToWidth($width);
				}
				$image->save(IMAGE_PATH . $originalFileName);
			}
			list($width, $height) = getimagesize(IMAGE_PATH . $originalFileName);
			if ($width < 400) {
				$width = 400;
			}
			$query = m_a(m_q('SELECT * FROM kasikiri WHERE arhiivinumber = :arhiivinumber LIMIT 1', ['arhiivinumber' => $_GET['materjal_id']]));
			echo $html1;
			echo '<div style="width: ' . $width . 'px;">';
			if ($count > 1) {
				echo pages('naita_pilt.php?materjal=kasikiri&materjal_id=' . $_GET['materjal_id'] . '&sari=' . $_GET['sari'] . '&formaat=' . $_GET['formaat'] . '&p=%d', $p, $count, 1);
			}
			if (sisse_logitud()) {
				echo '&nbsp;&nbsp;<a href="get_resource.php?t=kasikiri&amp;id=' . $query['id'] . '&amp;file=' . $originalFileName . '" target="_blank">' . $lang['taissuurus'] . '</a> ';
			}
			echo '<br /><img src="get_resource.php?t=kasikiri&amp;id=' . $query['id'] . '&amp;file=' . $originalFileName . '&amp;format=PISI" /></div>';
			echo $html2;
		}
	}
}
