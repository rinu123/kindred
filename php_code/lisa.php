﻿<?php

include('base.php');
if (!$sess = sisse_logitud()) {
	header('Location: index.php');
	die();
}
if ($grupi_oigused = grupi_oigused($sess)) {
	if ($grupi_oigused < 2) {
		header('Location: index.php');
		die();
	}
} else {
	header('Location: index.php');
	die();
}

if (isset($_GET['t'], $_GET['u'], $_GET['id'], $_GET['ref'])) {
	$xtabel = $_GET['t'];
	$tabel1 = kriips($xtabel);
	$u = $_GET['u'];
	$sisu = '
		<table class="colspace">
			<tr><td><a href="' . $_GET['ref'] . '.php?t=' . $_GET['u'] . '&id=' . $_GET['id'] . '">' . str_replace('{TABEL}', kriips($u), $lang['tagasi_juurde']) . '</a></td></tr>
		</table>';
} else {
	$sisu = '';
	$lisa = 0;
}
$pealkiri1 = $lang['lisa'];
$sisu .= <<<SISU
	<table class="colspace" id="default">
		<tr><td><a href="abi.php?t=lisa" target="_blank">$lang[lisamise_abi]</a></td></tr>
		<tr><td>$lang[lisa_paremalt]</td></tr>
	</table>
	<span id="content"></span>
SISU;
include('kujundus.php');
