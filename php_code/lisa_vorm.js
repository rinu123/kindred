function lisa_vorm(tabel, u, hide, id) {
	document.getElementById('default').style.display='none';
	document.getElementById('teade').style.display='none';
	if (u == 0) {
		$('#content').load('jquery/lisa_vorm.php?tabel='+tabel+'&id='+id);
	} else {
		if (!document.getElementById('' + tabel + '_' + u + '_sisu')) {
			$('#content').load('jquery/lisa_vorm.php?tabel='+tabel+'&u='+u+'&id='+id);
		} else {
			document.getElementById('' + tabel + '_' + u + '_sisu').style.display='inline';
			$('#' + tabel + '_' + u + '_sisu').load('jquery/lisa_vorm.php?tabel='+tabel+'&u='+u+'&hide='+hide);
		}
	}
}

function ab(form_id) {
	document.getElementById('teade').style.display='none';
	var form = document.getElementById(form_id);
	var elemente = form.elements.length;
	
	for (i = 0; i < elemente; i++) {
		if (form.elements[i].type == 'submit') {
			form.elements[i].disabled = false;
		}
	}
}

function arhiiviNr(tabel, id) {
	$('#' + tabel + '_arhiiv').load('jquery/arhiivinumber.php?tabel=' + tabel + '&id=' + id);
}

function generate_list(u, tabel) {
	$('#uus_' + tabel + '_' + tabel).load('jquery/generate_list.php?tabel=' + u + '&materjal=' + tabel);
}
