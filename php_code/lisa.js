function lisa(tabel, u, hide) {
	var form = document.getElementById(tabel + u);
	var elemente = form.elements.length;

	var vastus = "tabel=" + tabel;
	for (i = 0; i < elemente - 1; i++) {
		vastus = vastus + "&" + form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value);
	}
	if (u != '') {
		vastus = vastus + "&u=" + u;
		vastus = vastus + "&hide=" + hide;
	}

	$.post("jquery/lisa.php",
		vastus,
		function(data){
			$('#teade').html(data);
		});

	document.getElementById('teade').style.display = 'inline';
}
