<?php
include(__DIR__ . '/base.php');

function convertFiles($original, $converted, $format) {
	$ffmpegBin = defined('FFMPEG_BIN') ? FFMPEG_BIN : '/usr/bin/ffmpeg';

	if (is_dir($original)) {
		$handle = opendir($original);
		while ($file = readdir($handle)) {
			if (strpos($file, '.') !== 0) {
				$originalFileFullPath = $original . $file;
				if (is_dir($originalFileFullPath)) {
					convertFiles($originalFileFullPath . '/', $converted, $format);
				} elseif (in_array(strtolower(pathinfo($file)['extension']), ['wav', 'avi', 'mp4', 'mov', 'mpg', 'mts'])) {
					$finalConvertedFile = $converted . pathinfo($file)['filename'] . '.' . $format;
					if (!file_exists($finalConvertedFile) || filemtime($originalFileFullPath) > filemtime($finalConvertedFile)) {
						if ($format === 'mp3') {
							exec($ffmpegBin . ' -nostdin -nostats -i "' . $originalFileFullPath . '" -c:a libmp3lame "' . $finalConvertedFile . '"');
						}
						if ($format === 'mp4') {
							exec($ffmpegBin . ' -nostdin -nostats -i "' . $originalFileFullPath . '" -c:v libx264 -preset slow -crf 22 -c:a aac "' . $finalConvertedFile . '"');
						}
					}
				}
			}
		}
		closedir($handle);
	}
}

convertFiles(CHROOT_PREFIX . AUDIO_PATH, CHROOT_PREFIX . CONVERTED_AUDIO_PATH, 'mp3');
convertFiles(CHROOT_PREFIX . VIDEO_PATH, CHROOT_PREFIX . CONVERTED_VIDEO_PATH, 'mp4');
