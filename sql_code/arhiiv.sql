-- phpMyAdmin SQL Dump
-- version 3.3.10.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 06, 2013 at 01:21 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arhiiv`
--

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE IF NOT EXISTS `audio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sari` int(10) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(45) NOT NULL COMMENT 'show, wide',
  `pealkiri` varchar(225) DEFAULT NULL COMMENT 'show, wide',
  `markused` text,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `audio_trakk`
--

CREATE TABLE IF NOT EXISTS `audio_trakk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `audio` int(10) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(225) NOT NULL COMMENT 'show, wide',
  `keel` varchar(225) DEFAULT NULL COMMENT 'roll',
  `kestus` int(11) unsigned DEFAULT NULL COMMENT 'show, wide',
  `salvestusaeg` varchar(225) DEFAULT NULL,
  `salvestuskoht` varchar(225) DEFAULT NULL,
  `pealkiri` text,
  `markused` text,
  `seostuvad_viited` varchar(225) DEFAULT NULL,
  `markused_seostuvate_viidete_kohta` text,
  `originaali_formaat` varchar(225) DEFAULT NULL,
  `koopia_viide` varchar(225) DEFAULT NULL,
  `salvestusseadme_tyyp` varchar(225) DEFAULT NULL,
  `ressursi_tyyp` tinyint(4) unsigned DEFAULT NULL,
  `fyysiliselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `digitaalselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `resolutsioon` varchar(225) DEFAULT NULL,
  `failiformaat` varchar(225) DEFAULT NULL,
  `digit_aeg` date DEFAULT NULL COMMENT 'digitaliseerija',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `oigused` varchar(225) NOT NULL DEFAULT 'avalik',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='public' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `digitaliseerija`
--

CREATE TABLE IF NOT EXISTS `digitaliseerija` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `osaleja` int(11) unsigned NOT NULL COMMENT 'show, wide',
  `vanus` int(11) unsigned DEFAULT NULL COMMENT 'show, wide',
  `materjal` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'show, wide',
  `materjal_id` int(10) unsigned NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL,
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE IF NOT EXISTS `foto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fotoalbum` int(10) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(45) NOT NULL COMMENT 'show, wide',
  `pealkiri` text COMMENT 'show, roll, wide',
  `pildistamisaeg` varchar(225) DEFAULT NULL,
  `koht` varchar(225) DEFAULT NULL,
  `markused` text,
  `seostuvad_viited` varchar(225) DEFAULT NULL,
  `markused_seostuvate_viidete_kohta` varchar(225) DEFAULT NULL,
  `originaali_formaat` varchar(225) DEFAULT NULL,
  `koopia_viide` varchar(225) DEFAULT NULL,
  `oigused` varchar(45) NOT NULL DEFAULT 'avalik',
  `resolutsioon` int(11) unsigned DEFAULT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ressursi_tyyp` tinyint(4) unsigned DEFAULT NULL,
  `fyysiliselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `digitaalselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `salvestusseadme_tyyp` varchar(255) DEFAULT NULL,
  `failiformaat` varchar(255) DEFAULT NULL,
  `digit_aeg` date DEFAULT NULL COMMENT 'digitaliseerija',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='public' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fotoalbum`
--

CREATE TABLE IF NOT EXISTS `fotoalbum` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sari` int(11) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(45) NOT NULL COMMENT 'show, wide',
  `pealkiri` varchar(225) DEFAULT NULL COMMENT 'show, wide',
  `markused` varchar(255) DEFAULT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grupp`
--

CREATE TABLE IF NOT EXISTS `grupp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nimetus` varchar(255) NOT NULL COMMENT 'show, wide',
  `grupi_oigused` int(11) unsigned NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='sys' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kasikiri`
--

CREATE TABLE IF NOT EXISTS `kasikiri` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sari` int(10) NOT NULL,
  `arhiivinumber` varchar(225) NOT NULL COMMENT 'show, wide',
  `keel` varchar(20) DEFAULT NULL,
  `khk` varchar(225) DEFAULT NULL,
  `koht` varchar(225) DEFAULT NULL COMMENT 'roll',
  `pealkiri` text COMMENT 'show, wide',
  `too_liik` varchar(225) DEFAULT NULL,
  `lehekylgede_arv` int(10) unsigned DEFAULT NULL,
  `koostamise_aeg` varchar(45) DEFAULT NULL,
  `originaali_formaat` varchar(225) DEFAULT NULL,
  `markused` text,
  `seostuvad_viited` varchar(225) DEFAULT NULL,
  `markused_seostuvate_viidete_kohta` varchar(255) DEFAULT NULL,
  `koopia_viide` varchar(225) DEFAULT NULL,
  `ymberkirjutuse_orig_viide` varchar(225) DEFAULT NULL,
  `ymberkirjutamise_aasta` varchar(225) DEFAULT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ressursi_tyyp` tinyint(4) unsigned DEFAULT NULL,
  `fyysiliselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `digitaalselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `failiformaat` varchar(255) DEFAULT NULL,
  `resolutsioon` varchar(225) DEFAULT NULL,
  `digit_aeg` date DEFAULT NULL COMMENT 'digitaliseerija',
  `digit_lk_arv` int(11) DEFAULT NULL,
  `oigused` varchar(45) NOT NULL DEFAULT 'avalik',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='public' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kasutaja`
--

CREATE TABLE IF NOT EXISTS `kasutaja` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(20) DEFAULT NULL,
  `perenimi` varchar(20) DEFAULT NULL,
  `email` varchar(45) NOT NULL COMMENT 'show, wide',
  `grupp` int(20) NOT NULL DEFAULT '1',
  `parool` varchar(100) DEFAULT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  `aegub` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='sys' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keel`
--

CREATE TABLE IF NOT EXISTS `keel` (
  `id` varchar(225) NOT NULL COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='info';

-- --------------------------------------------------------

--
-- Table structure for table `kihelkond`
--

CREATE TABLE IF NOT EXISTS `kihelkond` (
  `id` varchar(225) NOT NULL COMMENT 'show, wide',
  `kihelkonna_nimi` varchar(225) DEFAULT NULL COMMENT 'wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='info';

-- --------------------------------------------------------

--
-- Table structure for table `lemmikud`
--

CREATE TABLE IF NOT EXISTS `lemmikud` (
  `id` int(225) unsigned NOT NULL AUTO_INCREMENT,
  `lisaja` int(225) NOT NULL,
  `tabel` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vali` int(225) NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='sys' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `logi`
--

CREATE TABLE IF NOT EXISTS `logi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL,
  `syndmus` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='sys' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `materjal_keel`
--

CREATE TABLE IF NOT EXISTS `materjal_keel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keel` varchar(50) NOT NULL COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `materjal` varchar(50) NOT NULL COMMENT 'show, wide',
  `materjal_id` int(10) unsigned NOT NULL,
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keel` (`keel`,`materjal`,`materjal_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `osaleja`
--

CREATE TABLE IF NOT EXISTS `osaleja` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(45) NOT NULL COMMENT 'show, wide',
  `perenimi` varchar(45) NOT NULL COMMENT 'show, wide',
  `hyydnimi` varchar(225) DEFAULT NULL,
  `kihelkond` varchar(225) DEFAULT NULL COMMENT 'show, wide',
  `kyla` varchar(225) DEFAULT NULL COMMENT 'wide',
  `talu` varchar(45) DEFAULT NULL,
  `elukoht` varchar(45) DEFAULT NULL,
  `synniaasta` varchar(225) DEFAULT NULL,
  `surmaaasta` varchar(225) DEFAULT NULL,
  `synnikoht` varchar(225) DEFAULT NULL,
  `synniparane_nimi` varchar(225) DEFAULT NULL,
  `markused_osaleja_kohta` text,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='materjalidega seotud inimesed' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `otsingu_logi`
--

CREATE TABLE IF NOT EXISTS `otsingu_logi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) NOT NULL,
  `sona` text NOT NULL,
  `tabel` varchar(225) NOT NULL,
  `grupp` tinyint(3) unsigned NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='sys' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roll`
--

CREATE TABLE IF NOT EXISTS `roll` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rolli_nimetus` varchar(225) NOT NULL COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `osaleja` int(11) unsigned NOT NULL COMMENT 'show, wide',
  `vanus` int(11) unsigned DEFAULT NULL COMMENT 'show, wide',
  `materjal` varchar(225) NOT NULL COMMENT 'show, wide',
  `materjal_id` int(10) unsigned NOT NULL,
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rolli_nimetus`
--

CREATE TABLE IF NOT EXISTS `rolli_nimetus` (
  `id` varchar(225) NOT NULL COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  `jrknr` int(10) unsigned NOT NULL,
  `materjali_geoinfo` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='info';

-- --------------------------------------------------------

--
-- Table structure for table `sari`
--

CREATE TABLE IF NOT EXISTS `sari` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pealkiri` varchar(225) DEFAULT NULL,
  `sarja_tahis` varchar(225) NOT NULL COMMENT 'show, wide',
  `sarja_selgitus` text COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  `materjali_tyyp` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tabeli_keeled`
--

CREATE TABLE IF NOT EXISTS `tabeli_keeled` (
  `id` varchar(3) NOT NULL,
  `audio` text NOT NULL,
  `sari` text NOT NULL,
  `arhiivinumber` text NOT NULL,
  `pealkiri` text NOT NULL,
  `markused` text NOT NULL,
  `andmebaasi_lisatud` text NOT NULL,
  `lisaja` text NOT NULL,
  `audio_trakk` text NOT NULL,
  `keel` text NOT NULL,
  `kestus` text NOT NULL,
  `salvestusaeg` text NOT NULL,
  `salvestuskoht` text NOT NULL,
  `originaali_formaat` text NOT NULL,
  `koopia_viide` text NOT NULL,
  `seostuvad_viited` text NOT NULL,
  `markused_seostuvate_viidete_kohta` text NOT NULL,
  `salvestusseadme_tyyp` text NOT NULL,
  `ressursi_tyyp` text NOT NULL,
  `resolutsioon` text NOT NULL,
  `failiformaat` text NOT NULL,
  `digit_aeg` text NOT NULL,
  `oigused` text NOT NULL,
  `digitaliseerija` text NOT NULL,
  `osaleja` text NOT NULL,
  `vanus` text NOT NULL,
  `materjal` text NOT NULL,
  `materjal_id` text NOT NULL,
  `foto` text NOT NULL,
  `fotoalbum` text NOT NULL,
  `pildistamisaeg` text NOT NULL,
  `koht` text NOT NULL,
  `kasikiri` text NOT NULL,
  `khk` text NOT NULL,
  `too_liik` text NOT NULL,
  `lehekylgede_arv` text NOT NULL,
  `koostamise_aeg` text NOT NULL,
  `ymberkirjutuse_orig_viide` text NOT NULL,
  `ymberkirjutamise_aasta` text NOT NULL,
  `digit_lk_arv` text NOT NULL,
  `keelejuht` text NOT NULL,
  `eesnimi` text NOT NULL,
  `perenimi` text NOT NULL,
  `hyydnimi` text NOT NULL,
  `kihelkond` text NOT NULL,
  `kyla` text NOT NULL,
  `talu` text NOT NULL,
  `elukoht` text NOT NULL,
  `synniaasta` text NOT NULL,
  `surmaaasta` text NOT NULL,
  `synnikoht` text NOT NULL,
  `synniparane_nimi` text NOT NULL,
  `markused_osaleja_kohta` text NOT NULL,
  `roll` text NOT NULL,
  `rolli_nimetus` text NOT NULL,
  `sarja_tahis` text NOT NULL,
  `sarja_selgitus` text NOT NULL,
  `video` text NOT NULL,
  `video_trakk` text NOT NULL,
  `asukoht` text NOT NULL,
  `aadress` text NOT NULL,
  `materjali_tyyp` text NOT NULL,
  `fyysiliselt_olemas` text NOT NULL,
  `digitaalselt_olemas` text NOT NULL,
  `kihelkonna_nimi` text NOT NULL,
  `materjal_keel` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='sys';

-- --------------------------------------------------------

--
-- Table structure for table `too_liik`
--

CREATE TABLE IF NOT EXISTS `too_liik` (
  `id` varchar(225) NOT NULL COMMENT 'show, wide',
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='info';

-- --------------------------------------------------------

--
-- Table structure for table `ui_keeled`
--

CREATE TABLE IF NOT EXISTS `ui_keeled` (
  `id` varchar(3) NOT NULL,
  `avaleht` text NOT NULL,
  `otsi` text NOT NULL,
  `lisa` text NOT NULL,
  `haldus` text NOT NULL,
  `seaded` text NOT NULL,
  `logi_sisse` text NOT NULL,
  `logi_valja` text NOT NULL,
  `lehe_pealkiri` text NOT NULL,
  `otsinguabi` text NOT NULL,
  `minu_lemmikud` text NOT NULL,
  `tapne_vaste` text NOT NULL,
  `otsi_paremalt` text NOT NULL,
  `genereeritud` text NOT NULL,
  `sekundiga` text NOT NULL,
  `otsi_andmebaasist` text NOT NULL,
  `tyhi_otsing` text NOT NULL,
  `viimane_otsing` text NOT NULL,
  `kuni` text NOT NULL,
  `aasta` text NOT NULL,
  `AAAA` text NOT NULL,
  `sekundid` text NOT NULL,
  `avalik` text NOT NULL,
  `mitteavalik` text NOT NULL,
  `otsitavad_vaartused` text NOT NULL,
  `valja_nimi` text NOT NULL,
  `lehekylgi_kokku_on` text NOT NULL,
  `neist_digitaliseeritud` text NOT NULL,
  `trakkide_kogupikkus_on` text NOT NULL,
  `otsiti` text NOT NULL,
  `parameetri_jargi_leiti` text NOT NULL,
  `rida` text NOT NULL,
  `lisa_lemmikutesse` text NOT NULL,
  `sinu_lemmik` text NOT NULL,
  `muuda` text NOT NULL,
  `kustuta` text NOT NULL,
  `tabelit_ei_leitud` text NOT NULL,
  `leiti` text NOT NULL,
  `vastust` text NOT NULL,
  `midagi_ei_leitud` text NOT NULL,
  `arhiivimaterjal` text NOT NULL,
  `email` text NOT NULL,
  `parool` text NOT NULL,
  `unustasin_parooli` text NOT NULL,
  `vale_email_voi_parool` text NOT NULL,
  `kasutajat_ei_leitud` text NOT NULL,
  `parool_muudetud_saadetud` text NOT NULL,
  `email_ei_saadetud` text NOT NULL,
  `telli_uus_parool` text NOT NULL,
  `lemmikud_info` text NOT NULL,
  `lemmikud_pealkiri` text NOT NULL,
  `uus_parool_kirja_pealkiri` text NOT NULL,
  `eelmised` text NOT NULL,
  `jargmised` text NOT NULL,
  `kehtiv_parool_pole_oige` text NOT NULL,
  `palun_vali_pikem_parool` text NOT NULL,
  `uued_paroolid_ei_kattu` text NOT NULL,
  `parool_on_vahetatud` text NOT NULL,
  `parooli_muutmine` text NOT NULL,
  `kehtiv_parool` text NOT NULL,
  `uus_parool` text NOT NULL,
  `uus_parool_uuesti` text NOT NULL,
  `muuda_parool` text NOT NULL,
  `lemmikud_info2` text NOT NULL,
  `eemalda_lemmikutest` text NOT NULL,
  `lisa_paremalt` text NOT NULL,
  `lisamise_abi` text NOT NULL,
  `tagasi` text NOT NULL,
  `tagasi_juurde` text NOT NULL,
  `lemmikut_ei_saanud_lisada` text NOT NULL,
  `juba_lisasid` text NOT NULL,
  `lemmik_lisatud` text NOT NULL,
  `tabelit` text NOT NULL,
  `ei_leitud` text NOT NULL,
  `numbrid` text NOT NULL,
  `TT_MM_SS` text NOT NULL,
  `peida` text NOT NULL,
  `digitaliseerija_lisamisvorm` text NOT NULL,
  `keelejuhi_lisamisvorm` text NOT NULL,
  `rolli_lisamisvorm` text NOT NULL,
  `on_kohustuslik` text NOT NULL,
  `vaata` text NOT NULL,
  `sama_arhiivinumbriga_kirjeid_on_veel` text NOT NULL,
  `lisatud` text NOT NULL,
  `paring_ei_onnestunud` text NOT NULL,
  `eelmine` text NOT NULL,
  `jargmine` text NOT NULL,
  `taissuurus` text NOT NULL,
  `lisa_uus` text NOT NULL,
  `tabeli_kirje_valimata_voi_kirjet_pole_olemas` text NOT NULL,
  `tabel_valimata_voi_tabelit_pole_olemas` text NOT NULL,
  `pole_oigusi_kustutamiseks` text NOT NULL,
  `jah` text NOT NULL,
  `oled_kindel_et_soovid_oma_lemmiku_kustutada` text NOT NULL,
  `sarja_ei_saa_kustutada` text NOT NULL,
  `osalejat_ei_saa_kustutada` text NOT NULL,
  `oled_kindel_et_soovid_kustutada` text NOT NULL,
  `koos_sellega_kustutaksid_veel_seotud_kirjet` text NOT NULL,
  `seotud_kirjed` text NOT NULL,
  `tabelis_pole_sellist_kirjet_olemas` text NOT NULL,
  `tabelit_pole_olemas` text NOT NULL,
  `lyhike_number` text NOT NULL,
  `boolean_jah_voi_ei` text NOT NULL,
  `suur_number` text NOT NULL,
  `number` text NOT NULL,
  `tekst` text NOT NULL,
  `pikk_tekst` text NOT NULL,
  `aeg_AAAA_KK_PP_TT_MM_SS` text NOT NULL,
  `kuupaev_PP_KK_AAAA` text NOT NULL,
  `uus_kasutaja_kirja_pealkiri` text NOT NULL,
  `kasutaja_on_aegunud` text NOT NULL,
  `keelejuhid` text NOT NULL,
  `digitaliseerijad` text NOT NULL,
  `muud_rollid` text NOT NULL,
  `viimati_sooritatud_otsing` text NOT NULL,
  `lae_alla` text NOT NULL,
  `vaata_fotot` text NOT NULL,
  `vaata_kasikirju` text NOT NULL,
  `lae_kasikirjad_alla` text NOT NULL,
  `sellist_kirjet_ei_leitud` text NOT NULL,
  `oled_hiljuti_parooli_tellinud` text NOT NULL,
  `kuula` text NOT NULL,
  `laen` text NOT NULL,
  `pole_saadaval` text NOT NULL,
  `kirje_muutmine_onnestus1` text NOT NULL,
  `kirje_muutmine_onnestus0` text NOT NULL,
  `video_trakkide_kogupikkus_on` text NOT NULL,
  `ei` text NOT NULL,
  `fyysiline` text NOT NULL,
  `digitaalne` text NOT NULL,
  `olemas` text NOT NULL,
  `kadunud` text NOT NULL,
  `laenatud` text NOT NULL,
  `andmed_osaleja_kohta` text NOT NULL,
  `haldus_abi` text NOT NULL,
  `kustutatud` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='sys';

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sari` int(11) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(45) NOT NULL COMMENT 'show, wide',
  `pealkiri` varchar(225) DEFAULT NULL COMMENT 'show, wide',
  `markused` text,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `video_trakk`
--

CREATE TABLE IF NOT EXISTS `video_trakk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` int(10) unsigned DEFAULT NULL,
  `arhiivinumber` varchar(225) NOT NULL COMMENT 'show, wide',
  `keel` varchar(45) DEFAULT NULL COMMENT 'roll',
  `kestus` int(10) unsigned DEFAULT NULL,
  `salvestusaeg` varchar(225) DEFAULT NULL,
  `pealkiri` varchar(225) DEFAULT NULL COMMENT 'show, wide',
  `originaali_formaat` varchar(225) DEFAULT NULL,
  `digit_aeg` date DEFAULT NULL COMMENT 'digitaliseerija',
  `resolutsioon` varchar(225) DEFAULT NULL,
  `markused` text,
  `seostuvad_viited` varchar(45) DEFAULT NULL,
  `markused_seostuvate_viidete_kohta` text,
  `koopia_viide` varchar(45) DEFAULT NULL,
  `asukoht` varchar(45) DEFAULT NULL,
  `oigused` varchar(45) NOT NULL,
  `andmebaasi_lisatud` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ressursi_tyyp` tinyint(4) unsigned DEFAULT NULL,
  `fyysiliselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `digitaalselt_olemas` tinyint(4) unsigned DEFAULT NULL,
  `aadress` varchar(1000) DEFAULT NULL,
  `salvestusseadme_tyyp` varchar(255) DEFAULT NULL,
  `failiformaat` varchar(255) DEFAULT NULL,
  `lisaja` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='public' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tekstid`
--

CREATE TABLE IF NOT EXISTS `tekstid` (
  `id` varchar(3) NOT NULL,
  `avaleht` text NOT NULL,
  `haldus` text NOT NULL,
  `lisa` text NOT NULL,
  `otsi` text NOT NULL,
  `uuskonto` text NOT NULL,
  `uusparool` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='sys';

--
-- Dumping data for table `grupp`
--

INSERT INTO `grupp` (`id`, `nimetus`, `grupi_oigused`, `andmebaasi_lisatud`, `lisaja`) VALUES
(1, 'grupp_avalik_kasutaja', 1, '0000-00-00 00:00:00', NULL),
(2, 'grupp_tavakasutaja', 2, '0000-00-00 00:00:00', NULL),
(3, 'grupp_administraator', 3, '0000-00-00 00:00:00', NULL);

--
-- Dumping data for table `tabeli_keeled`
--

INSERT INTO `tabeli_keeled` (`id`, `audio`, `sari`, `arhiivinumber`, `pealkiri`, `markused`, `andmebaasi_lisatud`, `lisaja`, `audio_trakk`, `keel`, `kestus`, `salvestusaeg`, `salvestuskoht`, `originaali_formaat`, `koopia_viide`, `seostuvad_viited`, `markused_seostuvate_viidete_kohta`, `salvestusseadme_tyyp`, `ressursi_tyyp`, `resolutsioon`, `failiformaat`, `digit_aeg`, `oigused`, `digitaliseerija`, `osaleja`, `vanus`, `materjal`, `materjal_id`, `foto`, `fotoalbum`, `pildistamisaeg`, `koht`, `kasikiri`, `khk`, `too_liik`, `lehekylgede_arv`, `koostamise_aeg`, `ymberkirjutuse_orig_viide`, `ymberkirjutamise_aasta`, `digit_lk_arv`, `keelejuht`, `eesnimi`, `perenimi`, `hyydnimi`, `kihelkond`, `kyla`, `talu`, `elukoht`, `synniaasta`, `surmaaasta`, `synnikoht`, `synniparane_nimi`, `markused_osaleja_kohta`, `roll`, `rolli_nimetus`, `sarja_tahis`, `sarja_selgitus`, `video`, `video_trakk`, `asukoht`, `aadress`, `materjali_tyyp`, `fyysiliselt_olemas`, `digitaalselt_olemas`, `kihelkonna_nimi`, `materjal_keel`) VALUES
('est', 'audio', 'sari', 'arhiivinumber', 'pealkiri', 'märkused', 'andmebaasi lisatud', 'lisaja', 'audioträkk', 'keel', 'kestus', 'salvestusaeg', 'salvestuskoht', 'originaali formaat', 'koopia viide', 'seostuvad viited', 'märkused seostuvate viidete kohta', 'salvestusseadme tüüp', 'ressursi tüüp', 'resolutsioon', 'failiformaat', 'digit aeg', 'õigused', 'digitaliseerija', 'osaleja', 'vanus', 'materjal', 'materjali id', 'foto', 'fotoalbum', 'pildistamisaeg', 'koht', 'käsikiri', 'kihelkond', 'töö liik', 'lehekülgede arv', 'koostamise aeg', 'ümberkirjutuse orig viide', 'ümberkirjutamise aasta', 'digit lk arv', 'keelejuht', 'eesnimi', 'perenimi', 'hüüdnimi', 'kihelkond', 'küla', 'talu', 'elukoht', 'sünniaasta', 'surmaaasta', 'sünnikoht', 'sünniparane nimi', 'märkused osaleja kohta', 'roll', 'rolli nimetus', 'sarja tähis', 'sarja selgitus', 'video', 'videoträkk', 'asukoht', 'aadress', 'materjali tüüp', 'Füüsiliselt olemas', 'Digitaalselt olemas', 'kihelkonna nimi', 'materjali keel'),
('eng', 'audio', 'series', 'archive number', 'title', 'comments', 'added to the database', 'added by', 'audio track', 'language', 'duration', 'recording time', 'recording place', 'originaali formaat', 'koopia viide', 'reference to related materials', 'comments about related materials', 'type of recording equipment', 'type of recourse', 'resolution', 'file format', 'digitizing time', 'access rights', 'digitized by', 'participant', 'age', 'material', 'material id', 'photo', 'photo album', 'time picture taken', 'place', 'manuscript', 'parish', 'type of work', 'number of pages', 'koostamise aeg', 'ymberkirjutuse orig viide', 'ymberkirjutamise aasta', 'digitized pages', 'informant', 'first name', 'last name', 'nickname', 'parish', 'village', 'farm name', 'place of residence', 'year of birth', 'year of death', 'place of birth', 'maiden name', 'comments about the person', 'role', 'role title', 'acronym of series', 'description of series', 'video', 'video track', 'location', 'aaddress', 'material type', 'physical copy available', 'digital copy available', 'kihelkonna nimi', 'materjali keel');

--
-- Dumping data for table `ui_keeled`
--

INSERT INTO `ui_keeled` (`id`, `avaleht`, `otsi`, `lisa`, `haldus`, `seaded`, `logi_sisse`, `logi_valja`, `lehe_pealkiri`, `otsinguabi`, `minu_lemmikud`, `tapne_vaste`, `otsi_paremalt`, `genereeritud`, `sekundiga`, `otsi_andmebaasist`, `tyhi_otsing`, `viimane_otsing`, `kuni`, `aasta`, `AAAA`, `sekundid`, `avalik`, `mitteavalik`, `otsitavad_vaartused`, `valja_nimi`, `lehekylgi_kokku_on`, `neist_digitaliseeritud`, `trakkide_kogupikkus_on`, `otsiti`, `parameetri_jargi_leiti`, `rida`, `lisa_lemmikutesse`, `sinu_lemmik`, `muuda`, `kustuta`, `tabelit_ei_leitud`, `leiti`, `vastust`, `midagi_ei_leitud`, `arhiivimaterjal`, `email`, `parool`, `unustasin_parooli`, `vale_email_voi_parool`, `kasutajat_ei_leitud`, `parool_muudetud_saadetud`, `email_ei_saadetud`, `telli_uus_parool`, `lemmikud_info`, `lemmikud_pealkiri`, `uus_parool_kirja_pealkiri`, `eelmised`, `jargmised`, `kehtiv_parool_pole_oige`, `palun_vali_pikem_parool`, `uued_paroolid_ei_kattu`, `parool_on_vahetatud`, `parooli_muutmine`, `kehtiv_parool`, `uus_parool`, `uus_parool_uuesti`, `muuda_parool`, `lemmikud_info2`, `eemalda_lemmikutest`, `lisa_paremalt`, `lisamise_abi`, `tagasi`, `tagasi_juurde`, `lemmikut_ei_saanud_lisada`, `juba_lisasid`, `lemmik_lisatud`, `tabelit`, `ei_leitud`, `numbrid`, `TT_MM_SS`, `peida`, `digitaliseerija_lisamisvorm`, `keelejuhi_lisamisvorm`, `rolli_lisamisvorm`, `on_kohustuslik`, `vaata`, `sama_arhiivinumbriga_kirjeid_on_veel`, `lisatud`, `paring_ei_onnestunud`, `eelmine`, `jargmine`, `taissuurus`, `lisa_uus`, `tabeli_kirje_valimata_voi_kirjet_pole_olemas`, `tabel_valimata_voi_tabelit_pole_olemas`, `pole_oigusi_kustutamiseks`, `jah`, `oled_kindel_et_soovid_oma_lemmiku_kustutada`, `sarja_ei_saa_kustutada`, `osalejat_ei_saa_kustutada`, `oled_kindel_et_soovid_kustutada`, `koos_sellega_kustutaksid_veel_seotud_kirjet`, `seotud_kirjed`, `tabelis_pole_sellist_kirjet_olemas`, `tabelit_pole_olemas`, `lyhike_number`, `boolean_jah_voi_ei`, `suur_number`, `number`, `tekst`, `pikk_tekst`, `aeg_AAAA_KK_PP_TT_MM_SS`, `kuupaev_PP_KK_AAAA`, `uus_kasutaja_kirja_pealkiri`, `kasutaja_on_aegunud`, `keelejuhid`, `digitaliseerijad`, `muud_rollid`, `viimati_sooritatud_otsing`, `lae_alla`, `vaata_fotot`, `vaata_kasikirju`, `lae_kasikirjad_alla`, `sellist_kirjet_ei_leitud`, `oled_hiljuti_parooli_tellinud`, `kuula`, `laen`, `pole_saadaval`, `kirje_muutmine_onnestus1`, `kirje_muutmine_onnestus0`, `video_trakkide_kogupikkus_on`, `ei`, `fyysiline`, `digitaalne`, `olemas`, `kadunud`, `laenatud`, `andmed_osaleja_kohta`, `haldus_abi`, `kustutatud`) VALUES
('est', 'Avaleht', 'Otsi', 'Lisa', 'Haldus', 'Seaded', 'Logi sisse', 'Logi välja', 'Tartu Ülikooli eesti murrete ja sugulaskeelte arhiiv', 'Otsinguabi', 'Minu lemmikud', 'Täpne vaste', 'Või vali paremalt, mida otsida soovid.', 'leht genereeritud', 'sekundiga', 'Otsi kogu andmebaasist:', 'Palun sisesta midagi otsingusse.', 'Viimati sooritatud otsing', 'kuni', 'aasta', 'AAAA', 'sekundid', 'Avalik', 'Mitteavalik', 'Otsitavad väärtused', 'Välja nimi', 'Lehekülgi kokku on', 'neist digitaliseeritud', 'Träkkide kogupikkus on', 'Otsiti', 'parameetri järgi, leiti', 'rida', 'Lisa lemmikutesse', 'Sinu lemmik', 'Muuda', 'Kustuta', 'Tabelit ei leitud', 'Leiti', 'vastust', 'Midagi ei leitud', 'Arhiivimaterjal', 'Email', 'Parool', 'Unustasin parooli', 'Email või parool on vale. Proovi uuesti.', 'Sellist kasutajat ei leitud', 'Parool on muudetud ja saadetud emaili aadressile', 'Tekkis probleem kirja saatmisel, parooli ei muudetud', 'Telli uus parool', 'Lemmikute lisamiseks tuleb otsingutulemuste kõrval oleval + märgiga pildil klikkida. Lemmikud kustuvad sessiooni aegudes.', 'Lemmikud', 'Arhiiv - uus parool', 'Eelmised', 'Järgmised', 'Kehtiv parool pole õige', 'Palun vali pikem parool', 'Uued paroolid ei kattu', 'Parool on vahetatud', 'Parooli muutmine', 'Kehtiv parool', 'Uus parool', 'Uus parool uuesti', 'Muuda parool', 'Lemmikute lisamiseks tuleb otsingutulemuste kõrval olevat + märgiga nuppu vajutada.', 'Eemalda lemmikutest', 'Vali paremalt, mida soovid lisada.', 'Lisamise abi', 'Tagasi', 'Tagasi {TABEL} juurde', 'Lemmikut ei saanud lisada', 'Juba lisasid', 'Lemmik lisatud', 'Tabelit', 'ei leitud!', 'numbrid', '[TT:][MM:]SS', 'Peida', 'digitaliseerija lisamisvorm', 'keelejuhi lisamisvorm', 'rolli lisamisvorm', 'on kohustuslik!', 'Vaata', 'Sama arhiivinumbriga kirjeid on veel', 'lisatud!', 'Päring ei õnnestunud', 'Eelmine', 'Järgmine', 'Täissuurus', 'Lisa uus', 'Tabeli kirje valimata või kirjet pole olemas!', 'Tabel valimata või tabelit pole olemas!', 'Pole õigusi kustutamiseks.', 'Jah', 'Oled kindel, et soovid selle oma lemmikute nimekirjast eemaldada?', 'Sarja ei saa kustutada, sest sellega on seotud veel {NUMBER} kirjet.', 'Osalejat ei saa kustutada, sest temaga on seotud {NUMBER} keelejuhti, digitaliseerijat ja muud rolli.', 'Oled kindel, et soovid {TABEL} kustutada?', 'Koos sellega kustutaksid veel {NUMBER} seotud kirjet.', 'Seotud kirjed', 'Tabelis pole sellist kirjet olemas.', 'Tabelit pole olemas.', 'lühike number', 'jah või ei (1/0)', 'suur number', 'number', 'tekst', 'pikk tekst', 'aeg AAAA-KK-PP TT:MM:SS', 'kuupäev [PP.][KK.]AAAA', 'Arhiiv - uus kasutaja', 'Teie kasutajakonto on aegunud. Konto aktiveerimiseks võtke palun ühendust arhiivi administraatoriga', 'Keelejuhid', 'Digitaliseerijad', 'Osalejad', 'Viimati sooritatud otsing', 'Lae alla', 'Vaata fotot', 'Vaata käsikirju', 'Lae käsikirjad alla', 'Sellist kirjet ei leitud', 'Oled hiljuti parooli tellinud', 'Kuula', 'Laen', 'Pole saadaval', 'Kirje muutmine õnnestus', 'Kirje muutmine õnnestus, kuid failinimesid ei saanud muuta, sest uue arhiivinumbriga fail(id) on juba olemas', 'Videoträkkide kogupikkus on', 'Ei', 'Füüsiline', 'Digitaalne', 'Olemas', 'Kadunud', 'Laenatud', 'Andmed osaleja kohta:', 'Haldusjuhend', 'Kustutatud!'),
('eng', 'Home', 'Search', 'Add', 'Admin', 'Settings', 'Log in', 'Log out', 'Archives of Estonian Dialects and Kindred Languages', 'Instructions for searching from the database', 'My favorites', 'Exact match', 'Or select what to search for from the menu on the right.', 'results generated in', 'seconds', 'Search the whole database:', 'Please enter a search string.', 'Your last search', 'to', 'year', 'YYYY', 'seconds', 'Public', 'Not public', 'Values to search for', 'Name of the field', 'Total number of pages is', 'of which digitized are', 'Total duration of recordings is', 'Searched by', 'parameter(s), found', 'row(s)', 'Add to favorites', 'Your favorite', 'Edit', 'Delete', 'Table not found', 'Found', 'matches', 'Nothing was found', 'Archive material', 'Email', 'Password', 'Forgot password?', 'Wrong email or password. Try again.', 'No such user was found', 'Your password is changed and sent to your email address.', 'There was a problem with sending an email, your password isn&#039;t changed.', 'Order a new password', 'To add to the list of favorites click the + icon next to a search result. Favorites are deleted automatically when the session expires.', 'Favorites', 'Archives of Estonian Dialects and Kindred Languages - new password', 'Previous', 'Next', 'Incorrect current password', 'Please select a longer password', 'New passwords don&#039;t match', 'Password changed successfully!', 'Change password', 'Current password', 'New password', 'Repeat new password', 'Change password', 'To add to your list of favorites click the + icon next to a search result.', 'Remove from favorites', 'Select from the menu on the right what to add.', 'Instructions for adding', 'Back', 'Back to {TABEL}', 'Could not add to favorites', 'Already added', 'Added to your favorites', 'Table', 'not found!', 'numbers', '[HH:][MM:]SS', 'Hide', 'digitaliseerija lisamisvorm', 'keelejuhi lisamisvorm', 'rolli lisamisvorm', 'is mandatory!', 'View', 'Entries with the same archive number already in the database!', 'added!', 'Query unsuccessful', 'Previous', 'Next', 'Full size', 'Add new', 'No row selected or does not exist!', 'No table selected or does not exist!', 'You don&#039;t have rights to delete this.', 'Yes', 'Are you sure you want to remove this from your list of favorites?', 'Series cannot be deleted, there are {NUMBER} of entries related to it.', 'Participant cannot be deleted, there are {NUMBER} role(s) related with this participant.', 'Are you sure you want to delete {TABEL}?', 'With this you would further delete {NUMBER} connected rows.', 'Connected entries', 'Such entry not found in this table.', 'Table doesn&#039;t exist.', 'short number', 'boolean (1/0)', 'big number', 'number', 'text', 'long text', 'time YYYY-MM-DD HH:MM:SS', 'date [DD.][MM.]YYYY', 'Archives of Estonian Dialects and Kindred Languages - new user', 'This user account has expired. Please contact the archive administrator to reactivate your account', 'Informants', 'Digitizers', 'Participants', 'Last search', 'Download', 'View the photo', 'View the manuscript', 'Download the manuscript', 'Entry not found', 'You have recently ordered a new password', 'Listen', 'Loading', 'Not available', 'Entry changed successfully', 'Kirje muutmine õnnestus, kuid failinimesid ei saanud muuta, sest uue arhiivinumbriga fail(id) on juba olemas', 'Total duration of video tracks is', 'No', 'Physical', 'Digital', 'Available', 'Lost', 'Borrowed', 'Information about the participant:', 'Instructions for adminisrator', 'Kustutatud!');
